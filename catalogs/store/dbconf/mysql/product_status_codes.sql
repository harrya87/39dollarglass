Database        product_status_codes      product_status_codes.txt __SQLDSN__
Database        product_status_codes      USER    __SQLUSER__
Database        product_status_codes      PASS    __SQLPASS__
Database        product_status_codes      CREATE_SQL      <<SQL
create table product_status_codes (
	code int not null auto_increment primary key,
	inactive TINYINT(1) not null default 0,
	reason varchar(255) not null
);
SQL

