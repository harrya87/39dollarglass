__NAME__ Logout_choice

[if type=explicit compare="[userdb function=logout clear='[cgi clear_values]']"]
	[set mv_no_count]1[/set]
	[set mv_no_session_id]1[/set]
	[if cgi clear_cart]
	[calc] @$Items = (); return; [/calc]
	mv_nextpage=[either][cgi mv_successpage][or][cgi mv_nextpage][/either]
[else]
	mv_nextpage=[either][cgi mv_failpage][or][cgi mv_nextpage][/either]
[/else]
[/if]

__END__

__NAME__ Logout

[if type=explicit compare="[userdb function=logout clear=1]"]
	[set mv_no_count]1[/set]
	[set mv_no_session_id]1[/set]
	mv_nextpage=[either][cgi mv_successpage][or][cgi mv_nextpage][/either]
[else]
	mv_nextpage=[either][cgi mv_failpage][or][cgi mv_nextpage][/either]
[/else]
[/if]

__END__

__NAME__ Login

[if env HTTPS eq 'on']
       [if type=explicit compare="[userdb function='login' validchars='-A-Za-z0-9_@.+']"]
		[set mv_no_count][/set]
		[set mv_no_session_id][/set]
		mv_nextpage=[either][cgi mv_successpage][or][cgi mv_nextpage][/either]
		[if cgi mv_nextpage eq 'product_info/use_your_own_frame']
			[set just_logged_in]1[/set]
		[/if]
	[else]
		mv_nextpage=[either][cgi mv_failpage][or][cgi mv_nextpage][/either]
	[/else]
	[/if]
[else]
	[calc]
		&Log("Login attempt using non-SSL mode.");
		$Session->{failure} = "Insecure logins are not allowed.";
		return;
	[/calc]
	mv_nextpage=[either][cgi mv_failpage][or][cgi mv_nextpage][/either]
[/else][/if]

__END__

__NAME__ Change_password

[if type=explicit compare="[userdb change_pass]"]
	mv_nextpage=[either][cgi mv_successpage][or][cgi mv_nextpage][/either]
[else]
	mv_nextpage=[either][cgi mv_failpage][or][cgi mv_nextpage][/either]
[/else]
[/if]

__END__
