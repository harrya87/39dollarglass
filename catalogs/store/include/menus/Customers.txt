code	mgroup	msort	next_line	indicator	exclude_on	depends_on	page	form	name	super	inactive	description	help_name	img_dn	img_up	img_sel	img_icon	url	debug
001	Customers	x001					admin/customer_search		Customer Search		0								
002	Customers	x002		showactive			admin/customer	showactive=1	Active Customers		0								1
003	Customers	x003		showinactive			admin/customer	showinactive=1	Inactive Customers		0								1
004	Customers	x004					admin/flex_editor	page_title=Create new customer&mv_data_table=userdb&help_name=create.new.customer&ui_new_item=1&ui_return_to=admin/customer	Create new customer		0								
005		x005					admin/customer_mailing		Customer Mailing		0	Send mail to all customers on a list							
006		x006					admin/consolidate		Consolidate Accounts		0
007		x007					admin/view-item-states		Item State List		0								
008	Customers	x008	1		deletecustomer	customer	admin/customer_view	customer=[cgi customer]	View		0								
009	Customers	x009			deletecustomer	customer	admin/customer_ship	customer=[cgi customer]	Shipping		0								
010	Customers	x010			deletecustomer	customer	admin/customer_bill	customer=[cgi customer]	Billing		0								
011	Customers	x011			deletecustomer	customer	admin/customer_all	customer=[cgi customer]	All		0								
012	Customers	x012			deletecustomer	customer	admin/customer_pref	customer=[cgi customer]	Preferences		0								
013	Customers	x013			deletecustomer	customer	admin/entry	customer=[cgi customer]	Enter Order		0								
