code	o_group	description	o_value	o_label	o_default	o_sort	price	wholesale	sku	o_subgroup	display_label	o_name	price_deduct	base_add	tint	o_label_es
100004	lens		SVF	Single-Vision (full time wear)	1		0				Single-Vision (full time wear)	Lens Type		0		
100006	lens		SVR	Single-Vision (reading only)			0				Single-Vision (reading only)	Lens Type		0		
100007	lens		BIF	Regular Bifocals (with a line)			40				Regular Bifocals (with a line)	Lens Type		0		
100008	lens		PROG	Progressives (no-line bifocals)			50				Progressives (no-line bifocals)	Lens Type		0		
100010	lenstype	Clear Polycarbonate Lenses	OPT1	Package A	1	01	0			Clear Lenses	Package A - Clear Polycarbonate (1.59 index) Lenses	Lens Option		0	0	
100011	lenstype	Clear Seiko&reg; 1.67 Super High-Index Lenses	OPT2	Package B		02	29.95			Clear Lenses	Package B - Clear Seiko&reg; 1.67 Super High-Index Lenses	Lens Option		0	0	
100009	lenstype	1.74 Ultra-High Index Lenses	OPT4	Package T		03	89.95			Clear Lenses	Package T - 1.74 Ultra-High Index Lenses	Lens Option		0	0	
100012	lenstype	CR39 Tinted Sun Lenses (Darkest Tint)	SUN1	Package C		03	9.95			Sunglass Lenses	Package C - CR39 Tinted Sun Lenses (Darkest Tint)	Lens Option		0	1	
100013	lenstype	Seiko&reg; 1.67 Super High-Index Sun Lenses (Dark Tint)	SUN3	Package D		05	39.90			Sunglass Lenses	Package D - Seiko&reg; 1.67 Super High-Index Sun Lenses (Dark Tint)	Lens Option		0	1	
100014	lenstype	CR39 Polarized Sun Lenses	POLS2	Package E		04	49.90			Sunglass Lenses	Package E - CR39 Polarized Sun Lenses	Lens Option		0	1	
100015	lenstype	Polycarbonate Polarized Sun Lenses	POLS4	Package F		06	88.90			Sunglass Lenses	Package F - Polycarbonate Polarized Sun Lenses	Lens Option		0	1	
100016	lenstype	CR39 Next Generation&trade; Transitions&reg; Lenses	TRAN1	Package G		07	49.90				Package G - CR39 Next Generation&trade; Transitions&reg; Lenses	Lens Option		0	1	
100017	lenstype	Polycarbonate Quantum&reg; Transitions&reg; Lenses	TRAN2	Package H		08	88.90				Package H - Polycarbonate Quantum&reg; Transitions&reg; Lenses	Lens Option		0	1	
100018	lenstype	Clear Polycarbonate Regular Bifocals	BIF_OPT1	Package J		09	0				Package J - Clear Polycarbonate Regular Bifocals	Lens Option		0	0	
100019	lenstype	Clear Polycarbonate Sola&reg; Progressives	PROG_OPT1	Package K		10	0				Package K - Clear Polycarbonate Sola&reg; Progressives	Lens Option		0	0	
100020	lenstype	NuPolar&reg; Polarized Polycarbonate Bifocals	BIF_POLS5	Package M		11	98.90				Package M - NuPolar&reg; Polarized Polycarbonate Bifocals	Lens Option		0	1	
100021	lenstype	NuPolar&reg; Image&reg; Polarized Polycarbonate Progressives	PROG_POLS6	Package N		12	98.90				Package N - NuPolar&reg; Image&reg; Polarized Polycarbonate Progressives	Lens Option		0	1	
100022	lenstype	Transitions VI&reg; SolaMAX&reg; Polycarbonate Progressives	PROG_TRAN3	Package O		13	88.90				Package O - Transitions VI&reg; SolaMAX&reg; Polycarbonate Progressives	Lens Option		0	1	
100032	coating		COA4	<b>Anti Reflective coating is automatically included with these frames at no additional charge!</b>		3	0				Included free	AR Coating		0		<b>La capa contra-reflexiva se incluye con estos marcos en ninguna carga adicional.</b>
100031	coating		COA3	No thanks		2	0				None	AR Coating		0		No Gracias
100029	coating		COA1	Yes (only $24.95 additional)		1	24.95				Yes	AR Coating		0		S� (Por solo $24.95 adicionales)
100034	varilux		yes	Yes (only <font color="#0000ff">${PRICE}</font> additional)		2	39				Yes	Varilux		0		S� (Por solo <font color="#0000ff">${PRICE}</font> adicionales)
100035	varilux		no	No thanks		1	0				No	Varilux		0		No Gracias
100036	varilux		mandatory	Varilux is a mandatory ${PRICE} upgrade for this frame. See our <a href="#" onclick="popWin('/small_frame_guideline.html','thewindow'); return false;" class="linkA">Small Frame Guideline</a> for more information.		3	38				Yes	Varilux		0		
100038	tint		CLEAR	Clear	1	01	0				Clear	Tint		0		
100040	tint		BROWN	Brown		02	0				Brown	Tint		0		
100041	tint		GREY	Grey		03	0				Grey	Tint		0		
100042	tint		G15	G-15		04	0				G-15	Tint		0		
100043	tint		BLUE	Blue		05	0				Blue	Tint		0		
100044	tint		ROSE	Rose		06	0				Rose	Tint		0		
100045	tint		ORANGE	Orange		07	0				Orange	Tint		0		
100046	tint		YELLOW	Yellow		08	0				Yellow	Tint		0		
100023	lenstype	Custom (please specify)	PKGZ	Package Z		99	0				Package Z - Custom (please specify)	Lens Option		0	0	
100054	lenstype	Transitions VI&reg; Varilux&reg; Airwear&reg; Photochromic Progressives	PROGE_TRAN2	Package RT			137.90				Package RT - Transitions VI&reg; Varilux&reg; Airwear&reg; Photochromic Progressives	Lens Option		0	1	
100058	lenstype	Clear Varilux&reg; Airwear&reg; Polycarbonate Progressives	PROGE_OPT3	Package R		14	49				Package R - Clear Varilux&reg; Airwear&reg; Polycarbonate Progressives	Lens Option		1	0	
100060	lens		PROGE	Progressives (Short)			50				Progressives (Short)	Lens Type		0		
100062	lenstype	Tinted CR39 Varilux&reg; Ellipse&trade; Progressives (Darkest Tint)	PROGE_SUN7	Package S		15	49				Package S - Tinted CR39 Varilux&reg; Ellipse&trade; Progressives (Darkest Tint)	Lens Option		0	1	
100067	case		CLAMSHELL	Hard Clamshell			0				Hard Clamshell	Case		0		
100069	case		POLKADOTS	Polka Dots		3	4.95				Polka Dots	Case		0		
100071	case		FLIPTOPCHROME	Fliptop Chrome		1	4.95				Fliptop Chrome	Case		0		
100072			GUNMETALFLIPTOP	Gunmetal Fliptop		2	4.95				Gunmetal Fliptop	Case		0		
100074	case		BULLSEYE	Bullseye		4	4.95				Bullseye	Case		0		
100075	case		ITALIANSMOOTH	Italian Smooth Black		5	4.95				Italian Smooth Black	Case		0		
100082	lenstype	Polycarbonate Tinted Sun Lenses (Dark Tint)	SUN9	Package I			19.95			Sunglass Lenses	Package I - Polycarbonate Tinted Sun Lenses (Dark Tint)	Lens Option		0	1	
100084	lenstype	Polycarbonate Tinted Sun Lenses (Dark Tint)	SUN10	Package I			-5.00			Sunglass Lenses	Package I - Polycarbonate Tinted Sun Lenses (Dark Tint)	Lens Option	19.95	0	1	
100086	lenstype	Tinted CR39 Regular Bifocals	BIF_SUN11	Package JC			9.95				Package JC - Tinted CR39 Regular Bifocals	Lens Option		0	1	
100088	lenstype	Tinted Polycarbonate Regular Bifocal Lenses (70% Tint)	BIF_SUN12	Package JI			19.95				Package JI - Tinted Polycarbonate Regular Bifocal Lenses (70% Tint)	Lens Option		0	1	
100092	lenstype	Tinted CR39 Sola&reg; Progressive Lenses	PROG_SUN14	Package KC			9.95				Package KC - Tinted CR39 Sola&reg; Progressive Lenses	Lens Option		0	1	
100094	lenstype	Tinted Polycarbonate Sola&reg; Progressives (70% tint)	PROG_SUN15	Package KI			19.95				Package KI - Tinted Polycarbonate Sola&reg; Progressives (70% tint)	Lens Option		0	1	
100096	lenstype	Tinted CR39 Varilux&reg; Progressive Lenses	PROG_SUN16	Package LC			49.95				Package LC - Tinted CR39 Varilux&reg; Progressive Lenses	Lens Option		0	1	
100098	lenstype	Tinted Varilux&reg; Airwear&reg; Polycarbonate Progressives	PROG_SUN17	Package LI			58.95				Package LI - Tinted Varilux&reg; Airwear&reg; Polycarbonate Progressives	Lens Option		0	1	
100100	lenstype	Tinted Varilux&reg; Airwear&reg; Polycarbonate Progressives	PROGE_SUN18	Package RI			68.95				Package RI - Tinted Varilux&reg; Airwear&reg; Polycarbonate Progressives	Lens Option		0	1	
100104	lenstype	NuPolar&reg; Polarized CR-39&trade; Optical Resin Bifocals	BIF_POLS7	Package WP			88.90				Package WP - NuPolar&reg; Polarized CR-39&trade; Optical Resin Bifocals	Lens Option		0	1	
100106	lenstype	Transitions VI&reg; CR-39&trade; Photochromic Bifocals	BIF_TRAN4	Package WT			88.90				Package WT - Transitions VI&reg; CR-39&trade; Photochromic Bifocals	Lens Option		0	1	
100113	lenstype	SMARTshades&reg; by Optical Dynamics&reg; Photochromic Bifocals	BIF_TRAN5	Package JUT			49.95				Package JUT - SMARTshades&reg; by Optical Dynamics&reg; Photochromic Bifocals	Lens Option		0	1	
100108	lenstype	CR39 Polarized Sola&reg; Progressives	PROG_POLS8	Package KP			59.95				Package KP - CR39 Polarized Sola&reg; Progressives	Lens Option		0	1	
100110	lenstype	CR39 Polarized Varilux&reg; Progressives	PROG_POLS9	Package LP			98.95				Package LP - CR39 Polarized Varilux&reg; Progressives	Lens Option		0	1	
100112	lenstype	Varilux&reg; Airwear&reg; Polarized Polycarbonate Progressives	PROG_POLS10	Package LN			137.90				Package LN - Varilux&reg; Airwear&reg; Polarized Polycarbonate Progressives	Lens Option		0	1	
100114	lenstype	CR39 Transitions&reg; Sola&reg; Progressives	PROG_TRAN4	Package KT			59.95				Package KT - CR39 Transitions&reg; Sola&reg; Progressives	Lens Option		0	1	
100116	lenstype	CR39 Transitions&reg; Varilux&reg; Progressives	PROG_TRAN5	Package LT			98.95				Package LT - CR39 Transitions&reg; Varilux&reg; Progressives	Lens Option		0	1	
100095	lenstype	SMARTshades&reg; Optical Dynamics&reg; Photochromic Progressives	PROG_TRAN8	Package UT			49.95				Package UT - SMARTshades&reg; Optical Dynamics&reg; Photochromic Progressives	Lens Option		0	1	
100097	lenstype	Transitions VI&reg; Varilux&reg; Airwear&reg; Polycarbonate Progressives	PROG_TRAN9	Package P			137.90				Package P - Transitions VI&reg; Varilux&reg; Airwear&reg; Polycarbonate Progressives	Lens Option		0	1	
100103	coating		custom	Custom			0				Custom	AR Coating		0		
100099	lens		custom	Custom			0				Custom	Lens Type		0		
100101	lens		safety	Safety eyewear (Single vision, Bifocals, or Progressives)			0				Safety eyewear (Single vision, Bifocals, or Progressives)	Lens Type		0		
100039	uyof_add		no	No	1	01	0				No	Rimless frame		0		No
100049	uyof_add		yes	Yes		02	10				Yes	Rimless frame		0		S�
100053	lenstype	CR39 Ellipse Transitions&copy; Lenses	PROGE_TRAN1	Package ST			100				Package ST - CR39 Ellipse Transitions&copy; Lenses	Lens Option		0	1	
100052	lenstype	Clear Polycarbonate Industrial-Grade Safety Lenses	OPT3	Package ZA			0			Clear Lenses	Package ZA - Clear Polycarbonate Industrial-Grade Safety Lenses	Lens Option		0	0	
100055	lenstype	Tinted Polycarbonate Industrial-Grade Safety Lenses	SUN11	Package ZI			19.95			Sunglass Lenses	Package ZI - Tinted Polycarbonate Industrial-Grade Safety Lenses	Lens Option		0	1	
100059	lenstype	Polarized Polycarbonate Industrial-Grade Safety Lenses	POLS5	Package ZI			88.90			Sunglass Lenses	Package ZI - Polarized Polycarbonate Industrial-Grade Safety Lenses	Lens Option		0	1	
100063	lenstype	Polycarbonate Transitions&copy; Industrial-Grade Safety Lenses	TRAN3	Package ZH			88.90				Package ZH - Polycarbonate Transitions&copy; Industrial-Grade Safety Lenses	Lens Option		0	1	
100066	lenstype	Clear Industrial-Grade Polycarbonate Regular Bifocal Safety Lenses	BIF_OPT2	Package ZJ			0				Package ZJ - Clear Industrial-Grade Polycarbonate Regular Bifocal Safety Lenses	Lens Option		0	0	
100109	lenstype	clearLIGHT&reg; by Optical Dynamics&reg; Bifocals	BIF_OPT3	Package JU			0				Package JU - clearLIGHT&reg; by Optical Dynamics&reg; Bifocals	Lens Option		0	0	
100068	lenstype	Tinted Industrial-Grade Polycarbonate Regular Bifocal Safety Lenses	BIF_SUN14	Package ZJI			19.95				Package ZJI - Tinted Industrial-Grade Polycarbonate Regular Bifocal Safety Lenses	Lens Option		0	1	
100111	lenstype	Tinted clearLIGHT&reg; by Optical Dynamics&reg; Bifocals	BIF_SUN15	Package JUI			9.95				Package JUI - Tinted clearLIGHT&reg; by Optical Dynamics&reg; Bifocals	Lens Option		0	1	
100076	lenstype	Clear Industrial-Grade Polycarbonate Kodak7reg; Progressives	PROG_OPT4	Package ZK			0				Package ZK - Clear Industrial-Grade Polycarbonate Kodak7reg; Progressives	Lens Option		0	0	
100077	lenstype	clearLIGHT&reg; by Optical Dynamics&reg; Progressives	PROG_OPT5	Package U			0				Package U - clearLIGHT&reg; by Optical Dynamics&reg; Progressives	Lens Option		0	0	
100079	lenstype	Varilux&reg; Airwear&reg; Polycarbonate Progressives	PROG_OPT6	Package L			49.00				Package L - Varilux&reg; Airwear&reg; Polycarbonate Progressives	Lens Option		0	0	
100080	lenstype	Tinted Industrial-Grade Polycarbonate Kodak&reg; Progressives	PROG_SUN18	Package ZKI			19.95				Package ZKI - Tinted Industrial-Grade Polycarbonate Kodak&reg; Progressives	Lens Option		0	1	
100081	lenstype	Tinted clearLIGHT&reg; by Optical Dynamics&reg; Progressives	PROG_SUN19	Package UI			9.95				Package UI - Tinted clearLIGHT&reg; by Optical Dynamics&reg; Progressives	Lens Option		0	1	
100091	lenstype	Tinted Varilux&reg; Airwear&reg; Polycarbonate Progressives	PROG_SUN20	Package LI			58.95				Package LI - Tinted Varilux&reg; Airwear&reg; Polycarbonate Progressives	Lens Option		0	1	
100061	lenstype	Tinted Polycarbonate Signet Armorlite&copy; (Dark Tint)	PROGE_SUN22	Package VI			19.95				Package VI - Tinted Polycarbonate Signet Armorlite&copy; (Dark Tint)	Lens Option		0	1	
100065	lenstype	Tinted CR39 Signet Armorlite&copy; (Darkest Tint)	PROGE_SUN21	Package VC			9.95				Package VC - Tinted CR39 Signet Armorlite&copy; (Darkest Tint)	Lens Option		0	1	
100105	lenstype	Tinted clearLIGHT&reg; by Optical Dynamics&reg; Progressives	PROGE_SUN23	Package YI			9.95				Package YI - Tinted clearLIGHT&reg; by Optical Dynamics&reg; Progressives	Lens Option		0	1	
100073	lenstype	Signet Armorlite&copy; Polycarbonate	PROGE_OPT2	Package V		13	0				Package V - Signet Armorlite&copy; Polycarbonate	Lens Option		0	0	
100102	lenstype	clearLIGHT&reg; by Optical Dynamics&reg; Progressives	PROGE_OPT4	Package Y			0				Package Y - clearLIGHT&reg; by Optical Dynamics&reg; Progressives	Lens Option		0	0	
100078	lenstype	Transitions VI&reg; Compact CR39 Photochromic Progressives	PROGE_TRAN5	Package KTC			69.95				Package KTC - Transitions VI&reg; Compact CR39 Photochromic Progressives	Lens Option		0	1	
100107	lenstype	SMARTshades&reg; Optical Dynamics&reg; Photochromic Progressives	PROGE_TRAN6	Package YT			49.95				Package YT - SMARTshades&reg; Optical Dynamics&reg; Photochromic Progressives	Lens Option		0	1	
100083	lenstype	Drivewear&reg; Transitions&reg; Polarized	POLS9	Package XC			89.00				Package XC - Drivewear&reg; Transitions&reg; Polarized	Lens Option		0	1	
100085	lenstype	Drivewear&reg; NuPolar&reg; Transitions&reg; Polarized Bifocals	BIF_POLS10	Package XJC			99.00				Package XJC - Drivewear&reg; NuPolar&reg; Transitions&reg; Polarized Bifocals	Lens Option		0	1	
100087	lenstype	Drivewear&reg; Image&reg; Polarized CR-39&trade; Progressives	PROG_POLS11	Package XKC			99.00				Package XKC - Drivewear&reg; Image&reg; Polarized CR-39&trade; Progressives	Lens Option		0	1	
100093	lenstype	Varilux&reg; Airwear&reg; Polarized Polycarbonate Progressives	PROG_POLS12	Package LN			137.90				Package LN - Varilux&reg; Airwear&reg; Polarized Polycarbonate Progressives	Lens Option		0	1	
100089	lenstype	Polycarbonate Polarized Lenses + Blue Mirror Coat	POLS12	Package FB			119.00				Package FB - Polycarbonate Polarized Lenses + Blue Mirror Coat	Lens Option		0	1	
100090	lenstype	Polycarbonate Polarized Lenses + Silver Mirror Coat	POLS13	Package FS			119.00				Package FS - Polycarbonate Polarized Lenses + Silver Mirror Coat	Lens Option		0	1	
