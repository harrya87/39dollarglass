name	value	category
CATEGORY_LIST_KIDS	4075_Silver4070_Navy4016_GoldTortoise4011_Gray4081_Blue4013_DeepWine4052_Gunmetal4051_Blue4001_Brown4004_Brown5009_GunmetalBlue5010_Brown	Category pages
CATEGORY_LIST_KIDS_SUN	4075_Silver4070_Navy4016_GoldTortoise4011_Gray4081_Blue4013_DeepWine4052_Gunmetal4051_Blue4001_Brown4004_Brown5009_GunmetalBlue5010_Brown	Category pages
CATEGORY_LIST_METAL	3008_Gunmetal3077_Black3023_Black3091_Brown3002_Black3097_Silver3005_Black3007_Gunmetal3021_Gunmetal7585_GunNavy3003_Brown7535_Coffee3004_Gunmetal7587_Gunmetal3009_Gunmetal3096_Black3017_Gunmetal3084_Black3025_Coffee3068_Silver3015_Gunmetal3074_Silver3067_Gunmetal3016_Silver3013_Black3073_DarkGunmetal3012_Champagne3062_Cafe3014_Burgandy3072_Silver3049_Brown8004_Gold3051_Brown	Category pages
CATEGORY_LIST_METAL_SUN	3008_Gunmetal3077_Black3023_Black3091_Brown3002_Black3097_Silver3005_Black3007_Gunmetal3021_Gunmetal7585_GunNavy3003_Brown7535_Coffee3004_Gunmetal7587_Gunmetal3009_Gunmetal3096_Black3017_Gunmetal3084_Black3025_Coffee3068_Silver3015_Gunmetal3074_Silver3067_Gunmetal3016_Silver3013_Black3073_DarkGunmetal3012_Champagne3062_Cafe3014_Burgandy3072_Silver3049_Brown8004_Gold3051_Brown	Category pages
CATEGORY_LIST_PLASTIC	3529_Black3524_BlackCrystal3516_BlackCrystal3525_BlackCrystal3561_BlackLime3566_Black3502_BlackCrystal3539_Black3520_Black3575_Brown3482_BlackCherry3360_Black3501_BlackCrystal3574_WineGold3409_Brown3511_BlackTan3536_Black3563_Black3526_BlackCrystal3573_BrownTan3565_RedGold3571_WineGold3533_DemiAmber3520_Coffee3572_BrownOrange3554_BlackBrown3513_PinkGrape3507_Tan3504_Mocha3517_Brown3560_Black3514_Caramel3494_Coffee3515_BlackTan3552_BrownMarble3518_Grey3519_Rose	Category pages
CATEGORY_LIST_PLASTIC_SUN	3529_Black3524_BlackCrystal3516_BlackCrystal3525_BlackCrystal3561_BlackLime3566_Black3502_BlackCrystal3539_Black3520_Black3575_Brown3482_BlackCherry3360_Black3501_BlackCrystal3574_WineGold3409_Brown3511_BlackTan3536_Black3563_Black3526_BlackCrystal3573_BrownTan3565_RedGold3571_WineGold3533_DemiAmber3520_Coffee3572_BrownOrange3554_BlackBrown3513_PinkGrape3507_Tan3504_Mocha3517_Brown3560_Black3514_Caramel3494_Coffee3515_BlackTan3552_BrownMarble3518_Grey3519_Rose	Category pages
CATEGORY_LIST_RIMLESS	2164_Brown2165_Brown2166_Brown2167_Brown5505_Brown5506_Brown6001_Blue6002_Blue6003_Pink6004_Blue5101_Black5102_Black5103_Black5104_Brown5105_Brown5106_GMBlue	Category pages
CATEGORY_LIST_RIMLESS_SUN	2164_Brown 2165_Brown 2166_Brown 2167_Brown 5505_Silver 5506_Brown 6001_Blue 6002_Crystal 6003_Pink 6004_Brown 5101_Black 5102_Black 5103_Black 5104_Brown 5105_Brown 5106_GMBlue	Category pages
CATEGORY_LIST_SEMI_RIMLESS	6559_Black6576_Brown6506_Gunmetal7584_SilverGray6563_Chocolate6511_Tortoise6556_GunmetalGray7971_Gunmetal6565_Black7586_SilverBlack6505_Brown6562_BlackWine	Category pages
CATEGORY_LIST_SEMI_RIMLESS_SUN	6559_Black6576_Brown6506_Gunmetal7584_SilverGray6563_Chocolate6511_Tortoise6556_GunmetalGray7971_Gunmetal6565_Black7586_SilverBlack6505_Brown6562_BlackWine	Category pages
CATEGORY_LIST_BENDABLE_TITANIUM	5020_Black5016_Gunmetal5024_Gunmetal5023_Gunmetal5071_Black5070_GunmetalBlue5072_Black5079_Rose5080_Chocolate5007_GunmetalBlue5005_Grey5011_Brown5004_GunmetalBlue5009_GunmetalBlue5010_Brown5101_Black5102_Black5103_Black5104_Brown5105_Brown5106_GMBlue	Category pages
CATEGORY_LIST_BENDABLE_TITANIUM_SUN	5020_Black5016_Gunmetal5024_Gunmetal5023_Gunmetal5071_Black5070_GunmetalBlue5072_Black5079_Rose5080_Chocolate5007_GunmetalBlue5005_Grey5011_Brown5004_GunmetalBlue5009_GunmetalBlue5010_Brown5101_Black5102_Black5103_Black5104_Brown5105_Brown5106_GMBlue	Category pages
CATEGORY_LIST_NEW_ARRIVALS	3077_Black3539_Black3091_Brown3575_Brown3096_Black3360_Black3097_Silver3563_Black7585_GunNavy6576_Brown7535_Coffee3574_BrownTan7587_Gunmetal3573_WinePink7584_SilverGray3511_BrownTan7971_Gunmetal3572_WineRose7586_SilverBlack3571_BrownTan3068_Silver6511_Tortoise3074_Black6556_GunmetalGray3084_Black6565_Black5020_Black6562_BlackWine5016_Gunmetal4013_DeepWine5024_Gunmetal4011_Gray5023_Gunmetal4070_Navy5071_Black4081_Blue5079_Rose4075_Silver5070_GunmetalBlue4051_Blue5072_Black4052_Gunmetal5080_Chocolate4016_GoldTortoise	Category pages
CATEGORY_LIST_NEW_ARRIVALS_SUN	3077_Black3539_Black3091_Brown3575_Brown3096_Black3360_Black3097_Silver3563_Black7585_GunNavy6576_Brown7535_Coffee3574_BrownTan7587_Gunmetal3573_WinePink7584_SilverGray3511_BrownTan7971_Gunmetal3572_WineRose7586_SilverBlack3571_BrownTan3068_Silver6511_Tortoise3074_Black6556_GunmetalGray3084_Black6565_Black5020_Black6562_BlackWine5016_Gunmetal4013_DeepWine5024_Gunmetal4011_Gray5023_Gunmetal4070_Navy5071_Black4081_Blue5079_Rose4075_Silver5070_GunmetalBlue4051_Blue5072_Black4052_Gunmetal5080_Chocolate4016_GoldTortoise	Category pages
SUNGLASS_TINTS	<span title="Brown" style="border: solid #000000 1px;background-color: #7a5640; color: #000000; width: 10px; height: 10px;overflow: hidden; display: inline">&nbsp;</span><img src="__THEME_IMG_DIR__spacer.gif" width="2" height="1" alt=""><span title="Grey" style="border: solid #000000 1px; background-color: #333333;color: #000000; width: 10px; height: 10px; overflow: hidden; display:inline">&nbsp;</span><img src="__THEME_IMG_DIR__spacer.gif" width="2" height="1" alt=""><span title="G15" style="border: solid #000000 1px;background-color: #385950; color: #000000; width: 10px; height: 10px;overflow: hidden; display: inline">&nbsp;</span><img src="__THEME_IMG_DIR__spacer.gif" width="2" height="1" alt=""><span title="Blue" style="border: solid #000000 1px; background-color: #40697b;color: #000000; width: 10px; height: 10px; overflow: hidden; display:inline">&nbsp;</span><img src="__THEME_IMG_DIR__spacer.gif" width="2" height="1" alt=""><span title="Rose" style="border: solid #000000 1px;background-color: #eba2a9; color: #000000; width: 10px; height: 10px;overflow: hidden; display: inline">&nbsp;</span><img src="__THEME_IMG_DIR__spacer.gif" width="2" height="1" alt=""><span title="Orange" style="border: solid #000000 1px; background-color: #ffa418;color: #000000; width: 10px; height: 10px; overflow: hidden; display:inline">&nbsp;</span><img src="__THEME_IMG_DIR__spacer.gif" width="2" height="1" alt=""><span title="Yellow" style="border: solid #000000 1px;background-color: #ffff66; color: #000000; width: 10px; height: 10px;overflow: hidden; display: inline">&nbsp;</span>	Category pages
CATEGORY_LIST_PROG_RIMLESS	2164_Brown2165_Brown2166_Brown2167_Brown5505_Brown5506_Brown	Category pages
CATEGORY_LIST_PROG_METAL	3008_Gunmetal3023_Black3077_Black3002_Black3005_Black3091_Brown3007_Gunmetal3084_Black3021_Gunmetal3097_Silver3003_Brown3068_Silver3004_Gunmetal3074_Silver3009_Gunmetal3096_Black3017_Gunmetal7585_GunNavy3025_Coffee7535_Coffee3015_Gunmetal7587_Gunmetal3067_Gunmetal3016_Silver3013_Black3073_DarkGunmetal3012_Champagne3062_Cafe3014_Burgandy3072_Silver3049_Brown8004_Gold3051_Brown	Category pages
CATEGORY_LIST_PROG_BEND_TIT	5020_Black5016_Gunmetal5024_Gunmetal5023_Gunmetal5071_Black5070_GunmetalBlue5072_Black5079_Rose5080_Chocolate5007_GunmetalBlue5005_Grey5011_Brown	Category pages
CATEGORY_LIST_BIF_METAL	3008_Gunmetal3077_Black3023_Black3091_Brown3002_Black3097_Silver3005_Black3007_Gunmetal3021_Gunmetal7585_GunNavy3003_Brown7535_Coffee3004_Gunmetal7587_Gunmetal3009_Gunmetal3096_Black3017_Gunmetal3084_Black3025_Coffee3068_Silver3015_Gunmetal3074_Silver3067_Gunmetal3016_Silver3013_Black3073_DarkGunmetal3012_Champagne3062_Cafe3014_Burgandy3072_Silver3049_Brown8004_Gold3051_Brown	Category pages
POWER_REVIEWS_ENABLE	1	Partners
POWER_REVIEWS_JS	<script type="text/javascript">var pr_style_sheet="http://cdn.powerreviews.com/aux/12320/7516/css/express.css";</script><script type="text/javascript" src="http://cdn.powerreviews.com/repos/12320/pr/pwr/engine/js/full.js"></script>	Partners
CATEGORY_LIST_MENU_SUN	3005_Black Metal Sunglasses metal-sunglasses3501_BlackCrysta  Plastic Sunglasses plastic-sunglasses2164_Brown Rimless Sunglasses rimless-sunglasses6508_Gunmetal Semi Rimless Sunglasses semi-rimless-sunglasses5005_Brown Bendable Titanium Sunglasses bendable-titanium-sunglasses2153_MatteBrown Kids Sunglasses kids-sunglasses	Category pages
CATEGORY_LIST_MENU_SUN_en_US	3005_Black|Metal Sunglasses|metal-sunglasses3501_BlackCrystal|Plastic Sunglasses|plastic-sunglasses2164_Brown|Rimless Sunglasses|rimless-sunglasses6508_Gunmetal|Semi Rimless Sunglasses|semi-rimless-sunglasses5005_Brown|Bendable Titanium Sunglasses|bendable-titanium-sunglasses4003_Brown|Kids Sunglasses|kids-sunglasses3007_Brown|Bifocals|bifocals-sun3005_Black|Progressives &amp; Varilux&reg;|progressives-sun	Category pages
CATEGORY_LIST_MENU_SUN_es_ES	3005_Black Lentes de Sol Met�licas metal-sunglasses3501_BlackCrystal Lentes de Sol Pl�sticas plastic-sunglasses2164_Brown Lentes de Sol Sin borde rimless-sunglasses6508_Gunmetal Lentes de Sol de medio borde semi-rimless-sunglasses5005_Brown Lentes de Sol flexibles de Titanio bendable-titanium-sunglasses2153_MatteBrown Lentes de Sol para ni�os kids-sunglasses3007_Brown Lentes de Sol Bifocales bifocals-sun3005_Black Lentes de Sol Progresivas & Varilux&reg; progressives-sun	Category pages
CATEGORY_LIST_MENU_en_US	3002_Black Metal Eyeglasses 110003506_BlackCrystal Plastic Eyeglasses 120002164_Brown Rimless Eyeglasses 140005002_Gunmetal Bendable Titanium Eyeglasses 150002103_Black Progressives &amp; Varilux&trade; progressives3008_Gunmetal Bifocal Eyeglasses bifocals2153_MatteBrown Kids Eyeglasses 130006509_Silver Semi-Rimless Eyeglasses semi-rimless-eyeglasses	Category pages
CATEGORY_LIST_MENU_es_ES	3002_Black Lentes met�licos 110003506_BlackCrystal Lentes pl�sticos 120002164_Brown Lentes sin montura 140005002_Gunmetal Lentes Titanium flexibles 150002103_Black Progresivos y Varilux&reg; progressives3008_Gunmetal Multifocales bifocals2153_MatteBrown Lentes Deportivos 130006509_Silver Lentes para ninos semi-rimless-eyeglasses	Category pages
CATEGORY_LIST_MENU	3002_Black|Metal Eyeglasses|110003520_Black|Plastic Eyeglasses|120002164_Brown|Rimless Eyeglasses|140005002_Gunmetal|Bendable Titanium Eyeglasses|150002103_Black|Progressives &amp; Varilux&trade;|progressives3008_Gunmetal|Bifocal Eyeglasses|bifocals4003_Gunmetal|Kids Eyeglasses|130006529_Wine|Semi-Rimless Eyeglasses|semi-rimless-eyeglasses	Category pages
PD_RANGE	383940414243444546474849505152535455565758596061626364656667686970717273747576777879808182838485868788	Prescription Info
CATEGORY_LIST_BIF_SEMI_RIMLESS	6559_Black6576_Brown6506_Gunmetal7584_SilverGray6563_Chocolate6511_Tortoise6556_GunmetalGray7971_Gunmetal6565_Black7586_SilverBlack6505_Brown6562_BlackWine	Category pages
SPH_RANGE	PLANO-12.00-11.75-11.50-11.25-11.00-10.75-10.50-10.25-10.00-9.75-9.50-9.25-9.00-8.75-8.50-8.25-8.00-7.75-7.50-7.25-7.00-6.75-6.50-6.25-6.00-5.75-5.50-5.25-5.00-4.75-4.50-4.25-4.00-3.75-3.50-3.25-3.00-2.75-2.50-2.25-2.00-1.75-1.50-1.25-1.00-0.75-0.50-0.25+0.25+0.50+0.75+1.00+1.25+1.50+1.75+2.00+2.25+2.50+2.75+3.00+3.25+3.50+3.75+4.00+4.25+4.50+4.75+5.00+5.25+5.50+5.75+6.00+6.25+6.50+6.75+7.00+7.25+7.50+7.75+8.00D.S.SPH	Prescription Info
CATEGORY_LIST_BIF_BEND_TIT_RIMLESS	5101_Black 5102_Black 5103_Black 5104_Brown 5105_Brown 5106_GMBlue	Category pages
CYL_RANGE_LARGE	-10.00-9.75-9.50-9.25-9.00-8.75-8.50-8.25-8.00-7.75-7.50-7.25-7.00-6.75-6.50-6.25-6.00-5.75-5.50-5.25-5.00-4.75-4.50-4.25-4.00-3.75-3.50-3.25-3.00-2.75-2.50-2.25-2.00-1.75-1.50-1.25-1.00-0.75-0.50-0.25+0.00+0.25+0.50+0.75+1.00+1.25+1.50+1.75+2.00+2.25+2.50+2.75+3.00+3.25+3.50+3.75+4.00+4.25+4.50+4.75+5.00+5.25+5.50+5.75+6.00+6.25+6.50+6.75+7.00+7.25+7.50+7.75+8.00+8.25+8.50+8.75+9.00+9.25+9.50+9.75+10.00D.S.SPH	Prescription Info
CATEGORY_LIST_BIF_BEND_TIT	5020_Black5016_Gunmetal5024_Gunmetal5023_Gunmetal5071_Black5070_GunmetalBlue5072_Black5079_Rose5080_Chocolate5007_GunmetalBlue5005_Grey5011_Brown5009_GunmetalBlue5010_Brown5101_Black5102_Black5103_Black5104_Brown5105_Brown5106_GMBlue	Category pages
ADD_RANGE	+0.25+0.50+0.75+1.00+1.25+1.50+1.75+2.00+2.25+2.50+2.75+3.00+3.25+3.50	Prescription Info
CATEGORY_LIST_PROG_BEND_TIT_RIMLESS	5101_Black5102_Black5103_Black5104_Brown5105_Brown5106_GMBlue	Category pages
CATEGORY_LIST_BIF_PLASTIC	3529_Black3524_BlackCrystal3516_BlackCrystal3561_BlackLime3566_Black3539_Black3520_Black3575_Brown3482_BlackCherry3360_Black3501_BlackCrystal3574_WineGold3409_Brown3511_BlackTan3536_Black3563_Black3526_BlackCrystal3573_BrownTan3565_RedGold3571_WineGold3533_DemiAmber3520_Coffee3572_BrownOrange3554_BlackBrown3513_PinkGrape3507_Tan3504_Mocha3517_Brown3560_Black3514_Caramel3515_BlackTan3552_BrownMarble3518_Grey3519_Rose	Category pages
CYL_RANGE	-3.75-3.50-3.25-3.00-2.75-2.50-2.25-2.00-1.75-1.50-1.25-1.00-0.75-0.50-0.25+0.00+0.25+0.50+0.75+1.00+1.25+1.50+1.75+2.00+2.25+2.50+2.75+3.00+3.25+3.50+3.75D.S.SPH	Prescription Info
AXI_RANGE	001002003004005006007008009010011012013014015016017018019020021022023024025026027028029030031032033034035036037038039040041042043044045046047048049050051052053054055056057058059060061062063064065066067068069070071072073074075076077078079080081082083084085086087088089090091092093094095096097098099100101102103104105106107108109110111112113114115116117118119120121122 123124125126127128129130131132133134135136137138139140141142143144145146147148149150151152153154155156157158159160161162163164165166167168169170171172173174175176177178179180	Prescription Info
GENERIC_KEYWORDS	eyeglasses,sunglasses,eyeglass lenses,eyeglass,glasses,eye glasses,sun glasses,prescription eyeglasses, prescription eye glasses,cheap eyeglasses,cheap eye glasses,eye doctor,eye doctors,eyeglass prescription,eye glass prescription,prescription glasses,optician,opticians,optometrist,optometrists,ophthalmologist,ophthalmologists,opthamologist,opthamologists,eyewear,sunwear,eyesight,eye sight,nearsighted,farsighted,vision,39dollarglasses,39 dollar glasses,39 glasses,bifocals,cheap glasses,discount eye glasses,discount eyeglasses,discount eyewear,discount glasses,eyeglass frame,eye glass frame,eyeglasses online,eye glasses online,lens,rimless eyeglasses,rimless eye glasses,progressive lens,progressive lenses,transition,transitions,varilux,prescription sunglasses,prescription sun glasses, prescription sunglass,prescription sun glass,polarized lens,polarized sunglasses,polarized sun glasses,CR39,CR-39,polycarbonate,high index,anti-reflective coating,A/R coating,antireflective coating,amblyopia,smartfit,UV protection,metal eyeglasses,metal eye glasses,plastic eyeglasses,plastic eye glasses,titanium eyeglasses,titanium eye glasses,bendable eyeglasses,bendable eye glasses,lens replacement,kids eyeglasses,kids eye glasses,child eyeglasses,child eye glasses,presbyopia	SEO
SPH_RANGE_LARGE	PLANOBAL-20.00-19.75-19.50-19.25-19.00-18.75-18.50-18.25-18.00-17.75-17.50-17.25-17.00-16.75-16.50-16.25-16.00-15.75-15.50-15.25-15.00-14.75-14.50-14.25-14.00-13.75-13.50-13.25-13.00-12.75-12.50-12.25-12.00-11.75-11.50-11.25-11.00-10.75-10.50-10.25-10.00-9.75-9.50-9.25-9.00-8.75-8.50-8.25-8.00-7.75-7.50-7.25-7.00-6.75-6.50-6.25-6.00-5.75-5.50-5.25-5.00-4.75-4.50-4.25-4.00-3.75-3.50-3.25-3.00-2.75-2.50-2.25-2.00-1.75-1.50-1.25-1.00-0.75-0.50-0.25+0.00+0.25+0.50+0.75+1.00+1.25+1.50+1.75+2.00+2.25+2.50+2.75+3.00+3.25+3.50+3.75+4.00+4.25+4.50+4.75+5.00+5.25+5.50+5.75+6.00+6.25+6.50+6.75+7.00+7.25+7.50+7.75+8.00+8.25+8.50+8.75+9.00+9.25+9.50+9.75+10.00+10.25+10.50+10.75+11.00+11.25+11.50+11.75+12.00+12.25+12.50+12.75+13.00+13.25+13.50+13.75+14.00+14.25+14.50+14.75+15.00+15.25+15.50+15.75+16.00+16.25+16.50+16.75+17.00+17.25+17.50+17.75+18.00+18.25+18.50+18.75+19.00+19.25+19.50+19.75+20.00D.S.SPH	Prescription Info
CATEGORY_LIST_PROG_PLASTIC	3529_Black3524_BlackCrystal3516_BlackCrystal3561_BlackLime3566_Black3539_Black3520_Black3575_Brown3360_Black3501_BlackCrystal3574_WineGold3409_Brown3511_BlackTan3536_Black3563_Black3526_BlackCrystal3573_BrownTan3565_RedGold3571_WineGold3533_DemiAmber3520_Coffee3572_BrownOrange3513_PinkGrape3504_Mocha3517_Brown3560_Black3514_Caramel3515_BlackTan3552_BrownMarble3518_Grey3519_Rose	Category pages
CATEGORY_LIST_SAFETY_GLASSES	7001_GoldTortoise7001_SilverBlack7004_Grey7008_Silver7005_Silver7005_Gold7006_Brown7006_Silver7009_Copper7009_SilverGold	Category pages
CATEGORY_LIST_PROG_SEMI_RIMLESS	6559_Black6576_Brown6506_Gunmetal7584_SilverGray6563_Chocolate7971_Gunmetal6556_GunmetalGray7586_SilverBlack6565_Black6562_BlackWine6505_Brown5007_GunmetalBlue	Category pages
CATEGORY_LIST_BIF_RIMLESS	2164_Brown 2165_Brown 2166_Brown 2167_Brown 5505_Brown 5506_Brown	Category pages
EMAIL_SERVICE	contact@39dollarglasses.com	Company
SHIP_DEFAULT_MODE	DOM1	Shipping
SHIP_FLAT_INTL_RATE	12.95	Shipping
XSELL_DISCOUNT	10	General
TEMPLATE_DIR	/home/sites/www.39dollarglasses.com/report_templates	Directories and Paths
AMBASSADOR_CREDIT_SFP	10	Rewards
PGP_KEY	724A025B	Encryption
LABEL_FROM_ADDRESS	Value Eyecare Network, Inc.3125 Veterans Memorial Hwy., Suite 3Ronkonkoma, NY 11779	Shipping
EMAIL_IMAGE_PREFIX	http://www.39dollarglasses.com/remote/email/	Directories and Paths
TAXSHIPPING		Tax
TAXFIELD	state	Tax
UYOF_SHIP_COST	5.95	Shipping
SHIP_DEFAULT_COUNTRY	US	Shipping
TAXRATE		Tax
MV_DEMO_MODE	0	General
EMAIL_INFO	contact@39dollarglasses.com	Company
UI_TRAFFIC_STATS		Directories and Paths
AMBASSADOR_CREDIT_PPP	5	Rewards
UI_CLONE_TABLES	products* pricing* inventory merchandising options:sku	Item display
MAILING_TO	{FNAME} {LNAME} <{EMAIL}>	Mailings
AMBASSADOR_MONITOR	samp@webmaint.com	General
SECURE_ENABLE	1	General
ENCRYPTOR		Encryption
LABEL_SENT_BY	39 Dollar Glasses 1-800-672-6304	Shipping
UI_IMAGE_DIR	/interchange-5/	Admin control
BIZRATE_ENABLE	1	Company
COD_ACCEPTED	0	Payment
MAILING_FROM	39dollarglasses.com <service@39dollarglasses.com>	Mailings
TAXAREA		Tax
UI_HELP_HEIGHT	480	Admin layout
DECREMENT_INVENTORY	1	Order
UPS_POSTCODE_FIELD	zip	Shipping
UI_META_LINK	1	Admin control
CREDIT_CARDS_ACCEPTED	visa mc discover amex 	Payment
UI_HELP_WIDTH	650	Admin layout
MV_MAILFROM	orders@39dollarglasses.com	Company
UI_ITEM_TABLES	products pricing inventory merchandising options:sku	Item display
GENERIC_DESCRIPTION	Discount prices on prescription eyeglasses and sunglasses online.	SEO
UI_COMPONENT_DIR	templates/components	Directories and Paths
QUERY_FORM_CUST_TO	corporate@39dollarglasses.com	Company
UI_ADMIN_TABLES	mv_metadata locale variable dict ichelp route access	Admin control
UI_PAGE_DIR	pages	Directories and Paths
QUERY_FORM_TO	contact@39dollarglasses.com	Company
MV_PAYMENTECH_TRANSBIN	000002	Payment
MV_PAYMENTECH_INDUSTRY_TYPE	EC	Payment
DEFAULT_REWARD	4	Rewards
IMAGE_MOGRIFY	/usr/bin/mogrify	Directories and Paths
UYOF_SHIP_MODE	DOM2	Shipping
CONTRAST	RED	Template
PO_ACCEPTED	0	Payment
UI_SECURE	1	Admin control
MV_ERROR_STD_LABEL	<span style="color:red">{LABEL}<!-- <small><i>(%s)</i></small> --></span>[else]{REQUIRED <b>}{LABEL}{REQUIRED </b>}[/else]	Cart/Checkout
POSTAL_ACCEPTED	0	Payment
UI_IMAGE_DIR_SECURE	/interchange-5/	Admin control
CHECK_ACCEPTED	0	Payment
COMPANY	39dollarglasses.com	Company
STYLE	reloaded	Template
UI_TEMPLATE_DIR	templates	Directories and Paths
CLIP_PRICE	12.95	Addon Pricing
