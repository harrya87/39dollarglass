UserTag ab_log               Order        flag
UserTag ab_log               addAttr
UserTag ab_log               attrAlias    base mv_ib
UserTag ab_log               PosNumber    1
UserTag ab_log               Routine   <<EOR
sub {
	my ($flag, $ref) = @_;
	$ref->{flag} = $ref->{flag} || $ref->{page} || $flag;

	return if $Session->{browser} =~ /ScanAlert|Chirp/ || $Session->{browser} eq '';

	#Escape if this tag has been called a second time by a single page (won't count for AJAX calls)
	return if $Scratch->{'ab-tracked'};

	if( $ref->{flag} ){
		$Tag->perl({ tables => 'ab_testing_log products' });

		my %columns = (
			sessionid => $Session->{id},
			ip				=> $Session->{host},
			browser 	=> $Session->{browser},
			page 			=> $ref->{flag},
		);

		#Don't set the ab column if there is no ab running (turns the it into more of a stats log)
		$columns{'ab'}						= $Session->{ab} if $Tag->var('AB_TEST_RUNNING');
		$columns{'order_number'} 	= $ref->{order} if exists $ref->{order};
		$columns{'page_option'} 	= $ref->{option} if exists $ref->{option};

		my @order = keys %columns;
		my $sql = "INSERT INTO ab_testing_log (";

		$sql .= "$_," foreach @order;
		chop $sql; #Remove trailing comma
		$sql .= ") VALUES (";
		$sql .= $Tag->filter('dbi_quote', $columns{$_}) . ',' foreach @order;
		chop $sql; #Remove trailing comma
		$sql .= ")";

		$Tag->query({ hashref => 'ablog', sql => $sql });

		$Tag->tmpn('ab-tracked',1);
	}
	return undef;
}
EOR