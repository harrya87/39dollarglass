UserTag ab_multi AddAttr
UserTag ab_multi Routine <<EOR
#mainline code
sub {
        #get incoming parameters
        my ($parameters) = @_ ;
        
        #create variables and initialize for database calls
        my $query = sprintf(qq|SELECT id FROM multivariate_test WHERE current = ? LIMIT 0,1|);
        my @columns = ('id'); 
        my $parm = '1';
        my $attribute = 'id';       
        my $current_id = run_query_return_result($query,\@columns,$parm,$attribute);
      
        #recreate and initialize in order to run second query to get next session var
        my $query = sprintf(qq|SELECT MAX(id) AS id FROM multivariate_test LIMIT 0,1|);
        my @columns = ('id'); 
        my $parm = undef;
        my $attribute = 'id';
        my $max_id = run_query_return_result($query,\@columns,undef,$attribute);

        #logic to determine next session var
        my $next_var = (($current_id eq $max_id) ? '1' : $current_id + 1);#if there is no return value for some reason, the calc will be 0+1, and the prcoess will effectively reset itself
        my $query = sprintf(qq|SELECT variable FROM multivariate_test WHERE id = ? LIMIT 0,1|);
        my @columns = ('variable'); 
        my $parm = $next_var;
        my $attribute = 'variable';
        my $new_mvt_var = run_query_return_result($query,\@columns,$parm,$attribute);
   
        #recreate and initialize in order to run second query to get next session var
        my $table = 'products';#cant get a handle on new table for some reason, so we generate handle from products table
        $Tag->perl({tables => $table});
        my $dbh = $Sql{$table} or die "No DBH Handle $!";
        $dbh->do(q|UPDATE multivariate_test SET current = '1' WHERE id = ?|,undef, $next_var);
        $dbh->do(q|UPDATE multivariate_test SET current = '0' WHERE id = ?|,undef, (($next_var eq '1' ) ? $max_id : $next_var - 1));

        #####return value#####
        return $new_mvt_var;

    sub run_query_return_result{
            #get paramters for query
            my ($query,$columns,$parm,$attribute) = @_;
            my $dbh;
            my $sth;
            my $table = 'products';#cant get a handle on new table for some reason, so we generate handle from products table
            my @columns_array = @$columns;
            
            #get database handle 
            $Tag->perl({tables => $table});
            $dbh = $Sql{$table} or die "No DBH Handle die $!";
            
            #prepare statement and execute
            $sth = $dbh->prepare($query) or die "Can't get statement handle: $!";
            if($parm){
                $sth->execute($parm) or die "Error executing query: $!";
            } else {
                $sth->execute() or die "Error executing query: $!";
            }
            
            #iterate through results
            my $results;
            while (my $row = $sth->fetchrow_arrayref){
                my $count = 0;
                my $ref;
                for (@$row){
                    $ref->{$columns_array[$count++]} = $_;
                }
                push @$results, $ref;
            }
            
            #actual processing
            my $return_value;
            foreach my $res (@$results){
                $return_value .= $res->{$attribute};
            }
            
            #cleanup objects
            undef $query;
            undef $columns;
            undef $parm;
            undef $attribute;
            undef $dbh;
            undef $sth;
            undef $table;
            undef @columns_array;
            
            #return query result
            return $return_value;
    }
}
EOR
