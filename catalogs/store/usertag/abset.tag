UserTag abset Order
UserTag abset AddAttr
UserTag abset Routine <<EOR

sub {

	if($Session->{id} ne 'nsession')
	{
		if($Session->{ab} && !$CGI->{forceab})
		{
			#$Tag->ab_log({flag=>'flag10'}); ##log the assignment and exit
			return undef;
		}
	
		if ($CGI->{forceab}){
			$Session->{ab} = $CGI->{forceab};
		} else {
			# see if its been set in a cookie previously..
			my $ab = $Tag->read_cookie({ name => 'ab', });
			if ($ab){
				$Session->{ab} = $ab;
			}
			else {
			  #remove random setting of ab		
				#$Session->{ab} = (int(rand(100)) % 2) ? 'a' : 'b';
				    #turn ab fifty fifty to mvt
				        #$Session->{ab} = (int($Tag->counter({ sql=>'abtesting_counter:abtesting_counter' })) % 2) ? 'a' : 'b';
                        $Session->{ab} = $Tag->ab_multi({});
			}
		}
		# and set to cookie..
		$Tag->set_cookie({ 	name => 'ab',
					value => "$Session->{ab}",
					});
		$Tag->ab_log({ flag=>'entry', option => $Tag->env('REQUEST_URI') }); ##log the assignment
					
	}

	return undef;
}
EOR


