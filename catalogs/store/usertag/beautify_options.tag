UserTag beautify-options hasEndTag
UserTag beautify-options addAttr
UserTag beautify-options Routine <<EOR
sub {
	my $opt = $_[0];
	local $_ = $_[1];

	# remove unneeded options
	s~\bvarilux:\s*(?:<br>|$)~~g;
	s~\b(?:lens_)?coating:\s*(?:No\s*)?(?:<br>|$)~~gi;

	# make list and boldface if HTML
	my $before = my $after = '';
	if( $opt->{html} ){
		$before = exists $opt->{before}	? $opt->{before} : '<li><b>';
		$after = exists $opt->{after}		? $opt->{after} : '</b></li>';
	}

	# beautify remaining options
	s~\blenstype:~${before}Lens type:$after~g;
	s~\bvarilux:~${before}Varilux:$after~g;
	s~\b(?:lens_)?coating:~${before}AR coating?$after~g;
	s~\bIncluded free~Yes~g;
	s~\btint:~${before}Tint:$after~g;
	s~\blens:~${before}Lens:$after~g;
	s~\buyof_add:~${before}Rimless Frames?$after~g;
	s~\bcase:~${before}Case?$after~g;

	if( $opt->{stripEmpty} ){
		s~${before}AR coating\?${after}\sNone<br>~~g;
		s~${before}Tint:${after}\s+Clear<br>~~g;
	}
	
	if (! $opt->{html}) {
		$_ = $Tag->filter('html2text', $_);
		# strip leading/trailing whitespace on each line
		s/^[ \t]+/$1/gm;
		s/[ \t]+$/$1/gm;
		# make sure no newline at end of string
		s/\s+$//;
	}

	return $_;
}
EOR
