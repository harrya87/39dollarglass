UserTag build_insert Order	table	columns
UserTag build_insert Routine <<EOR
sub{
	my ($table, @columns) = @_;
	my $cols = join(',', @columns);
	my $values = join(',', map("?", @columns) );
	
	return qq|INSERT INTO $table ($cols) VALUES ($values)|;
}
EOR


