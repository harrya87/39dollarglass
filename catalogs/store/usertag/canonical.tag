UserTag canonical Routine <<EOR
sub {
	my $out = 'http://' . $Variable->{SERVER_NAME} . '/';
	if ($Scratch->{canonical})
	{
		$out .= $Scratch->{canonical};
		$Scratch->{canonical} = undef;
	}
	else
	{
		$out .= $Scratch->{thispage};
	}

	#&Log('OUT: ' . $out);

	$out .= '.html' unless $out =~ /\.html?$/;
	return $Tag->filter('encode_entities',$out);
}
EOR
