UserTag category_page Routine <<EOR
sub {
	my ($category, $data_only, $opt) = @_;
	my $where;
	my $hidden_input;
	my @split_values;
	my @split_url;
	my $sql;
	my $page_link = $Tag->filter('encode_entities', $Tag->env('SCRIPT_URL'));
	my $db = &Vend::Data::database_exists_ref('products');
	$db = $db->ref();
	my $dbh = $db->[$Vend::Table::DBI::DBI];

	my $locked_sql;
    my $secondary_color;
    my $tertiary_color;

	my $generic_refinement_list = {
		'category'        => '',
		'gender'          => '',
		'primary_color'   => '',
		'lens_shape'	  => '',
	};

	my $complex_refinement_list = {
		'tot_frame_width' => '',
		'temple_length'   => '',
		'options_lens'	  => '',
	};
	
	unless ($Scratch->{show_all}){
	    $Scratch->{show_all} = $CGI->{submittedShowAll} && $CGI->{show_all} ? '1' : '0';
	} else {
	    delete $Scratch->{show_all} if ($CGI->{submittedShowAll} && !$CGI->{show_all});
	}
	 
	if ($page_link =~ /[eye|sun]glasses\/(.*).html/ || $Scratch->{'post_page_link'} =~ /(.*)/) {
		foreach my $url_name (split('-', $1)) {
			$sql = qq|SELECT c.queryName, ci.value FROM category c INNER JOIN categoryItems ci ON c.id = ci.categoryId WHERE ci.urlName = ?|;
			my $sth = $dbh->prepare($sql);
			$sth->execute($url_name);

			while (my $result = $sth->fetchrow_hashref()) {
				if (exists $generic_refinement_list->{$result->{'queryName'}}) {
					if (lc($result->{'queryName'}) eq 'lens_shape') {
						$generic_refinement_list->{$result->{'queryName'}} .= $generic_refinement_list->{$result->{'queryName'}} ? q|, | . $Tag->filter('dbi_quote', $result->{'value'}) : qq|( fgpt.description = 'Lens Shape' AND fgpv.value in (| . $Tag->filter('dbi_quote', $result->{'value'}); 
					}
					elsif (lc($result->{'queryName'}) eq 'primary_color'){
						$generic_refinement_list->{'primary_color'} .= $generic_refinement_list->{'primary_color'} ? q|, | . $Tag->filter('dbi_quote', $result->{'value'}) : qq|primary_color IN (| . $Tag->filter('dbi_quote', $result->{'value'});
						$secondary_color .= $secondary_color ? q|, | . $Tag->filter('dbi_quote', $result->{'value'}) : qq|secondary_color IN (| . $Tag->filter('dbi_quote', $result->{'value'});
						$tertiary_color .= $tertiary_color ? q|, | . $Tag->filter('dbi_quote', $result->{'value'}) : qq|tertiary_color IN (| . $Tag->filter('dbi_quote', $result->{'value'});
					}
					else {
						$generic_refinement_list->{$result->{'queryName'}} .= $generic_refinement_list->{$result->{'queryName'}} ? q|, | . $Tag->filter('dbi_quote', $result->{'value'}) : qq|p.$result->{'queryName'} IN (| . $Tag->filter('dbi_quote', $result->{'value'}); 
					}
				}
				elsif (exists $complex_refinement_list->{$result->{'queryName'}}) {
					@split_values = split('/', $result->{'value'});
					if (@split_values > 1){
						$complex_refinement_list->{$result->{'queryName'}} .= ($complex_refinement_list->{$result->{'queryName'}} ? q| OR | : '') .
							qq| p.$result->{'queryName'} BETWEEN cast(| . $Tag->filter('dbi_quote', @split_values->[0]) . q| as unsigned integer) AND cast(| . $Tag->filter('dbi_quote', @split_values->[1]) . q| as unsigned integer)|;
					}
					else {
						$complex_refinement_list->{$result->{'queryName'}} .= ($complex_refinement_list->{$result->{'queryName'}} ? q| OR | : '') .
							(lc($result->{'queryName'}) eq 'options_lens' ? qq| p.$result->{'queryName'} like | . $Tag->filter('dbi_quote', '%' . $result->{'value'} . '%') : qq| p.$result->{'queryName'} = | . $Tag->filter('dbi_quote', $result->{'value'}));
					}
				}
			}
		}
	}

	$generic_refinement_list->{'lens_shape'} .= ")" if $generic_refinement_list->{'lens_shape'};

	# Add unisex to it if there is an existing gender or genders since the items that are listed as mens or womens only are also accounted for as unisex.
	$generic_refinement_list->{'gender'} .= qq|, 'Unisex'| if $generic_refinement_list->{'gender'};

	#set the last browsing gender to set as default on prodcuct page
	if (my $gender = $generic_refinement_list->{'gender'}){
		$gender =~ s/^p.gender * IN *\(//;
		$gender =~ s/[^A-Za-z ]//g;
		my @genders = split / +/, $gender;
		if (@genders > 2){
			delete $Scratch->{productCatFilter};
		}else {
			$Scratch->{productCatFilter} = (grep $_ eq 'Women', @genders) ? 'w' : 'm';
		}
	}

	foreach my $generic_refinement_key (keys%{$generic_refinement_list}) {
		if ($generic_refinement_key eq 'primary_color') {
			$where .= qq|AND ($generic_refinement_list->{$generic_refinement_key}) OR $secondary_color) OR $tertiary_color)) | if $generic_refinement_list->{$generic_refinement_key};
		}
		else {
			$where .= qq|AND $generic_refinement_list->{$generic_refinement_key}) | if $generic_refinement_list->{$generic_refinement_key};
		}
	}

	foreach my $complex_refinement_key (keys%{$complex_refinement_list}) {
		$where .= qq|AND ($complex_refinement_list->{$complex_refinement_key}) | if $complex_refinement_list->{$complex_refinement_key}; 
	}

	my $new_arrivals = ($page_link =~ /new\-arrivals/ && !$Scratch->{show_all}) ? qq|AND cast(p.date_added as datetime) >= cast("| . $Tag->var('NEW_ARRIVALS_DATE_ADDED') . qq|" as datetime)| : '';
	my $kids = $page_link =~ /kids/ ? '' : q\ AND p.category != 'Kids' \;

	if ($Scratch->{kidsglasses}) {
		$where .= qq|AND p.price = 39 |;
	}

	$sql = qq|
		SELECT COUNT(DISTINCT p.sku) AS count 
			FROM frames f
		INNER JOIN products p ON p.sku = f.sku
		INNER JOIN frameGroupProperties fgp ON fgp.partialSku = f.partialSku
		INNER JOIN frameGroupPropertyTypes fgpt ON fgp.frameGroupPropertyTypesID = fgpt.frameGroupPropertyTypesID
		INNER JOIN frameGroupPropertyValues fgpv ON fgp.frameGroupPropertyValuesID = fgpv.frameGroupPropertyValuesID
		WHERE p.inactive != 1 AND p.sku RLIKE '^[0-9]+_'
			$kids
			$new_arrivals
			$where
			$locked_sql
	|;

	my $total_results = $dbh->selectrow_array($sql);
	if( $total_results <= 0 ){
		$Tag->tmp('noResults',1);
		return qq|<span>There are no results.  Try removing filters to view our products.</span>|;
	}

	my $items_per_page = $Tag->cgi('ipp') ? $Tag->cgi('ipp') : 24;
	my $current_page_number = (!$Tag->cgi('page_num') || (($Tag->cgi('page_num') - 1) * $items_per_page) > $total_results) ? 1 : $Tag->cgi('page_num');

	my %sortOptions = (
		'calculated_sr desc' => 1,
		'price asc' => 1,
		'price desc' => 1,
		'date_added' => 1,
		'description desc' => 1,
		'description asc' => 1
	);
	my $sort = $sortOptions{ $CGI->{'sort'} } ? $CGI->{'sort'} : undef; 
	my $order_by_cgi = 
		$sort ? $sort : 
			($Scratch->{'sunglasses'} || ( exists $CGI->{lens_tint} && $Tag->cgi('lens_tint') != 'CLEAR' ) ? q|CAST(effective_sgsr AS decimal(10,3)) desc| : q|CAST(effective_sr AS decimal(10,3)) desc|);

	my $order_by = q|order by | . ($order_by_cgi eq 'price' ? q|cast(price as unsigned integer)| : ( $order_by_cgi eq 'date_added' || $new_arrivals ? 'date_added DESC' : $order_by_cgi ) );
	my $limit = q|limit | . $items_per_page * ($current_page_number - 1) . q|, | . $items_per_page; 
	my $page_list = ceil($total_results / $items_per_page) ne 1 ? GetPageList(ceil($total_results / $items_per_page), $current_page_number) : '';

	$sql = qq|
		SELECT DISTINCT f.sku, p.description, p.retail_price, IFNULL( ad.discounted_price, p.price ) AS price, IFNULL( ad.original_price, p.price ) AS original_price, p.ven_color
			FROM frames f
		INNER JOIN products p ON p.sku = f.sku
		LEFT JOIN active_discounts ad ON p.sku = ad.sku
		INNER JOIN frameGroupProperties fgp ON fgp.partialSku = f.partialSku
		INNER JOIN frameGroupPropertyTypes fgpt ON fgp.frameGroupPropertyTypesID = fgpt.frameGroupPropertyTypesID
		INNER JOIN frameGroupPropertyValues fgpv ON fgp.frameGroupPropertyValuesID = fgpv.frameGroupPropertyValuesID
		WHERE p.inactive != 1 AND p.sku RLIKE '^[0-9]+_'
			$kids
			$new_arrivals
			$where
			$locked_sql
			$order_by
			$limit
	|;

	my $sth = $dbh->prepare($sql);

	$sth->execute();

	my $lens_opts = GetLensTints();
	my $pop_opt = GetPopularity();
	my $ipp_opt = GetItemPerPage();
	my $page = $CGI->{page_num};
	my $pagination = qq|
		<div class="mid_box">
			<form class="sort" method="post" action="$page_link">
				<input name="page_num" type="hidden" value="$page" />
				<div class="result_text">$total_results Results</div>
				<div class="lens_text">
					<span>Lens Tint :</span>
					<select id="lens_tint_input" name="lens_tint">
						$lens_opts
					</select>
				</div>
				<div class="sort_text">
					<span>Sort :</span>
					<select id="sort_by_input" name="sort">
						$pop_opt
					</select>
				</div>
				<div class="item_text">
					<span>Items Per Page :</span>
					<select id="ipp_input" name="ipp">
						$ipp_opt
					</select>
				</div>
				$page_list
			</form>
		</div>|;

	my $output = qq|
		$pagination
		<div class="product_cont">
	|;

	while (my $result = $sth->fetchrow_hashref()) {
		my ($digits) = $result->{sku} =~ /^(\d+)/;
		$sql = qq|
			SELECT fpv.value
			FROM framePropertyValues fpv
			INNER JOIN frameProperties fp
				ON fp.framePropertyValuesID = fpv.framePropertyValuesID
			INNER JOIN framePropertyTypes fpt
				ON fpt.framePropertyTypesID = fp.framePRopertyTypesID
			WHERE fp.sku RLIKE "^${digits}_" AND fpt.description = "Frame Color";
		|;
		my $frame_color_results = $dbh->prepare($sql);

		$frame_color_results->execute();

		my $frame_color_options;

		while (my $frame_color_result = $frame_color_results->fetchrow_hashref()) {
			$frame_color_result->{value} =~ s/\//_/g;
			if($frame_color_result->{value} eq 'Clear'){
				$frame_color_result->{value} = 'White';
			}
			$frame_color_options .= qq|
				<img src="/store/images/new_site/frame_color_$frame_color_result->{value}.png" alt="$frame_color_result->{value} Color" />
			|;
		}

		my $lens_tint = $Tag->cgi('lens_tint') ? $Tag->cgi('lens_tint') : ($Scratch->{'sunglasses'} ? "GREY" : "CLEAR");
		my $sunglasses_link;
		my $product_lens_tint;
		if (lc($lens_tint) ne "clear") {
			$sunglasses_link = "sun-glasses/";
			$product_lens_tint = "lens_tint=$lens_tint";
		}

		my $product_href = $Tag->area({ href=>"$sunglasses_link$result->{'sku'}", form=>"$product_lens_tint" });
		my $item_price = lc($lens_tint) ne "clear" ? $Tag->eyeglasses_price( { sku=>$result->{'sku'}, sunonly=>1, noformat=>1 }) : $result->{'price'};
		my $image_src = $Tag->image({ src_only=>1, src=>"tints/$result->{'sku'}_$lens_tint.jpg", makesize=>"200x200" });

		#Check if this price is currently discounted, if so output the original price with an additional strikethrough
		my $prices = $result->{price} != $result->{original_price} ?
		q|
		<div class="product_bprice"><s>Our Price |.$Tag->currency({ body => lc($lens_tint) ne "clear" ? $Tag->price( { code=>$result->{'sku'}, price=>$result->{original_price}, sunonly=>1, noformat=>1 }) : $result->{original_price} }).q|</s></div>
		<div class="product_bprice">Intro Price |.$Tag->currency({ body => $item_price }).q|</div>
		| : q|<div class="product_bprice">Our Price |.$Tag->currency({ body => $item_price }).q|</div>|;

		my $retail_price = $Tag->currency({body => $result->{'retail_price'}});
		my $url = 
		$output .= qq|
			<div class="product_box">
				<a href="#" class="try_on"><img alt="" /></a>
				<div class="available_colors">
					<span>Available Colors :</span>
					$frame_color_options
				</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="height:114px;">
				  <tr>
				    <td align="center" valign="middle">
				    	<a href="$product_href">
				    		<img src="$image_src" alt="" />
				    	</a>
				    </td>
				  </tr>
				</table>
				<h5><a href="$product_href">$result->{description}</a></h5>
				<div class="product_btext">$result->{ven_color}</div>
				<div class="product_btext"><s>Retail Price $retail_price</s></div>
				$prices
				<a href="#" onclick="return !1;" id="moreDetails-$result->{'sku'}" class="quick_info">quick info</a>
				<a href="$product_href" class="order_now">order now</a>
			</div>|;
	}

	$output .= qq|</div>| . ($total_results > 6 ? $pagination : '');

	return $output;

	sub GetPageList{
		my ($total_pages, $current_page) = @_;

		my $page_loop_start = $current_page - 1 > 1 ? $current_page - 1 : 1;
		my $page_loop_end = $current_page + 1 < $total_pages - 1 ? $current_page + 1 : $total_pages;
		my $previous_page = $current_page - 1;
		my $next_page = $current_page + 1;
		my $show_all = $CGI->{show_all} ? '1' : '0';
		my $output = qq|<ul class="pagination"><input name="show_all" type="hidden" value="$show_all" />| .
			($current_page != 1 ? qq|<li><a href="#" class="pageNumber" name="$previous_page" onclick="return false;">&laquo; Prev</a></li>| : '') .
			($page_loop_start != 1 ? (qq|<li><a href="#" class="pageNumber" name="1" onclick="return false;">1</a><span>&hellip;</span></li>|) : '');

		foreach($page_loop_start .. $page_loop_end){
			$output .= $_ == $current_page ? qq|<li><a href="#" class="active notLink">$_</a></li> \n| : qq|<li><a href="#" class="pageNumber" name="$_" onclick="return false;">$_</a></li>|;
		}

		$output .= ($page_loop_end != $total_pages ? qq| <li><a href="#" class="notLink">&hellip;</a></li><li><a href="#" class="pageNumber" name="$total_pages" onclick="return false;">$total_pages</a></li>| : '') .
				   ($current_page != $total_pages ? (qq|<li><a href="#" class="pageNumber" name="$next_page" onclick="return false;">Next &gt;</a></li>|) : '');

		return $output . q|</ul>|;
	}

	sub GetLensTints{
		my $options = [
			{ 'value' => 'CLEAR',  'display' => 'Clear'  },
			{ 'value' => 'BROWN',  'display' => 'Brown'  },
			{ 'value' => 'GREY',   'display' => 'Grey'   },
			{ 'value' => 'G15',    'display' => 'Green'  },
			{ 'value' => 'BLUE',   'display' => 'Blue'   },
			{ 'value' => 'ROSE',   'display' => 'Rose'   },
			{ 'value' => 'ORANGE', 'display' => 'Orange' },
			{ 'value' => 'YELLOW', 'display' => 'Yellow' },
		];

		my $default_lens_tint = $Scratch->{'sunglasses'} ? 'GREY' : 'CLEAR';
		return SortOptionList($options, $Tag->cgi('lens_tint'), $default_lens_tint);
	}

	sub GetPopularity{
		my $options;
		my $default_sort;
		if( $Scratch->{'sunglasses'} || ( exists $CGI->{lens_tint} && $Tag->cgi('lens_tint') != 'CLEAR') ){
			$default_sort = 'calculated_sgsr desc';
			$options = [
				{ 'value' => 'calculated_sgsr desc', 'display' => 'Best Seller' },
				{ 'value' => 'price desc',              'display' => 'Price: High to Low' },
				{ 'value' => 'price asc',   'display' => 'Price: Low to High'  },
				{ 'value' => 'description asc',    'display' => 'Name (A-Z)'  },
				{ 'value' => 'description desc',   'display' => 'Name (Z-A)'  },
				{ 'value' => 'date_added',           'display' => 'Date Added'  },
			];
		}
		else{
			$default_sort = 'calculated_sr desc';
			$options = [
				{ 'value' => 'calculated_sr desc', 'display' => 'Best Seller' },
				{ 'value' => 'price desc',              'display' => 'Price: High to Low' },
				{ 'value' => 'price asc',   'display' => 'Price: Low to High'  },
				{ 'value' => 'description asc',    'display' => 'Name (A-Z)'  },
				{ 'value' => 'description desc',   'display' => 'Name (Z-A)'  },
				{ 'value' => 'date_added',           'display' => 'Date Added'  },
			];
		}

		return SortOptionList($options, $Tag->cgi('sort'), ($new_arrivals ? 'date_added' : $default_sort));
	}

	sub GetItemPerPage{
		my $options = [
			{ 'value' => '12',  'display' => '12'  },
			{ 'value' => '24',  'display' => '24'  },
			{ 'value' => '48',  'display' => '48'  },
			{ 'value' => '96',  'display' => '96' },
		];

		return SortOptionList($options, $Tag->cgi('ipp'), 24);
	}

	sub SortOptionList{
		my ($options, $cgi_value, $default_value) = @_;
		my $output;
		my $selected;
		my $prev_selected_value = $cgi_value ? $cgi_value : $default_value;

		foreach my $option (@$options) {
			$selected = $prev_selected_value eq $option->{'value'} ? "selected" : "";
			$output .= qq|<option value="$option->{'value'}" $selected>$option->{'display'}</option>|;	
		}

		return $output;
	}
}
EOR

