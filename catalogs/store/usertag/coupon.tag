UserTag coupon Order code redeem
UserTag coupon Alias code coupon_code
UserTag coupon HasEndTag
UserTag coupon addAttr
UserTag coupon Routine <<EOR
sub {
	my ($code, $redeem, $opt) = @_;

	$Tag->perl({ tables => 'coupon_codes products ambassador_user' });
	
	if ($code eq 'BuyOneGetOne50' && !$Scratch->{bogo_active}){
		$code = '';	
	}
	if ($code && ref $Scratch->{mask_coupon} eq 'HASH' && $code eq $Scratch->{mask_coupon}->{mask}){
		$code = $Scratch->{mask_coupon}->{code};
	}

	my $quotedCode = $Tag->filter('dbi_quote',$code);
	
	
	if( $redeem ){
		my $db = $Db{coupon_codes};
		my $coupon = $db->query({
			sql => "SELECT * FROM coupon_codes WHERE code = $quotedCode",
			hashref => 1,
			table => 'coupon_codes'
		})->[0];

		if( $coupon->{orig_qty} ){
			$db->query({ 	sql => "UPDATE coupon_codes SET qty_remain = " . ($coupon->{qty_remain} - 1) . " WHERE code = $quotedCode", table => 'coupon_codes' });
		}

		if( $code =~ /UHC\d\d\d\d/ ){
			$Tag->email({
				to => 'drmarlow@39dollarglasses.com, drmarcweinstein@39dollarglasses.com, jmellow@39dollarglasses.com',
				from => '39DG Website <website@39dollarglasses.com>',
				subject => 'Priority Coupon Code Usage Notice',
				body => "This notice is to inform you of a recent transaction where coupon code $code was used. The associated order number is " . $Values->{mv_order_number} . ".\n\nThank you!",
			});
		}

		return;
	}

	$code = $code || $CGI->{mv_coupon_code} || $Values->{mv_coupon_code};
	return undef unless $code;	

	%$::Discounts = ();
	delete $Vend::Session->{discount};
	delete $Vend::Session->{current_coupon_code};
	delete $Scratch->{shipping_adder};
	delete $Scratch->{disc_amount};
	delete $Scratch->{pre_disc_st};
	delete $Scratch->{shipping_coupon};
	
	my $yes = $opt->{yes} || undef;
	my $no = $opt->{no} || undef;
	my $msgscratch = $opt->{msgscratch} || 'coupon_message';
	
	my $db = $Db{coupon_codes};
	my $coupon = $db->query({
		sql => "SELECT * FROM coupon_codes WHERE code = $quotedCode",
		hashref => 1,
		table => 'coupon_codes'
	})->[0];

	my $curr_dt = $Tag->convert_date({fmt => '%Y%m%d'});

	#Reject the coupon IF:
	if(
		!$coupon->{code} 		|| #No matching coupon was found OR
		!$coupon->{enabled} || #The coupon is not enabled OR
		( $coupon->{start_dt} && $curr_dt < $coupon->{start_dt} ) || #The coupon hasn't started yet OR
		( $coupon->{end_dt} && $curr_dt > $coupon->{end_dt} )				 #The coupon has expired
	){
		$Scratch->{$msgscratch} = '<!-- Err-C: -->You have entered an invalid code. Please try again.';
		return $no;
	}
	
	#Reject if there are no more remaining
	if( $coupon->{orig_qty} && $coupon->{qty_remain} < 1 ){
		$Scratch->{$msgscratch} = '<!-- Err-Q: -->You have entered an invalid code. Please try again.';
		return $no;
	}                                     
	#Reject if the coupon is username specific and this isn't the user it's intended for
	if( $coupon->{username} && $Session->{username} ne $coupon->{username} ){
		$Scratch->{$msgscratch} = '<!-- Err-U: -->You have entered an invalid code. Please try again.';
		return $no;
	}
	#Reject if it's an affiliate coupon and this customer did not come through the affiliate
	if( $coupon->{affiliate} && $Session->{source} ne $coupon->{affiliate} ){
		$Scratch->{$msgscratch} = '<!-- Err-A: -->You have entered an invalid code. Please try again.';
		return $no;
	}
	#Reject if the coupon is of an unknown type
	if( $coupon->{coup_type} !~ /^greater|pct|amt$/ ){
		$Scratch->{$msgscratch} = '<!-- Err-D: -->There is a problem with this code - please call us to redeem.';
		return $no;
	}

	$Scratch->{pre_disc_st} = $Tag->subtotal({ noformat => 1 });

	#Reject if the coupon has a minimum subtotal and the cart does not currently reach that limit
	if( $coupon->{apply_over_amt} && $Scratch->{pre_disc_st} < $coupon->{apply_over_amt} ){
		$Scratch->{$msgscratch} = '<!-- Err-ST: -->This code requires that the pre-discounted order subtotal be greater than or equal to ' . $Tag->currency({ body => $coupon->{apply_over_amt} }) . '.';
		return $no;
	}
	if ($code eq $Scratch->{mask_coupon}->{code} && $Scratch->{bogo_active} && scalar@{$::Carts->{main}} < 2){
		$Scratch->{$msgscratch} = 'This coupon requires there be at least two items in your cart.';
		$Session->{'recheck_coupon'} = $coupon->{code};
		return $no;
	}
	my $company = $db->query({sql => 'select b_company from ambassador_user a join userdb u using(email) where coupon = '.$quotedCode, hashref=>1});
	#Replace tokens in description
	my %token_hash = (
		discount => $coupon->{coup_type} eq 'pct' ? $coupon->{coup_amt}.'%' : '$'.$coupon->{coup_amt},
		company => ($company->[0]->{b_company} || ''),
		ambass_reward => '$'.$Tag->var('AMBASSADOR_REWARD_DONATIONS'),
		ambass_discount => $Tag->var('AMBASSADOR_COUPON_PERCENTAGE').'%',
	);
	my $max_tokens = 0;
	my $new_desc = $coupon->{description};
	while ($new_desc =~ /(#([^# ]+)#)/ && $max_tokens < 5){
		my ($var, $replace) = ( $token_hash{lc($2)}, $1 );
		$new_desc =~ s/$replace/$var/;
		++$max_tokens;
	}	
	$coupon->{description} = $new_desc;

	$Values->{campaign} = $coupon->{campaign} if $coupon->{campaign};
	$Values->{salesman} = $coupon->{salesman} if $coupon->{salesman};

	$Scratch->{disc_amount} = 0;

	my $cart = 'main';
	if ($coupon->{free_ship}){
		$Scratch->{shipping_adder} = '-4.95';
		$Scratch->{shipping_coupon} = 1;
	}

	#If the coupon is applicable on a per item basis AND there are no items in the cart
	if( $coupon->{type} =~ /^highest_item|lowest_item|item|all_items$/ && scalar@{$::Carts->{main}} < 1 ){
		#Store the code for the basket page to recall this tag and exit early
		$Session->{'recheck_coupon'} = $code;
		return $no;
	}

	if( $coupon->{type} eq 'highest_item' ||  $coupon->{type} eq 'lowest_item'){
		my (
			@cartitems,
			$itemToDiscount,
			$quantityToDiscount,
			$discountAmount
		);
		my  $item_count = 0;
		foreach my $item( @{$::Carts->{$cart}} ){
			delete $item->{lowest_price};
			push @cartitems, {
				sku => $item->{code},
				price => $Tag->eyeglasses_price({
					sku => $item->{code},
					lens => $item->{lens},
					lenstype => $item->{lenstype},
					tint => $item->{tint},
					coating => $item->{coating},
					varilux => $item->{varilux},
					uyof_add => $item->{uyof_add},
					case => $item->{case},
					sunonly => $item->{sunonly},
					packageonly => $item->{packageonly},
					clips => $item->{clips},
					noformat => 1,
				}),
				qty => $item->{quantity},
				backref => $item
			};
			++$item_count;
		}
		if ($coupon->{type} eq 'highest_item'){
			@cartitems = sort { $b->{price} <=> $a->{price} } @cartitems;
		}else{
			@cartitems = sort { $a->{price} <=> $b->{price} } @cartitems;
		}
		$itemToDiscount = $cartitems[0];
		$itemToDiscount->{backref}->{lowest_price} = 1
			if $item_count > 1;
		
		#If the coupon specifies how many should be discounted
		if( $coupon->{coup_item_disc_qty} ){
			#Store the number of items to be discounted, either the number specified or the quantity of this item in the cart
			$quantityToDiscount = $coupon->{coup_item_disc_qty} > $itemToDiscount->{qty} ? $itemToDiscount->{qty} : $coupon->{coup_item_disc_qty};
		}
		else{
			$quantityToDiscount = $itemToDiscount->{qty};
		}
		#$quantityToDiscount = 1 if $coupon->{type} eq 'lowest_item';
		#If the coupon type is percent off
		if ($coupon->{coup_type} eq 'pct'){
			# &Log('percent amount: ' . $coupon->{coup_amt});
			my $pct = 1 - ($coupon->{coup_amt} / 100);
			$discountAmount = ($itemToDiscount->{price} * $quantityToDiscount) - (($itemToDiscount->{price} * $quantityToDiscount) * $pct);
		}
		#Otherwise if the coupon is dollar amount based
		else{
			#If the coupon discount is higher than the price of the quantity allowed to be discounted
			if( $coupon->{coup_amt} > ( $itemToDiscount->{price} * $quantityToDiscount ) ){
				#Then set the discount amount to be only what is allowed
				$discountAmount = $itemToDiscount->{price} * $quantityToDiscount;
			}else{
				#Otherwise use the face value of the coupon
				$discountAmount = $coupon->{coup_amt};
			}
			#$discountAmount = $coupon->{coup_amt} > $Scratch->{pre_disc_st} ? $Scratch->{pre_disc_st} : $coupon->{coup_amt};
		}

		#If this coupon has an apply_max (max discount) specified
		#and the current discount exceeds it
		if( $coupon->{apply_max} && $discountAmount > $coupon->{apply_max} ){
			#Use the specified maximum
			$discountAmount = $coupon->{apply_max};
		}

		$Tag->discount('ENTIRE_ORDER','return $s - ' . $discountAmount);

		if ($coupon->{free_ship})
		{
			$Scratch->{shipping_adder} = '-4.95';
			$Scratch->{shipping_coupon} = 1;
		}

		$Scratch->{disc_amount} = $discountAmount;
		$Scratch->{$msgscratch} = 'Code accepted: ' . $coupon->{description};
		$Vend::Session->{current_coupon_code} = $code;
		return $yes;
	}
	elsif ($coupon->{type} eq 'entire_order')
	{
		my @skus = split(/,/, $coupon->{coup_item});
		my $sku_found;

		if (@skus)
		{
			foreach my $i (@{$::Carts->{$cart}})
			{
				#Log("Checking " . $i->{code} . " for match...");
				next unless in_array($i->{code}, \@skus);
				$sku_found = 1;
			}
		}
		elsif ($coupon->{gc_lp})
		{
			$Tag->perl({tables => 'desc_lens_pkg',});
			my $db = $Db{desc_lens_pkg};

			my ($lp_in_cart, $pkg);

			foreach my $i (@{$::Carts->{$cart}})
			{
				my $results = $db->query({ 	sql => "SELECT pkgletter FROM desc_lens_pkg WHERE pkgcode = '" . $i->{lenstype} . "'",
								hashref => 1,
								table => 'desc_lens_pkg'
				});
				$pkg = $results->[0]->{pkgletter};
				#Log('Entire Order coupon with lp constraint found. Current item has lp: ' . $pkg);
				my @pkgs = split(/,/, $coupon->{gc_lp});

				$lp_in_cart = 1 if in_array($pkg, \@pkgs);
				last if $lp_in_cart;
			}
			if (!$coupon->{gc_lp_type} && $lp_in_cart)
			{
				#Log('SKU found with proper lens pkg - valid.');
				$sku_found = 1;
			}
			elsif ($coupon->{gc_lp_type} && !$lp_in_cart)
			{
				#Log('Lens pkg constraint set to excludes and selected LP not found - valid.');
				$sku_found = 1;
			}
		}
		else
		{
			$sku_found = 1;
		}

		if ($sku_found)
		{
			if ($coupon->{coup_type} eq 'greater')
			{
				my $pct = 1 - ($coupon->{coup_amt} / 100);
				my $pdisc = $Scratch->{pre_disc_st} * ($coupon->{coup_amt} / 100);
				my $append;

				if ($pdisc >= $coupon->{coup_amt})
				{
					$append = '(' . $coupon->{coup_amt} . '% Discount Applied)';
					$Tag->discount('ENTIRE_ORDER','return $s * ' . $pct);
				}
				else
				{
					$append = '($' . $coupon->{coup_amt} . ' Discount Applied)';
					$Tag->discount('ENTIRE_ORDER','return $s - ' . $coupon->{coup_amt});
				}
				$Scratch->{disc_amount} = $Scratch->{pre_disc_st} - $Tag->subtotal({ noformat => 1 });
				$Scratch->{$msgscratch} = 'Code accepted: ' . $coupon->{description} . '<br />' . $append;
				$Vend::Session->{current_coupon_code} = $code;
				return $yes;
			}
			elsif ($coupon->{coup_type} eq 'pct')
			{
				my $pct = 1 - ($coupon->{coup_amt} / 100);
				$Tag->discount('ENTIRE_ORDER','return $s * ' . $pct);
			}
			else
			{
				$Tag->discount('ENTIRE_ORDER','return $s - ' . $coupon->{coup_amt});
			}

			$Scratch->{disc_amount} = $Scratch->{pre_disc_st} - $Tag->subtotal({ noformat => 1 });
			$Scratch->{$msgscratch} = 'Code accepted: ' . $coupon->{description};
			$Vend::Session->{current_coupon_code} = $code;
			return $yes;
		}
		else
		{
			#Log("Valid coupon code=$code entered, but required SKU not found in shopping cart.");
			$Scratch->{$msgscratch} = '<!-- Err-ReqSKU: -->This code is only valid when certain items are purchased and none of those items were found in your shopping cart.';
			return $yes;
		}
	}
	elsif ($coupon->{type} eq 'item')
	{
		my @skus = split(/,/, $coupon->{coup_item});
		my $disc_qty = $coupon->{coup_item_disc_qty};

		my (@valid_skus, $sku_found);

		$Tag->perl({tables => 'desc_lens_pkg',});
		my $db = $Db{desc_lens_pkg};

		foreach my $i (@{$::Carts->{$cart}})
		{
			my (@cats, $cat, @pkgs, $pkg);
			if (@skus)
			{
				#Log('Item coupon with SKUs found!');
				next unless in_array($i->{code}, \@skus);
			}
			elsif ($coupon->{gc_cat})
			{
				#Log('Item coupon with no SKUs but WITH categories!');
				$cat = $Tag->data({ table => 'products', column => 'category', key => $i->{code} });
				@cats = split(/,/, $coupon->{gc_cat});
			}

			$sku_found = 1;

			if (!$coupon->{gc_lp} && !$coupon->{gc_cat})
			{
				#Log('Item coupon with no cat or lp constraints - adding ' . $i->{code} . ' to valid list.');
				push(@valid_skus, $i->{code});
				next;
			}

			if ($coupon->{gc_lp})
			{
				my $results = $db->query({ 	sql => "SELECT pkgletter FROM desc_lens_pkg WHERE pkgcode = '" . $i->{lenstype} . "'",
								hashref => 1,
								table => 'desc_lens_pkg'
				});
				$pkg = $results->[0]->{pkgletter};
				#Log('Item coupon with lp constraint found. Current item has lp: ' . $pkg);
				@pkgs = split(/,/, $coupon->{gc_lp});
			}

			if ((@skus && $coupon->{gc_lp}) || ($coupon->{gc_lp} && !$coupon->{gc_cat}))
			{
				#Log('Item coupon with either SKUs and lp constraints OR no SKUs and no cat constraints but with lp constraints. 1');
				if ($coupon->{gc_lp_type})
				{
					#Log('Len pkg constraint set to excludes.');
					next if in_array($pkg, \@pkgs);
				}
				else
				{
					next unless in_array($pkg, \@pkgs);
				}
				#Log('Adding ' . $i->{code} . ' to valid SKUs list.');
				push(@valid_skus, $i->{code});
			}
			elsif (!@skus && !$coupon->{gc_lp} && $coupon->{gc_cat})
			{
				#Log('Item coupon with no SKUs and no lp constraints but with cat constraints. 2');
				if ($coupon->{gc_cat_type})
				{
					next if in_array($cat, \@cats);
				}
				else
				{
					next unless in_array($cat, \@cats);
				}
				push(@valid_skus, $i->{code});
			}
			elsif (!@skus && $coupon->{gc_lp} && $coupon->{gc_cat})
			{
				#Log('Item coupon with no SKUs but with both lp and cat constraints. 3');
				my ($valid_cat, $valid_lp);

				if ($coupon->{gc_cat_type})
				{
					$valid_cat = 1 if !in_array($cat, \@cats);
				}
				else
				{
					$valid_cat = 1 if in_array($cat, \@cats);
				}

				if ($coupon->{gc_lp_type})
				{
					$valid_lp = 1 if !in_array($pkg, \@pkgs);
				}
				else
				{
					$valid_lp = 1 if in_array($pkg, \@pkgs);
				}
				if ($valid_cat && $valid_lp)
				{
					push(@valid_skus, $i->{code});
				}
			}
		}

		if ($sku_found && !@valid_skus)
		{
			#Log("Valid coupon code=$code entered, but no items from appropriate category or items with proper associated lens packages not found.");
			$Scratch->{$msgscratch} = '<!-- Err-Constraint: -->This coupon is valid only with items from specific categories or with items associated with specific lens packages and no such items were found in your shopping cart.';
			return $no;
		}
		elsif (!$sku_found)
		{
			#Log("Valid coupon code=$code entered, but appropriate SKU not found in cart.");
			$Scratch->{$msgscratch} = '<!-- Err-SKU: -->This coupon is applicable to specific items and none of these items were found in your shopping cart.';
			return $no;
		}

		if ($coupon->{coup_type} eq 'pct')
		{
			my $pct = 1 - ($coupon->{coup_amt} / 100);
			foreach my $sku (@valid_skus)
			{
				if ($disc_qty)
				{
					$Tag->discount($sku, 'return $s * ' . $pct . ' if $q <= ' . $disc_qty . '; my $p = $s/$q; return (' . $disc_qty . ' * $p * ' . $pct . ') + ($p * ($q - ' . $disc_qty . '));');
				}
				else
				{
					$Tag->discount($sku, '$s * ' . $pct);
				}
			}
		}
		else
		{
			my $amt = $coupon->{coup_amt};
			foreach my $sku (@valid_skus)
			{
				if ($disc_qty)
				{
					$Tag->discount($sku,'return $s - (' . $amt . ' * $q) if $q <= ' . $disc_qty . '; my $p = $s/$q; return (' . $disc_qty .' * ($p - ' . $amt . ')) + (($q - '. $disc_qty . ') * $p);');
				}
				else
				{
					$Tag->discount($sku,'return $s - (' . $amt . ' * $q);');
				}
			}
		}

		$Scratch->{disc_amount} = $Scratch->{pre_disc_st} - $Tag->subtotal({ noformat => 1 });
		$Scratch->{$msgscratch} = 'Code accepted: ' . $coupon->{description};
		$Vend::Session->{current_coupon_code} = $code;
		return $yes;
	}
	elsif ($coupon->{type} eq 'all_items')
	{
		my @skus = split(/,/, $coupon->{coup_item});
		my $sku_found;

		if (@skus)
		{
			foreach my $i (@{$::Carts->{$cart}})
			{
				next unless in_array($i->{code}, \@skus);
				$sku_found = 1;
			}
		}
		else
		{
			$sku_found = 1;
		}

		if ($sku_found)
		{
			if ($coupon->{coup_type} eq 'pct')
			{
				my $pct = 1 - ($coupon->{coup_amt} / 100);
				$Tag->discount('ALL_ITEMS','return $s * ' . $pct);
			}
			else
			{
				$Tag->discount('ALL_ITEMS','return $s - (' . $coupon->{coup_amt} . ' * $q)');
			}

			$Scratch->{disc_amount} = $Scratch->{pre_disc_st} - $Tag->subtotal({ noformat => 1 });
			$Scratch->{$msgscratch} = 'Code accepted: ' . $coupon->{description};
			$Vend::Session->{current_coupon_code} = $code;
			return $yes;
		}
		else
		{
			#Log("Valid coupon code=$code entered, but required SKU not found in shopping cart.");
			$Scratch->{$msgscratch} = '<!-- Err-ReqSKU: -->This code is only valid when certain items are purchased and none of those items were found in your shopping cart.';
			return $no;
		}
	}
	elsif ($coupon->{type} eq 'bogo')
	{
		$Scratch->{$msgscratch} = 'Code accepted: ' . $coupon->{description};

		# CHECK COUPON CONSTRAINTS TO BE SURE A VALID ITEM IS IN THE CART - BUILD ARRAY OF VALID ITEMS
		# CHECK BOGO OPTS TO BE SURE A QUALIFYING ITEM EXISTS TO DISCOUNT - BUILD ARRAY OF QUALIFYING ITEMS

		# DISCOUNT ONE QUALIFYING ITEM FOR EVERY ONE VALID ITEM
		# DISCOUNTED ITEM(S) SHOULD ALWAYS BE THE LOWEST PRICED

		return $yes;
	}
	elsif ($coupon->{type} eq 'shipping')
	{
		if ($coupon->{coup_type} eq 'pct')
		{
			#Log("Valid discount code=$code entered, but type not set correctly for shipping coupon (was $coupon->{coup_type})");
			$Scratch->{$msgscratch} = '<!-- Err-Spct: -->There is a problem with this code - please call us to redeem.';
			return $no;
		}
		else
		{
			$Scratch->{shipping_adder} = '-' . $coupon->{coup_amt};
			#Log('setting shipping adder: ' . $Scratch->{shipping_adder});
			$Scratch->{shipping_coupon} = 1;
			$Scratch->{$msgscratch} = 'Code accepted: ' . $coupon->{description};
			$Vend::Session->{current_coupon_code} = $code;
			return $yes;
		}
	}
	else
	{
		#Log("Coupon: Incorrect Type '$coupon->{type}'");
		$Scratch->{$msgscratch} = '<!-- Err-T: -->There is a problem with the coupon you entered. Please call customer service for assistance.';
		return $no;
	}

	sub in_array
	{
		my ($search_for, $arr) = @_;
		foreach my $value (@$arr)
		{
			return 1 if $value eq $search_for;
		}
		return 0;
	}
}
EOR
