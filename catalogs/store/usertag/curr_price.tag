UserTag curr_price               Order        price
UserTag curr_price               addAttr
UserTag curr_price               attrAlias    base mv_ib
UserTag curr_price               PosNumber    1
UserTag curr_price               Routine   <<EOR
sub {
	my ($price, $ref) = @_;
	$ref->{price} ||= $price;

	#
	# Choose currency usertag
	#
	# This tag will display a price in chosen currency based on current
	# exchange rate data. Use this for places where theres no [price] tag
	#
	# Use: [curr_price price="39"]
	# Returns: (GBP 21) or whatever
	#

	my $c = lc($Scratch->{currency_div_1});
	my $r;
	my $iso;
	my $yourprice;
	my $exline;
	my $ok = 0;

	$Tag->query({ hashref => 'exrate', table => 'country', sql=> "SELECT exrate,iso_currency FROM country WHERE code='$c' LIMIT 1" });

	foreach $exline (@{$Tmp->{exrate}}) {
		$r = $exline->{exrate};
		$iso = $exline->{iso_currency};
		$yourprice = $price * $r;
		$yourprice = sprintf("%.2f", $yourprice);
	}

	if (!$r) {
		return;
	}

	return "$iso $yourprice" if $ref->{nostyle};
	return "($iso $yourprice)";
}
EOR