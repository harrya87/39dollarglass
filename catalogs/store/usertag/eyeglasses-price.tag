
UserTag eyeglasses_price Order sku
UserTag eyeglasses_price AddAttr
UserTag eyeglasses_price Routine <<EOR
sub {
	my ($sku, $opt) = @_;

	my (%option, $price, $qty, $sql, $db, $results, $rimless, 
			$tint, $sunonly, $options_lens, $options_lenstype, $prefix, $cheapest, $packageonly, $clips);

	$qty = $Vend::Interpolate::item->{quantity};
	$qty = $opt->{qty} unless $qty;

	$sku ||= $Vend::Interpolate::item->{code};

# options are:
# lens, lenstype, tint, coating, varilux
# price is the base price plus the price of each of these added together.

	$option{lens} = $opt->{lens} || $Vend::Interpolate::item->{lens} || 'SVF';
	$option{lenstype} = $opt->{lenstype} || $Vend::Interpolate::item->{lenstype};
	$option{tint} = $opt->{tint} || $Vend::Interpolate::item->{tint};
	$option{coating} = $opt->{coating} || $Vend::Interpolate::item->{coating};
	$option{varilux} = $opt->{varilux} || $Vend::Interpolate::item->{varilux};
	$option{uyof_add} = $opt->{uyof_add} || $Vend::Interpolate::item->{uyof_add};
	$option{case} = $opt->{case} || $Vend::Interpolate::item->{case};

	#See if the prism parameter was passed in
	$option{prism} = $opt->{prism};
	
	#If not (meaning the tag was called to calcuate the price on the item within IC itself
	unless( $option{prism} ){
		#Loop through each eye
		foreach my $eye( qw/s d/ ){
			# If either eye has both a direction && magnitude then prism = true
			$option{prism} ||= ($Vend::Interpolate::item->{"o${eye}_prism_vertical_magnitude"} && $Vend::Interpolate::item->{"o${eye}_prism_vertical_direction"});
			$option{prism} ||= ($Vend::Interpolate::item->{"o${eye}_prism_horizontal_magnitude"} && $Vend::Interpolate::item->{"o${eye}_prism_horizontal_direction"});
		}
	}
	#If there is a value in prism and it's not no (incase it was passed in) then set it to yes
	$option{prism} = $Vend::Interpolate::item->{prism} = $option{prism} && $option{prism} ne 'no' ? 'hasPrism' : '';
	
	$sunonly = $opt->{sunonly} || $Vend::Interpolate::item->{sunonly};
	$packageonly = $opt->{packageonly} || $Vend::Interpolate::item->{packageonly};
	$clips = $opt->{clips} || $Vend::Interpolate::item->{clips};

	$Tag->perl({ tables => 'options_price products', });
	
	$sql = qq{SELECT price, category, options_lens, options_lenstype FROM products WHERE sku = '$sku'};

	$db = $Db{products};
	# Log($sql);
	$results = $db->query( { 	sql => "$sql",
					hashref => 1,
					table => 'products',});
	#Added in the option to pass in a price. This was done to accomodate
	#the addition of the active_discounts table. Using opt->{price} enables
	#us to calculate the total price based on the 'original_price' in that table
	$price = $opt->{price} || $results->[0]->{price};
	
	if(($results->[0]->{category} =~ /rimless/i) && ($results->[0]->{category} !~ /semi/i))
	{
		$rimless = 1;
	}
	$options_lenstype = $results->[0]->{options_lenstype};
	$options_lens = $results->[0]->{options_lens};
	
#	if (!$options_lens) {
#		return $price;
#	}

	$db = $Db{options_price};
	
	$sql = qq{SELECT * FROM options_price};
	$results = $db->query( { 	sql => "$sql",
					hashref => 1,
					table => 'options_price',});

	# loop through the dataset..
	foreach my $row (@$results){
		if ($option{$row->{o_group}} eq $row->{o_value}){
			$price += $row->{price};
			$tint = 1 if $row->{tint};
		}
	}
	# if its UYOF, return price here, after its added A/R etc so that it skips the next bit
	# which only applies to frame+lens orders
	if (!$options_lens) {
		return $price;
	}

	# If we have clips, charge $24.95 for each pair
	if ($clips > 0) {
		$price += ($Variable->{CLIP_PRICE} * $clips);
	}

	# Since the base lens package may add cost, we need to check this if
	# there is not one specified..
	# Lens			Lens pkg prefix
	# SVF||SVR	OPT/SUN
	# BIF 			BIF_
	# PROG			PROG_ SUN/OPT/TRAN
	# PROGE			PROGE_ 
	
	
	if (!$option{lenstype}){
 # Log('no package passed, calculating..');
		$sql = qq{SELECT * FROM options_price WHERE o_group = 'lenstype'};
		$results = $db->query( { 	sql => "$sql",
					hashref => 1,
					table => 'options_price',});
					
		if ($option{lens} !~ /(SVF|SVR)/){
			$prefix = $option{lens} . '_';
		}
		$cheapest = 99999;
		foreach my $row (@$results){
			if ($prefix){
				next unless ($row->{o_value} =~ /^$prefix/);
			} else {
				next if ($row->{o_value} =~ /^(BIF|PROG|PROGE)/);
			}
			if ($sunonly){
				next unless $row->{tint};
			} else {
				next if $row->{tint};
			}
 # Log("looping, value=$row->{o_value}");
			# now loop through the available lens packages for this sku
			# find the cheapest one..

			foreach my $pkg(split /\s/, $options_lenstype){
				if ($pkg eq $row->{o_value}){
 # Log("looping, package=$pkg, price=$row->{price}");
					$cheapest = $row->{price} if $row->{price} < $cheapest;
					# NOTE, we are ignoring if this is a tint 
					# unless the pricing has explicitly asked us for a sunglasses price.
					# this is because the base price for rimless sunglasses is lower than clear..
					$tint = 1 if ($sunonly && $row->{tint});
 # Log("cheapest now $cheapest");
				}
			}
		}
		$price += $cheapest;
 # Log("sku=$sku, cheapest lens package=$cheapest, price=$price");
	}

	if ($packageonly){
		return $Tag->currency({ body => "$cheapest",}) unless $opt->{noformat};
		return $cheapest;
	}	
	
	# if its a rimless frame and a tint, we deduct $24.95 from the lens package
	# which is for the removal of AR..
	#$price -= 24.95 if ($rimless && $tint);
 	#Log("subtracting 24.95..") if ($rimless && $tint);

	return $Tag->currency({ body => "$price",}) unless $opt->{noformat};
	return $price;
}
EOR
