UserTag featured_tabs Order type field limit order_by
UserTag featured_tabs Routine <<EOR

sub avail_colors{
	my ($dbh, $sku) = @_;
	$sku =~ /^(\d+)_/;
	my $digits = $1;
	my $sth = $dbh->prepare(q\
		SELECT distinct primary_color FROM products where inactive != ? and sku like ?
	\);
	$sth->execute('1', "$digits\_%");
	my $avail_colors;
	while (my $r = $sth->fetchrow_arrayref){
		push @$avail_colors, $r->[0];
	}
	#this is a comment
	return $avail_colors || [];
}

sub fetchall_hashref {
    #its called a hash ref interchange, use it
    my ($sth, @columns) = @_;
    my $res;
    while (my $row = $sth->fetchrow_arrayref){
        my $count = 0;
        my $ref;
        for (@$row){
            $ref->{$columns[$count++]} = $_;
        }
        push @$res, $ref;
    }
    return $res;
}

sub {
	my ($type, $field, $limit, $order_by) = @_;
	$field ||= 'category';
	$limit ||= 6;
	$order_by ||= 'RAND()';
	$Tag->perl({tables => 'products'});
	my $dbh = $Sql{'products'};
	my $sth;
	my @prod_columns = qw\sku ven_color description price retail_price image\;
	my @types;
	if ($type && $type ne 'best_sellers' && $type ne 'new_arrivals'){
		my @wheres;
		if ($type =~ /####/){
			@types = split /####/, $type;
			for (@types){
				push @wheres, "( $field like ? )";
			}
		}
		my $where = @wheres ? (join ' OR ', @wheres) : "$field = ?";
		$sth = $dbh->prepare(sprintf(q\
			SELECT substring(p.sku,1,INSTR(p.sku,'_')-1) as master, %1$s FROM
			products p where p.inactive != 1 
			and p.sku rlike '^[0-9]+' and ( %4$s )
			GROUP by master
			ORDER BY %2$s LIMIT %3$s
		\, (join ', ', @prod_columns), $order_by, $limit, $where ));
		$sth->execute(@types ? (map { '%'.$_.'%' } @types) : $type);
		my $res = fetchall_hashref($sth, 'master', @prod_columns);
		for my $r (@$res){
			$r->{available_colors} = join '####', @{avail_colors($dbh, $r->{sku})};
		}
		return $res || [];
	}
	elsif ($type eq 'new_arrivals'){
		$sth = $dbh->prepare(sprintf(q\
			SELECT substring(p.sku,1,INSTR(p.sku,'_')-1) as master, %1$s FROM
			products p where inactive <> 1 
			and p.sku rlike '^[0-9]+'
			and date_added >= ?
			GROUP by master
			ORDER BY date_added DESC
			LIMIT %2$s
		\, (join ', ', @prod_columns), $limit));
		$sth->execute($Tag->var('NEW_ARRIVALS_DATE_ADDED'));
		my $res = fetchall_hashref($sth,'master', @prod_columns);
		for my $r (@$res){
			$r->{available_colors} = join '####', @{avail_colors($dbh, $r->{sku})};
		}
		return $res || [];
	}
	else{ # best sellers
		$sth = $dbh->prepare(sprintf(q\
			SELECT substring(p.sku,1,INSTR(p.sku,'_')-1) as master, %1$s FROM
			products p where inactive <> 1 
			and p.sku rlike '^[0-9]+'
			GROUP by master
			ORDER BY CAST(effective_sr AS decimal(10,3)) DESC
			LIMIT %2$s
		\, (join ', ', @prod_columns), $limit));
		$sth->execute();
		my $res = fetchall_hashref($sth,'master', @prod_columns);
		for my $r (@$res){
			$r->{available_colors} = join '####', @{avail_colors($dbh, $r->{sku})};
		}
		return $res || [];
	}
}
EOR
