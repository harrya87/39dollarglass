UserTag frame_names Order admin article
UserTag frame_names Routine <<EOR
sub {
	my ($admin, $article) = @_;
	unless ($admin){
		$Tag->perl({tables => 'search',});
		my $db = $Db{'search'};

		my $results = $db->query({
			sql => "SELECT name, color, sku from search where type ='frame'",
			table => 'search',
			hashref => 1
		}) || [];
		my @suggestions;
		for my $res (@$results){
			for (qw/Eyeglasses Sunglasses/){
				push @suggestions, {
					name => $res->{name}.' '.$res->{color}." $_",
					sku =>$res->{sku},
					sun => /Sunglasses/,
				};
			}
		}
		my %url_map = (
			Bifocal => {
				eye => 'bifocals',
				sun => 'bifocals-sun'
			},
			Reading => {
				eye => 'reading-glasses',
			},
			Progressive => {
				eye => 'progressives',
				sun => 'progressives-sun'
			},
			Rimless => {
				eye => '14000',
				sun => 'rimless-sunglasses'
			},
			'Semi-Rimless' => {
				eye => 'semi_rimless-eyeglasses',
				sun => 'semi_rimless-sunglasses'
			},
			Plastic => {
				eye => '12000',
				sun => 'plastic-sunglasses'
			},
			Metal => {
				eye => '11000.html',
				sun => 'metal-sunglasses'
			},
			Aviator => {
				sun => 'aviator-sunglasses'
			},
		);
		for (qw\Bifocal Reading Progressive Rimless Semi-Rimless Plastic Metal Aviator\){
			if ($_ ne 'Aviator'){
				push @suggestions, {
					name => "Shop $_ Eyeglasses",
					sku => $url_map{$_}->{eye}.'.html',
					cat => 1
				};
			}
			if ($_ ne 'Reading'){
				push @suggestions, {
					name => "Shop $_ Sunglasses",
					sku => $url_map{$_}->{sun}.'.html',
					cat => 1
				};
			} 
		}
		undef $results;
		return $Tag->filter('json',  \@suggestions );
	} else {
		$Tag->perl({tables => 'search',});
		my $db = $Db{'search'};
		my $results = $db->query({
			sql => "SELECT sku, name from search where type = '".($article ? 'article':'frame')."'",
			table => 'search',
			hashref => 1
		}) || [];
		my @suggestions;
		for (@$results){
			push @suggestions, { name => $_->{sku}.': '.$_->{name} };
		}
		undef $results;
		#hooray for chaining!
		return $Tag->filter('json',  \@suggestions );
	}

}
EOR
