UserTag generate_coupon Order prefix
UserTag generate_coupon addAttr
UserTag generate_coupon Routine <<EOR

sub
{
	my ($prefix) = @_;

	$Tag->perl({tables => 'coupon_codes'});

	my $db = &Vend::Data::database_exists_ref('coupon_codes');
	$db = $db->ref();
	my $dbh = $db->[$Vend::Table::DBI::DBI];
	unless ($dbh)
	{
		::logError("Can't create db handle");
		return;
	}

	my $dupe = 1;
	my $rand;

	my $sth = $dbh->prepare('SELECT COUNT(*) FROM coupon_codes WHERE code = ?')
		or ::logError("Can't prepare: %s", $dbh->errstr() );

	while ($dupe == 1)
	{
		$rand = (int(rand(999999)) + 100000);
		$rand = $prefix.$rand;
		$sth->execute($rand);
		my ($cnt) = $sth->fetchrow_array();
		$dupe = undef if $cnt == 0;
	}
	return $rand;
}
EOR
