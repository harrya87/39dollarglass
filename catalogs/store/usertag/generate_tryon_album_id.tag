# written for 39DG by Rick Bragg, Green Mountain Network, www.GreenMountainNetwork.com
# Feb 2011

UserTag generate_tryon_album_id Order prefix
UserTag generate_tryon_album_id addAttr
UserTag generate_tryon_album_id Routine <<EOR

sub
{
	my ($prefix) = @_;
	$Tag->perl({tables => 'tryon_albums'});
	my $db = &Vend::Data::database_exists_ref('tryon_albums');
	$db = $db->ref();
	my $dbh = $db->[$Vend::Table::DBI::DBI];
	unless ($dbh)
	{
		::logError("Can't create db handle");
		return;
	}
	my $dupe = 1;
	my $rand;
	my $sth = $dbh->prepare('SELECT COUNT(*) FROM tryon_albums WHERE code = ?')
		       or ::logError("Can't prepare: %s", $dbh->errstr() );

	while ($dupe == 1)
	{
		$rand = randgen() or ::logError("Can't can't make key: %s", $dbh->errstr() );
		$sth->execute($rand);
		my ($cnt) = $sth->fetchrow_array();
		$dupe = undef if $cnt == 0;
	}


	sub randgen
	{
      my $randkey;
      my @lets = ('A' .. 'Z', '0' .. '9', 'a' .. 'z');
		$randkey .= $lets[ rand @lets ] while length($randkey) < 25;
      return $randkey or ::logError("Can't can't make key: %s", $dbh->errstr() );
	}

   return $rand;
}
EOR




		
		
        


