UserTag history_log Order 
UserTag history_log addAttr
UserTag history_log HasEndTag
UserTag history_log Interpolate 1
UserTag history_log Routine <<EOR
sub {
	my ($opt, $body) = @_;

	my ($template, $prefix, $out, $suffix);


	$opt->{username} ||= $Session->{username};
	$opt->{customerid} ||= $Session->{username};	
	$opt->{historylog} ||= $body;

	$opt->{historydate} = $Tag->convert_date({ fmt => '%Y%m%d%H%M', });

	# write out to database..

	my $dbh = dbref('history')->dbh();

	$q = "INSERT INTO history (historydate,username,customerid,orderid,historytype,historylog) VALUES (?,?,?,?,?,?)";
	my $sth = $dbh->prepare($q)
		or die "failed to prepare $q: $DBI::errstr\n";

	$sth->execute(	$opt->{historydate},
			$opt->{username},
			$opt->{customerid},
			$opt->{orderid},
			$opt->{historytype},
			$opt->{historylog})
		or warn "Unable to log add history log entry\n";

	return 'ok';

}
EOR
