UserTag opt_display Order
UserTag opt_display addAttr
UserTag opt_display Routine <<EOR
sub {
	my ($opt) = @_;
	my ($db, $sql, $optval, @out);


	$opt->{separator} ||= ': ';
	$opt->{joiner} ||= '<br> ';

	my @opts = qw{
		lens	lenstype
		tint	coating
		varilux	case
		prism
		};

	$Tag->perl({tables => 'options_price',});

	$db = $Db{options_price};

	foreach my $option(@opts){
# push @out, "option: $option";
		$optval = $opt->{$option} || $Vend::Interpolate::item->{$option};

		next unless $optval;
# push @out, "value: $optval";
		$sql = qq{	SELECT o_name, display_label FROM options_price 
				WHERE o_value = '$optval'};

		my $results = $db->query( { 	sql => "$sql",
					hashref => 1,
					table => 'options_price',});
		if ($results){

			$results = $results->[0];
			$option = $results->{o_name} if ($opt->{beautify});

			push @out, $option . $opt->{separator} . $results->{display_label};
		} else {
			push @out, $option . $opt->{separator} . $optval;
		}

	}
	return join $opt->{joiner}, @out;
}
EOR
