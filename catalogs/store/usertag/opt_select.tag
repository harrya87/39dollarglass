UserTag opt_select Order
UserTag opt_select addAttr
UserTag opt_select Routine <<EOR
sub {
	my ($opt) = @_;
	my ($sku, $ip, $db, $dbp, $sql, $optval, $optselect, $ors, $prs, $row, @out, $optvals);


	$opt->{separator} ||= ': ';
	$opt->{joiner} ||= '<br> ';

	push @out, <<EOCSS;
<style>
OPTION.highlight {
	background-color: #dddddd;
}
</style>
EOCSS

	my @opts = qw{lens lenstype lenscustom tint coating coatingcustom case};

	$Tag->perl({tables => 'options_price products',});

	$db = $Db{options_price};
	$dbp = $Db{products};
	$sku = $item->{code} || $opt->{code};
	$ip = $opt->{mv_ip};

# Log(uneval_it($item));

	foreach my $option(@opts){
		
		my $name = '';
		$optselect = $Items->[$ip]->{$option};
		
		if($option eq 'lenscustom')
		{
			if($Items->[$ip]->{lenstype} eq 'PKGZ')
			{
				push @out, "<input type=\"text\" size=\"35\" name=\"$option$ip\" value=\"$optselect\" />";
				push @out, $opt->{joiner};
			}
			next;
		}	
		
		if($option eq 'coatingcustom')
		{
			if($Items->[$ip]->{coating} eq 'custom')
			{
				push @out, "<input type=\"text\" size=\"35\" name=\"$option$ip\" value=\"$optselect\" />";
				push @out, $opt->{joiner};
			}
			next;
		}		

# Log("option is: $option, selected is: $optselect");


		$sql = qq{	SELECT o_value, o_name, display_label FROM options_price 
				WHERE o_group = '$option'};
		
		# If we're looking up cases then only grab records with
		# a specified sort order
		if( $option eq 'case' ){
			$sql .= " AND o_sort != '' ORDER BY o_sort";
		}elsif( $option eq 'lenstype' ){
			$sql .= ' ORDER BY display_label';
		}

		$ors = $db->query( { 	sql => "$sql",
						hashref => 1,
						table => 'options_price',});


		# look up the option values available to this item..

		if ($option ne 'case'){
			$sql = qq{ SELECT options_$option AS thisoption FROM products WHERE sku = '$sku'};

			$prs = $dbp->query( { 	sql => "$sql",
							hashref => 1,
							table => 'products',});

# Log("option=$option, ref is: " . ref($prs));

			if (ref($prs) eq 'ARRAY'){
				$optvals = $prs->[0]->{thisoption};
				#Log("no options found for products field options_$option, sql=$sql");
			}
		}
		
		if (@$ors){
			#Log('records=' . @$ors);
			foreach $row(@$ors){

				unless ($name){
					# push @out, qq{<input type=hidden name="mv_item_option" value="$option">};
					push @out, $row->{o_name} . $opt->{separator};
					push @out, qq{<select name="$option$ip" id="$option-$ip" data-default="$optselect">};
					$name = $row->{o_name};
				}
				push @out, qq{<option value="$row->{o_value}"};
				push @out, q{ SELECTED} if ($row->{o_value} eq $optselect);
				push @out, q{class="highlight"} if $optvals !~ /$row->{o_value}/;
				push @out, qq{>$row->{display_label}</option>};
			}
		}


		push @out, qq{</select>};
		push @out, $opt->{joiner};
	}

	return join "\n", @out;
}
EOR
