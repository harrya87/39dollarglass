
UserTag package_price Order sku
UserTag package_price AddAttr
UserTag package_price Routine <<EOR
sub {
	my ($sku, $opt) = @_;

	my (%option, $price, $qty, $sql, $db, $results, $rimless, 
			$tint, $sunonly, $options_lenstype, $prefix, $cheapest);
			
	$qty = $Vend::Interpolate::item->{quantity};
	$qty = $opt->{qty} unless $qty;

	$sku ||= $Vend::Interpolate::item->{code};

# options are:
# lens, lenstype, tint, coating, varilux
# price is the base price plus the price of each of these added together.

	$option{lens} = $opt->{lens};
	$option{lenstype} = $opt->{lenstype};
	$option{tint} = $opt->{tint};
	$option{coating} = $opt->{coating};
	$option{varilux} = $opt->{varilux};
	$option{uyof_add} = $opt->{uyof_add};
	$option{case} = $opt->{case};

	$sunonly = $opt->{sunonly};

	my $base_pkg_price = $Tag->eyeglasses_price({ sku => "$sku", lens => "$option{lens}", sunonly => "$opt->{sunonly}", packageonly => 1, noformat => 1,});

	#Log("package-price: base_pkg_price=$base_pkg_price");
	# look up the price of this option..
	$Tag->perl({ tables => 'options_price products', });
	$db = $Db{options_price};
	$sql = qq{SELECT * FROM options_price WHERE o_group = 'lenstype' AND o_value = '$option{lenstype}'};
	$results = $db->query( { 	sql => "$sql",
					hashref => 1,
					table => 'options_price',});

	my $lenstype_price = $results->[0]->{price};
	$tint = $results->[0]->{tint};	
	
	#Log("package-price: lenstype_price=$lenstype_price");
	
	$price = $lenstype_price - $base_pkg_price;	
	
	if ($opt->{text}){
		my $cprice = $Tag->curr_price({price=>"$price",nostyle=>1});
		
		#added to set spanish, dirty but no other way really :(
		if($Scratch->{mv_locale} eq 'es_ES') {
			return 'sin cargo adicional' if $price == 0;
			return $cprice.' adicionales' if($cprice);
			return $Tag->currency({ body => "$price",}) . ' adicionales' if $price > 0;
		} else {
			return 'FREE!' if $price == 0;	
			return $cprice.' additional' if($cprice);
			return $Tag->currency({ body => "$price",}) . ' additional' if $price > 0;
		}
		
		$price = $price * -1;
		$cprice = $Tag->curr_price({price=>"$price",nostyle=>1});
		$cprice = ' ' . $cprice if $cprice;
		return 'subtract ' . $Tag->currency({body=>"$price",}) . $cprice;
	}
	return $Tag->currency({ body => "$price",}) unless $opt->{noformat};
	return $price;
	

}
EOR
