UserTag package_title Order pkg
UserTag package_title addAttr
UserTag package_title Routine <<EOR
sub {
	my ($pkg, $opt) = @_;
	$Tag->perl({tables => 'desc_lens_pkg'});
	my $db = $Db{desc_lens_pkg};
	my $results = $db->query({ 	sql => "SELECT pkgtitle FROM desc_lens_pkg WHERE pkgletter = '" . $pkg . "'",
					hashref => 1,
					table => 'desc_lens_pkg'
	});
	return $results->[0]->{pkgtitle};
}
EOR
