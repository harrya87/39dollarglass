UserTag page_breadcrumbs	AddAttr
UserTag page_breadcrumbs	Routine      <<EOR
sub {
	my ($opt) = @_;
	my $outstring = '<a href="'.$Tag->area().'">Home</a>';
	my ($itemtitle, $itemurl);
	if($opt->{location}){
		my @locationarray = split(/\|/,$opt->{location});

		my $numlocs = @locationarray-1;

		for(my $c=0;$c<@locationarray;$c++){
			#split the location value by comma, which gives us the title and url
			my @itemarray = split(/,/,$locationarray[$c]);
			$itemtitle = $itemurl = $itemarray[0];
			$itemurl = $itemarray[1] if($itemarray[1]);

			if($c == $numlocs){
				$outstring .= ' &raquo; '.$itemtitle;
			}else{
				if($itemurl =~ /\?/){
					$outstring .= ' &raquo; <a href="/'.$itemurl.'">'.$itemtitle.'</a>';
				}else{
					$outstring .= ' &raquo; <a href="'.$Tag->area({href=>$itemurl}).'">'.$itemtitle.'</a>';
				}
			}
		}
		return '<div class="pageBreadcrumbs">'.$outstring.'</div>';
	}
	return '';
}
EOR
