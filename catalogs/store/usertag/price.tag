UserTag price               Order        code
UserTag price               addAttr
UserTag price               attrAlias    base mv_ib
UserTag price               PosNumber    1
UserTag price               Routine   <<EOR
sub {
	my ($code, $ref) = @_;
	$ref->{code} ||= $code;

	my $amount = Vend::Data::item_price($ref);
	$amount = discount_price($code, $amount, $ref->{quantity})
			if $ref->{discount};

	#
	# Mods for displaying alternate currency next to $
	# Used in conjuction with [curr_price] tag
	#

	#
	# Work out what it is in yer chosen currency if that are been chosen
	#
	my $ok = 0;
	my $c = lc($Scratch->{currency_div_1});
	my $currency_info;
	my $r;
	my $iso;
	my $yourprice;
	my $yours_fmt;
	my $exline;

	if ($c) {
		#
		# In here we need to get exchange rate & work out new price
		#
		$Tag->query({ hashref => 'exrate', table => 'country', sql=> "SELECT exrate,iso_currency FROM country WHERE code='$c' LIMIT 1" });

		foreach $exline (@{$Tmp->{exrate}}) {
			$r = $exline->{exrate};
			$iso = $exline->{iso_currency};
			$yourprice = $amount * $r;
			$yours_fmt = sprintf("%.2f", $yourprice);
		}
		$currency_info = " ($iso $yours_fmt)";

		if ($r) {
			$ok = 1;
		}
	}
	#

	#
	# only add currency info if not using noformat
	#
	my $nof = $ref->{noformat};
	if (!$nof && $ok) {
		my $amt = currency( $amount, 0 );
		my $str = "$amt $currency_info";	
		$amount = $str;
		return $amount;
	}
	#

	#
	# End of mods
	#

	return currency( $amount, $ref->{noformat} );
	
}
EOR