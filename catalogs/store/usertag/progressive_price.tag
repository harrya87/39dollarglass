
UserTag progressive_price Order sku lenstype compare
UserTag progressive_price AddAttr
UserTag progressive_price Routine <<EOR
sub {
	my ($sku, $lenstype, $compare, $opt) = @_;

	my (%option, $price, $lenses, $lenstypes, $varilux, $proge_flag, $sql, $db,
			$results, $lowest_price, $lowest_clear, $lenstype_price, $compare_price, $rimless, $tint);

	$sku ||= $Vend::Interpolate::item->{code};

# options are:
# all we care about here is that there are progressive options
# and we look for the lowest one..

	$Tag->perl({ tables => 'options_price products', });

	$sql = qq{SELECT price, options_lens, options_lenstype, options_varilux, frame_rimless FROM products WHERE sku = '$sku'};

	$db = $Db{products};
	$results = $db->query( { 	sql => "$sql",
					hashref => 1,
					table => 'products',});
	$price = $results->[0]->{price};
	$lenses = $results->[0]->{options_lens};
	$lenstypes = $results->[0]->{options_lenstype};
	$varilux = $results->[0]->{options_varilux};
	$rimless = $results->[0]->{frame_rimless};
	
	$proge_flag = 0;
	
	$db = $Db{options_price};
	$lowest_price = 99999;

	foreach my $option(split /\s+/, $lenses){
		next unless $option =~ /PROG/;
		$proge_flag = 1 if $option =~ /PROGE/;
		$sql = qq{SELECT price, tint FROM options_price WHERE o_value = '$option' AND o_group = 'lens'};
		$results = $db->query( { 	sql => "$sql",
						hashref => 1,
						table => 'options_price',});
		$lowest_price = $results->[0]->{price} if $results->[0]->{price} < $lowest_price;
	}
	$lowest_price = 0 if $lowest_price == 99999;
	$price += $lowest_price;
	
	$lowest_price = 99999;
	$lowest_clear = 99999;

	# if there is a clear one then this makes up the price. 
	# If there isn't a clear then we just take the lowest price..
	foreach my $option(split /\s+/, $lenstypes){
		next unless $option =~ /PROG/;
		$sql = qq{SELECT price FROM options_price WHERE o_value = '$option' AND o_group = 'lenstype'};
		$results = $db->query( { 	sql => "$sql",
						hashref => 1,
						table => 'options_price',});
		$lowest_clear = $results->[0]->{price} if (($results->[0]->{price} < $lowest_clear) && ($option =~ /OPT/));
		$lowest_price = $results->[0]->{price} if $results->[0]->{price} < $lowest_price;
	}
	$lowest_price = 0 if $lowest_price == 99999;
	$lowest_clear = 0 if $lowest_clear == 99999;
	
	if ($lowest_clear){
			$lenstype_price = $lowest_clear;
		} else {
			$lenstype_price = $lowest_price;
		}
	
	$price += $lenstype_price;
	
	if (!$proge_flag){
		foreach my $option(split /\s+/, $varilux){
			next unless $option =~ /mandatory/;
			$sql = qq{SELECT price FROM options_price WHERE o_value = 'mandatory' AND o_group = 'varilux'};
			$results = $db->query( { 	sql => "$sql",
							hashref => 1,
							table => 'options_price',});
			$price += $results->[0]->{price};
		}
	}

	# check if a foreign currency has been chosen
	my $fc = lc($Scratch->{currency_div_1});

	# At this point, $price holds the lowest progressive price for clear lenses, (or lowest overall if no clear).
	
	if (!$compare){
# Log("progressive-price: tinted=$opt->{tinted}, rimless=$rimless");
		$price -= 5 if ($opt->{tinted} && $rimless);
		return $Tag->currency({ body => "$price",}) unless $opt->{noformat};
		return $price;
	}
	
	# if we got to here we are doing a comparison..
	
	# so we look up the cost of the option
	# remove the lenstype_price, add on the new compare option, 
	# and then compare this total price to $price..
	
	$sql = qq{SELECT price FROM options_price WHERE o_value = '$lenstype' AND o_group = 'lenstype'};
	$results = $db->query( { 	sql => "$sql",
						hashref => 1,
						table => 'options_price',});
	# return unless ref $results eq 'array';
		
	$compare_price = $price - $lenstype_price + $results->[0]->{price};

	Log("sku=$sku, lenstype=$lenstype");
# Log("its a compare.. price=$price, compare=$compare_price");

	if ($price == $compare_price){
		return 'no additional cost';
	}
	
	my $curr_add;

	if ($compare_price > $price){
		my $amt = $compare_price - $price;
		if ($fc) {	$curr_add = ' '. $Tag->curr_price({ price => "$amt" }); }
		return $Tag->currency({ body => "$amt",}) . ' additional' . $curr_add;
	}
	my $amt = $price - $compare_price;
	if ($fc) {	$curr_add = ' '. $Tag->curr_price({ price => "$amt" }); }
	return 'subtract ' . $Tag->currency({ body => "$amt",}) . $curr_add;
	
}
EOR
