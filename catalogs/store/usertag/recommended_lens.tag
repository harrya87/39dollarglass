UserTag recommended_lens Order	lenstype	lenses
UserTag recommended_lens AddAttr
UserTag recommended_lens Routine <<EOR

sub {
	my ($lenstype,@lenses) = @_;	
	my ($sql,$frame,$package);	
	
	$lenstype = lc($lenstype);
	
	my @ameas;
	my ($framematerial,$framesize);
	
	my ($sph_sql,$cyl_sql,$pwr_sql);
	
	my $lens = $Values->{mv_order_lens};
	if($lens eq 'SVR')
	{
		$lens = 'SVF';
	}
	
	$Tag->perl({tables=>'recommended_lenses products'});
	
	##get information about the current frame
	$sql = qq(SELECT * FROM products WHERE sku = '$Scratch->{sku}');
	$Tag->query({sql=>$sql,hashref=>'frame',table=>'products'});
	
	foreach $frame (@{$Tmp->{frame}})
	{		
		if($frame->{meas})
		{
			@ameas = split /\-/,$frame->{meas};
			my $ameas = $ameas[0];
			$framesize = 's';
			if($ameas > 50)
			{
				$framesize = 'l';
			}
		}
		$framematerial = $frame->{category};
		if($framematerial =~ /plastic/i) {
			$framematerial = 'plastic';
		}
		elsif($framematerial =~ /Rimless/i) {
			$framematerial = 'rimless';
		}
		elsif($framematerial =~ /Semi-Rimless/i) {
			$framematerial = 'semi-rimless';
		}
		else {
			$framematerial = 'metal';
		}
	}
	
	my $os_sph = $Values->{mv_order_ossph};
	my $os_cyl = $Values->{mv_order_oscyl};
	my $os_pwr = $os_sph + $os_cyl;	
	
	my $od_sph = $Values->{mv_order_odsph};
	my $od_cyl = $Values->{mv_order_odcyl};
	my $od_pwr = $od_sph + $od_cyl;
	
	if($os_sph eq 'PLANO') {
		$os_sph = 0;
	}
	if($od_sph eq 'PLANO') {
		$od_sph = 0;
	}
	
	my $rxstart = 'AND (';
	my $rxend = ')';
	
	my @packages;
	
	##we have the size + material so now we need to work out which range were in so we can select the package.
	##first of all we need to know what parts of the prescription we have we will need to get a list of all the recommeneded frames
	##then we will select the cheapest if there's more than 1
		
	if($os_sph ne '' || $od_sph ne '')
	{
		$sql = qq{
		SELECT
		package 
		FROM
		recommended_lenses
		WHERE
		framesize='$framesize'
		AND
		framematerial='$framematerial'
		AND
		lens='$lens'
		AND
		packagetype='$lenstype'
		AND
		((range_type='sph' AND (range_from <= '$os_sph' OR range_from <= '$od_sph') AND (range_to >= '$os_sph' OR range_to >= '$od_sph')) OR (type_default='1'))		
		AND
		(under18<>'1' OR under18 IS NULL)
		};
		
		$Tag->query({sql=>$sql,hashref=>'results',table=>'recommended_lenses'});	
		foreach my $pkg (@{$Tmp->{results}})
		{	
			$package = $pkg->{package};
		}	

	}
	
	if($os_cyl ne '' || $od_cyl ne '')
	{
		$sql = qq{
		SELECT
		package 
		FROM
		recommended_lenses
		WHERE
		framesize='$framesize'
		AND
		framematerial='$framematerial'
		AND
		lens='$lens'
		AND
		packagetype='$lenstype'
		AND
		((range_type='cyl' AND (range_from <= '$os_cyl' OR range_from <= '$od_cyl') AND (range_to >= '$os_cyl' OR range_to >= '$od_cyl')) OR (type_default='1'))		
		AND
		(under18<>'1' OR under18 IS NULL)
		};
		
		$Tag->query({sql=>$sql,hashref=>'results',table=>'recommended_lenses'});	
		foreach my $pkg (@{$Tmp->{results}})
		{	
			$package = $pkg->{package};
		}		
	}
	
	if(($os_sph ne '' || $od_sph ne '') && ($os_cyl ne '' || $od_cyl ne ''))
	{
		$sql = qq{
		SELECT
		package
		FROM
		recommended_lenses
		WHERE
		framesize='$framesize'
		AND
		framematerial='$framematerial'
		AND
		lens='$lens'
		AND
		packagetype='$lenstype'
		AND
		((range_type='pwr' AND (range_from <= '$os_pwr' OR range_from <= '$od_pwr') AND (range_to >= '$os_pwr' OR range_to >= '$od_pwr')) OR (type_default='1'))
		AND
		(under18<>'1' OR under18 IS NULL)
		};
		
		$Tag->query({sql=>$sql,hashref=>'results',table=>'recommended_lenses'});	
		foreach my $pkg (@{$Tmp->{results}})
		{	
			$package = $pkg->{package};
		}
	}	
	
	#Log('c'.$CGI->{over18_check}.'c');
	
	
	if(!$CGI->{over18_check})
	{
		# *** new ***
		#if the user unchecks the over 18 box we override the recommended lenses
		
		$sql = qq{
		SELECT
		package 
		FROM
		recommended_lenses
		WHERE
		lens='$lens'
		AND
		packagetype='$lenstype'
		AND
		under18='1'
		};
		
		#Log($sql);
		
		$Tag->query({sql=>$sql,hashref=>'results',table=>'recommended_lenses'});	
		foreach my $pkg (@{$Tmp->{results}})
		{	
			$package = $pkg->{package};
		}
	}
	
	### now we have the package we need to get the package code
	### select it from the options_price table
	$sql = 'SELECT pkgcode FROM desc_lens_pkg WHERE pkgletter = \''.$package.'\'';
	$Tag->query({sql=>$sql,hashref=>'results',table=>'desc_lens_pkg'});	
	foreach my $pkg (@{$Tmp->{results}})
	{	
		$package = $pkg->{pkgcode};
	}
	
	return $package;
}
EOR