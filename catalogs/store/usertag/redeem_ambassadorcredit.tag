UserTag redeem_ambassadorcredit			Order
UserTag redeem_ambassadorcredit			addAttr
UserTag redeem_ambassadorcredit			Routine   <<EOR
sub {
	my ($opt) = @_;
	my $username = $Session->{username};
	my $credit_to_redeem = $Values->{use_credit};	
	my $ambassador_id = $Scratch->{ambassador_id} || $Tag->data({table=>"ambassador_userdb",field=>"code",key=>$username,foreign=>"username"});
	
	return if(!$ambassador_id);
	
	my $credit_remain = $credit_to_redeem;
	my ($credit_sub,$new_status,$logmessage);
	
	$logmessage = 'Redeeming:'.$credit_to_redeem.' from:';
	
	my $sql = qq{
	SELECT
	*
	FROM
	ambassador_referral
	WHERE
	ambassador = '$ambassador_id'
	AND type = 'sfp'
	AND status = 'pending'
	ORDER BY amount ASC
	};
	$Tag->query({sql=>$sql,hashref=>'results'});
	foreach my $row (@{$Tmp->{results}})
	{
		$credit_sub = ($credit_remain >= $row->{amount})	? $row->{amount} : $credit_remain;
		$new_status = ($credit_remain >= $row->{amount})	? 'paid' : 'pending';
		
		$logmessage .= $row->{code}.'=>'.$credit_sub.' ';
				
		### update the current row redeeming the amount and updating the status		
		$Tag->query({sql=>"UPDATE ambassador_referral SET amount = '".($row->{amount}-$credit_sub)."', status = '$new_status' WHERE code = '$row->{code}'"});
		
		### insert a record into the ambassador_payments table
		$Tag->query({sql=>"INSERT INTO ambassador_payments (`ambassador`,`referral_code`,`status`,`paid_type`,`amount`) VALUES('$ambassador_id','$row->{code}','success','sfp','$credit_sub')"});
		
		### prepare remaining credit for next row
		$credit_remain -= $credit_sub;
		
		### exit if no credit left to redeem
		last if($credit_remain <= 0); 
	}
	
	Log($logmessage);
	
	return;
}
EOR