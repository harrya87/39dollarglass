UserTag repeat_customer Order username email search_by_email
UserTag repeat_customer Routine <<EOR
sub
{
	my ($username, $email, $search_by_email) = @_;
	my $dbh = dbref('transactions')->dbh();

	
	my $sth = $dbh->prepare(sprintf(
		'SELECT count(*) FROM transactions WHERE %1$s = ?'
	, $search_by_email ? 'email' : 'username'))
		or die "failed to prepare $q: $DBI::errstr\n";

	$sth->execute($search_by_email ? $email : $username)
		or die "failed to prepare select statement: $DBI::errstr\n";

	my $ref = $sth->fetchrow_arrayref;

	return @{$ref}[0] if @{$ref}[0] > 1;
	return undef;
}
EOR
