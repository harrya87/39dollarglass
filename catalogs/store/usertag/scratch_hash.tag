UserTag scratch_hash	Order	hash key
UserTag scratch_hash	Routine   <<EOR
sub {
	my ($hash,$key) = @_;

	return undef unless ref $Scratch->{$hash} eq 'HASH';
	return $Scratch->{$hash}->{$key};
}
EOR