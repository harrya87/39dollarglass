UserTag search_results Order query rpp page_num order_by
UserTag search_results Routine <<EOR
sub {
	my ($query, $want_array, $rpp, $page_num, $order_by) = @_;

	$rpp ||= 24;
	$page_num ||= 1;
	$order_by ||= 're';

	return [] unless $query;
	$query =~ s/Eyeglasses|Sunglasses//gi;
	#keywords are plural in our system for these terms
	$query .= 's' if $query =~ /bifocal|progressive/i;
	my $like_query = $Tag->filter('dbi_quote','%'.$query.'%');

	$Tag->perl({tables => 'search secondary_terms search_log'});
	my $count_query = q|
	SELECT count(*) as resultSet FROM (
		SELECT sku, sum(results_sorter) as results_sorter FROM (
			(
				SELECT
					sku, 200 as results_sorter
				FROM 
					search s
				WHERE 
					s.type = 'frame'
				AND
					(%1$s)
			)  UNION (
				SELECT
					sku, 50 as results_sorter
				FROM 
					search s
				JOIN
					secondary_terms t
				USING (sku)
				WHERE
					s.type = 'frame'
				AND
					( %2$s )
			) UNION (
				SELECT
					sku, 25 as results_sorter
				FROM 
					search s
				WHERE
					s.type = 'frame'
				AND
					( %3$s )
			) UNION (
				SELECT
					sku, 10 as results_sorter
				FROM 
					search s
				WHERE
					s.type = 'frame'
				AND
					( %4$s )
			)
		) AS x 
		WHERE
			results_sorter > 0
		GROUP BY 
			x.sku
	) AS y
	|;

	my $search_query = q|
		SELECT sku, name, color, type, url, CAST( price AS DECIMAL(10,2) ) as price, msrp, cat, date_added, effective_sr, sum(results_sorter) as results_sorter, count(*) as result_set
		FROM (
			(
				SELECT
					sku, name, color, s.type, url, price, msrp, cat, date_added, effective_sr, 200 as results_sorter
				FROM 
					search s
				WHERE 
					s.type = %5$s
				AND
					(%1$s)
			)  UNION (
				SELECT
					sku, name, color, t.type, url, price, msrp, cat, date_added, effective_sr, 50 as results_sorter
				FROM 
					search s
				JOIN
					secondary_terms t
				USING (sku)
				WHERE
					s.type = %5$s
				AND
					( %2$s )
			) UNION (
				SELECT
					sku, name, color, type, url, price, msrp, cat, date_added, effective_sr, 25 as results_sorter
				FROM 
					search s
				WHERE
					s.type = %5$s
				AND
					( %3$s )
			) UNION (
				SELECT
					sku, name, color, type, url, price, msrp, cat, date_added, effective_sr, 10 as results_sorter
				FROM 
					search s
				WHERE
					s.type = %5$s
				AND 
					( %4$s )
			)
		) AS x 
		WHERE
			results_sorter > 0
		GROUP BY 
			x.sku
		%6$s
		%7$s
	|;	

	my $limit_max = $rpp * $page_num;
	my $limit_min = $limit_max - $rpp;
	$limit_max = $rpp;
	my $db = $Db{'search'};
	my %results;
	my $or_clause = '(%1$s sounds like %2$s OR %1$s like %3$s)';
	my $like_clause = '%1$s like %2$s';
	my $sort_map = {
		re => ' results_sorter DESC ',
		na => ' date_added DESC ',
		bs => ' effective_sr DESC ',
		ph => ' price DESC ',
		pl => ' price ASC '
	};
	#TODO:
	#Clean up this query, ugh. It did start nice...
	for my $type (qw\frame article\){
		$results{$type} = $db->query({
			table => 'search',
			hashref => 'please',
			sql => sprintf($search_query, (join " OR \n", (map {
					$_ ? sprintf($or_clause,'s.name',$Tag->filter('dbi_quote', $_),$Tag->filter('dbi_quote', '%'.$_.'%')) : ()
			} (split /\s/, $query))), (join " OR \n", (map {
					$_ ? sprintf($or_clause,'t.term',$Tag->filter('dbi_quote', $_),$Tag->filter('dbi_quote', '%'.$_.'%')) : ()
			} (split /\s/, $query))), (join " OR \n", (map {
					$_ ? sprintf($like_clause,'s.search_color', $Tag->filter('dbi_quote', '%'.$_.'%')) : ()
			} (split /\s/, $query))),(join " OR \n", (map {
					$_ ? sprintf($or_clause,'s.cat',$Tag->filter('dbi_quote', $_),$Tag->filter('dbi_quote', '%'.$_.'%')) : ()
			} (split /\s/, $query))), $Tag->filter('dbi_quote',$type), 'ORDER BY '.($type eq 'frame' ? $sort_map->{$order_by} : ' results_sorter DESC '), $type eq 'frame' ? " LIMIT $limit_min, $limit_max " : '')
		});
	}
	my $resultCount = $db->query({
		table => 'search',
		hashref => 'please',
		sql => sprintf($count_query, (join " OR \n", (map {
				$_ ? sprintf($or_clause,'s.name',$Tag->filter('dbi_quote', $_),$Tag->filter('dbi_quote', '%'.$_.'%')) : ()
		} (split /\s/, $query))), (join " OR \n", (map {
				$_ ? sprintf($or_clause,'t.term',$Tag->filter('dbi_quote', $_),$Tag->filter('dbi_quote', '%'.$_.'%')) : ()
		} (split /\s/, $query))), (join " OR \n", (map {
				$_ ? sprintf($like_clause,'s.search_color',$Tag->filter('dbi_quote', '%'.$_.'%')) : ()
		} (split /\s/, $query))),(join " OR \n", (map {
				$_ ? sprintf($or_clause,'s.cat',$Tag->filter('dbi_quote', $_),$Tag->filter('dbi_quote', '%'.$_.'%')) : ()
		} (split /\s/, $query))))
	});

	$Tag->set('resultCount', $resultCount->[0]->{resultSet});

	unless ($Session->{spider}){
		$Tag->query({
			sql => sprintf(q|INSERT INTO search_log VALUES ( %1$s , %2$s, NOW() )|, (map{
				$Tag->filter('dbi_quote', $_)
			} ($Session->{id}, $query) )),
			table => 'search_log'
		});
	}

	return \%results;
}
EOR
