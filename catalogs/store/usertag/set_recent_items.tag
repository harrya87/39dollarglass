UserTag set_recent_items Order sku sunglasses
UserTag set_recent_items addAttr
UserTag set_recent_items Routine <<EOR
sub
{
	my ($sku, $sunglasses) = @_;

	my @all_recent_items_aoh;
	@all_recent_items_aoh = @{$Scratch->{all_recent_items_array}} if $Scratch->{all_recent_items_array};
	$Scratch->{all_recent_items_count} = unshift(@all_recent_items_aoh, { sku => $sku, sunglasses => $sunglasses } );
	$Scratch->{all_recent_items_array} = \@all_recent_items_aoh;

	$Scratch->{recent_items_limit} = 9;
	$Scratch->{recent_items_count} = 0;

	my $add_item_to_list = 0;

	for (my $i=0; $i < $Scratch->{all_recent_items_count}; $i++)
	{
		if ($Scratch->{recent_items_count} < $Scratch->{recent_items_limit})
		{
			$add_item_to_list = 1;
			for (my $j=0; $j <= $Scratch->{recent_items_count}; $j++)
			{
				if (($Scratch->{'recent_item_' . $j . '_code'} eq $all_recent_items_aoh[$i]{'sku'}) && ($Scratch->{'recent_item_' . $j . '_sunglasses'} eq $all_recent_items_aoh[$i]{'sunglasses'}))
				{
					$add_item_to_list = 0;
				}
			}
			if ($add_item_to_list > 0)
			{
				$Scratch->{recent_items_count} ++;
				$Scratch->{'recent_item_' . $Scratch->{recent_items_count} . '_code'} = $all_recent_items_aoh[$i]{'sku'};
				$Scratch->{'recent_item_' . $Scratch->{recent_items_count} . '_sunglasses'} = $all_recent_items_aoh[$i]{'sunglasses'};
			}
		}
		else
		{
			$i = $Scratch->{all_recent_items_count};
		}
	}
	return;
}
EOR
