# written for 39DG by Jonathan Clark, Webmaint
# 27 Sept 2004

UserTag ship_level Order sku
UserTag ship_level AddAttr
UserTag ship_level Routine <<EOR
sub {
	my ($opt) = @_;

	my $nitems = $Tag->nitems({});
	my $subtotal = $Tag->subtotal({ noformat => 1, });


# possible options are: 
#  0 = subtotal=0
#  1 = normal shipping: 1 item and <$99.
#  2 = reduced shipping: >1 item or > $99.

	return '0.00' unless $subtotal;
	return 1 if $CGI->{use_your_own_frame};
	return 1 if $subtotal < 99;
	return 2;
}
EOR
