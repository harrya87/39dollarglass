UserTag sysvars Routine <<EOR
sub {
	$Tag->perl({tables => 'sysvars'});
	my $db = &Vend::Data::database_exists_ref('sysvars');
	$db = $db->ref();
	my $dbh = $db->[$Vend::Table::DBI::DBI];
	unless ($dbh)
	{
		::logError("Can't create db handle");
		return;
	}
	my $sth = $dbh->prepare(qq!SELECT name, value FROM sysvars!)
		or ::logError("Can't prepare: %s", $dbh->errstr() );
	$sth->execute();
	while (my $row = $sth->fetch )
	{
		$::Variable->{$row->[0]} = $row->[1];
	}
	return;
}
EOR
