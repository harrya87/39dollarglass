UserTag templify Order file type
UserTag templify addAttr
UserTag templify Routine <<EOR
sub {
	my ($file, $type, $opt) = @_;

	my ($template, $prefix, $out, $suffix);

	$opt->{prefix} ||= 'st';

	my $replace = sub {
	    my ($var, $tag) = @_;
	    my $suffix = {
		'br'	=>	'<br>',
		'p'	=>	'<p>',
		'n'	=>	"\n",
	    };
	    return unless $var;
	    return $var . $suffix->{$tag};
	};

	# load the template file in..
	$file = $Tag->filter('filesafe', $file);
	$template = $Tag->file($file);

	# we are only interested in the bit between the markers (assuming we have markers)..

	$template = $1 if ($template =~ m/<\!--\sBEGIN\sCONTENT\s-->([\w\W]+)<\!--\sEND\sCONTENT\s-->/i);

	# replace the markers with their corresponding passed variables..
	if ($type eq 'scratch') {
		$template =~ s/\$([A-Za-z0-9_-]+)(\^([a-z]+))?\$/$replace->($Scratch->{"$opt->{prefix}_$1"}, $3)/ge;
	} else {
		$template =~ s/\$([A-Za-z0-9_-]+)(\^([a-z]+))?\$/$replace->($opt->{"$opt->{prefix}_$1"}, $3)/ge;
	}

	return $template;
}
EOR
