var resultHeight;	
var filterCatList = Array('a','m','w');
var filterSizeList = Array('a','nw','wn');
var filterMaterialList = Array('m','p','t','r','s');
var currentCatFilter = get_cookie("productCatfilter");
var currentSizeFilter = 'a'; // = get_cookie("productSizefilter");

function set_cookie(name,value,exp_y,exp_m,exp_d,path,domain,secure)
{
  var cookie_string = name + "=" + escape ( value );

  if (exp_y)
  {
    var expires = new Date ( exp_y, exp_m, exp_d );
    cookie_string += "; expires=" + expires.toGMTString();
  }
  if(path)
  {
  	cookie_string += "; path=" + escape ( path );
	}	
  if(domain)
  {
  	cookie_string += "; domain=" + escape ( domain );
  }  
  if(secure)
  {
  	cookie_string += "; secure";
  }
  
  document.cookie = cookie_string;
}

function get_cookie(name)
{
  var results = document.cookie.match(name + '=(.*?)(;|$)');
  if (results)
  {
		return (unescape(results[1]));
  }
  else
  {
		return null;
  }
}

function filterProducts(type,value)
{
	if(type == 'cat')
	{
		//currentCatFilter = filter.replace(/filterCat_/,'');
		currentCatFilter = value;
	}
	/*
	else if(type == 'material')
	{
		category = value;
		$('pageCategoryTitle').update(category);
	}
	*/
	/*
	else if(type == 'size')
	{
		currentSizeFilter = value;
	}
	*/
	resultHeight = $('productListResults').getHeight();
	new Effect.Fade('productListResults',{duration:0.3});
	$('productListLoading').show();
	setTimeout("showPlaceHolder(); getProducts();",300);
}	

function showPlaceHolder()
{
	$('productListPlaceHolder').show();
	$('productListPlaceHolder').setStyle({'height': resultHeight+'px'});	
}

function setSelectedOption()
{		
	//clear all options
	for(i=0;i<filterCatList.length;i++)
	{
		$('filterCat_'+filterCatList[i]).selected = '';		
	}
	$('filterCat_'+currentCatFilter).selected = true;
}

function setMaterial()
{
	//set the current material option
	for(i=0;i<filterMaterialList.length;i++)
	{
		$('filterMaterial_'+filterMaterialList[i]).selected = '';
		if($('filterMaterial_'+filterMaterialList[i]).value == category)
		{
			$('filterMaterial_'+filterMaterialList[i]).selected = true;
		}
	}	
}

function setSize()
{
	//set the current size option
	for(i=0;i<filterSizeList.length;i++)
	{
		$('filterSize_'+filterSizeList[i]).selected = '';
	}	
	$('filterSize_'+currentSizeFilter).selected = true;
}

function getProducts(start)
{	
	if(start)
	{
		$('productListFilterCatSelect').onchange = function() {filterProducts('cat',this.options[this.selectedIndex].value); return false};
		//$('productListFilterSizeSelect').onchange = function() {filterProducts('size',this.options[this.selectedIndex].value); return false};	
		//$('productListFilterMaterialSelect').onchange = function() {filterProducts('material',this.options[this.selectedIndex].value); return false};						
		if((!currentCatFilter) || (currentCatFilter == 'a'))
		{
			currentCatFilter = 'a';
			setSelectedOption();
			return false;
		}
		else
		{
			setSelectedOption();			
		}		
		if((!currentSizeFilter) || (currentSizeFilter == 'a'))
		{
			currentSizeFilter = 'a';	
		}
		//setSize();
		//setMaterial();
	}
	
	
	if(sunglasses)
	{
		sunglasses = '1';	
	}
	else
	{
		sunglasses = '';	
	}
	
	var url = '/10000_load.html';	
	new Ajax.Request(url,
	{
	  method: 'post',
	  parameters: { 'category': category , 'cat': currentCatFilter , 'size': currentSizeFilter, 'sun': sunglasses },
/*
	  parameters: { 'category': category , 'cat': currentCatFilter , 'size': currentSizeFilter, 'sun': sunglasses, evalJS: true },
*/
	  onSuccess: function(transport)
	  {	  	
	   	$('productListResults').update(transport.responseText);		    	
	   	new Effect.Appear('productListResults',{duration:0.3});
	   	//setCurrentLink();
	   	
	   	var current_date = new Date;
	    var cookie_year = current_date.getFullYear() + 1;
	    var cookie_month = current_date.getMonth();
	    var cookie_day = current_date.getDate();
	    set_cookie("productCatfilter",currentCatFilter,cookie_year,cookie_month,cookie_day);
	    set_cookie("productSizefilter",currentSizeFilter,cookie_year,cookie_month,cookie_day);
	   	
	   	//wait before hiding place holder otherwise layout shifts
	   	//because of show start to appear effect.
	   	setTimeout("$('productListPlaceHolder').hide();",50); 	   	
	   	setTimeout("$('productListLoading').hide();",300);
	  }
	});	
}
