var linkMenuTimer;
var linkMenuCurrent;
var linkMenuMoving = false;
var linkMenuClosing = false;
/* var linkMenus = 'login,currency,howtoorder'; */
var linkMenus = 'login';

function linkMenuCheck(e){
	var target = (e && e.target) || (event && event.srcElement);
	//check if we are clicking inside the div or outside
	//clicking outside will return true therefore close the menu
	var clickedLink = false;
	linkMenus.split(',').each(
		function(linkitem)
		{
			if(target == $(linkitem+'MenuLink'))
			{		
				if(linkMenuCurrent)
				{
					linkMenuCloseNow();		
				}
				if(linkitem == 'login')
				{
					if(!loggedin)
					{
						showLink('login');
					}
					else
					{
						window.location='/customerservice.html';
					}
				}
				else
				{
					showLink(linkitem);	
				}
				clickedLink = true;
			}	
		}
	);	
	
	var parent = linkMenuCheckParent(target);
	if((parent) && (!clickedLink) && (linkMenuCurrent))
	{
		linkMenuClose();
	}
	
}

function linkMenuCheckParent(t)
{
	if(linkMenuCurrent)
	{
		while(t.parentNode)
		{			
			if(t==$(linkMenuCurrent+'Menu'))
			{
				return false;
			}
			t=t.parentNode;
		}
	}
	return true;
} 

function showLinkInit()
{	
	document.onclick=linkMenuCheck;
	linkMenus.split(',').each(
		function(linkitem)
		{
			$(linkitem+'MenuLink').onclick = function()
			{
				return false;
			}
		}
	);
}

function showLink(menuitem) {
	if(linkMenuClosing) {
		//still closing, wait for it to close
		setTimeout("showLink('"+menuitem+"');",200);
	} else {
		showLinkShow(menuitem);	
	}
}

function showLinkShow(menuitem) {	
	//*** this loads the menu
	//first make sure its not already animating
	if(!linkMenuMoving)
	{
		linkMenuMoving = true;
		linkMenuCurrent = menuitem;
		var linkPos = getObjectPos(menuitem+'MenuLink'); //get the position of the link
		$(menuitem+'Menu').setStyle({left:linkPos.x+'px',top:(linkPos.y + 20)+'px'});
		$(menuitem+'MenuContent').setStyle({backgroundColor:'#fff'});
		new Effect.Grow(menuitem+'Menu',{direction:'top-left', duration:0.3, queue: {position:'end', scope:'linkMenu'}});
		setTimeout("$('"+menuitem+"MenuContent').setStyle({display:'block'});linkMenuMoving=false;",300);
	}
	linkMenuClear();
}

function linkMenuCloseInit(timeout) {
	if(!timeout)
	{
		timeout = 300;
	}
	if(!linkMenuMoving) {
		linkMenuTimer = setTimeout("linkMenuClose();",100);
	} else {
		linkMenuTimer = setTimeout("linkMenuClose();",timeout);
	}
}

function linkMenuCloseNow()
{
	$(linkMenuCurrent+'MenuContent').hide();	
	$(linkMenuCurrent+'Menu').hide();
	linkMenuMoving=false;
	linkMenuClosing=false;
	linkMenuCurrent=false;	
}

function linkMenuClose() {
	
	linkMenuMoving = true;
	linkMenuClosing = true;
	
	linkMenus.split(',').each(
		function(linkitem)
		{
			$(linkitem+'MenuContent').hide();			
			new Effect.Shrink(linkitem+'Menu',{direction:'top-left', duration:0.3, queue: {position:'end', scope:'linkMenu'}});
		}
	);

	setTimeout("linkMenuMoving=false;linkMenuClosing=false;linkMenuCurrent=false;",300);
}

function linkMenuClear() {
	clearTimeout(linkMenuTimer);	
}
