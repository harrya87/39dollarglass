/*
 * jQuery hashchange event - v1.3 - 7/21/2010
 * http://benalman.com/projects/jquery-hashchange-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,e,b){var c="hashchange",h=document,f,g=$.event.special,i=h.documentMode,d="on"+c in e&&(i===b||i>7);function a(j){j=j||location.href;return"#"+j.replace(/^[^#]*#?(.*)$/,"$1")}$.fn[c]=function(j){return j?this.bind(c,j):this.trigger(c)};$.fn[c].delay=50;g[c]=$.extend(g[c],{setup:function(){if(d){return false}$(f.start)},teardown:function(){if(d){return false}$(f.stop)}});f=(function(){var j={},p,m=a(),k=function(q){return q},l=k,o=k;j.start=function(){p||n()};j.stop=function(){p&&clearTimeout(p);p=b};function n(){var r=a(),q=o(m);if(r!==m){l(m=r,q);$(e).trigger(c)}else{if(q!==m){location.href=location.href.replace(/#.*/,"")+q}}p=setTimeout(n,$.fn[c].delay)}$.browser.msie&&!d&&(function(){var q,r;j.start=function(){if(!q){r=$.fn[c].src;r=r&&r+a();q=$('<iframe tabindex="-1" title="empty"/>').hide().one("load",function(){r||l(a());n()}).attr("src",r||"javascript:0").insertAfter("body")[0].contentWindow;h.onpropertychange=function(){try{if(event.propertyName==="title"){q.document.title=h.title}}catch(s){}}}};j.stop=k;o=function(){return a(q.location.href)};l=function(v,s){var u=q.document,t=$.fn[c].domain;if(v!==s){u.title=h.title;u.open();t&&u.write('<script>document.domain="'+t+'"<\/script>');u.close();q.location.hash=v}}})();return j})()})(jQuery,this);
/* END PLUGIN */

if (location.hash != '#lenstype' && window.location.href.match(/step2/)){
	location.hash = '#lenstype';
}


var pageload = {
    ignorehashchange: false,
    loadUrl: function(){
        if (pageload.ignorehashchange == false){
        	var hash = location.hash;
        	if (!hash){
        		hash = 'lenstype';
        	}
        	hash = hash.replace(/^#/,'');
        	if (hash != 'tintopt'){
	        	var $this = $('#hash_'+hash);
				$('div.stepContainer:visible').slideUp('slow','linear',function(){
					$('#'+$this.attr('data')).slideDown('slow','linear',function(){
						try{
							eval($this.attr('data-func')+'()');
						}catch (ex){}
					});
				});
			}
        }
    }
};

// will be populated with appropriate error codes
// by checkForm() function in different forms
errs = new Array();
var input_errs = [];
var allow_ar, chose_ar,selected_tint,no_tint,include_ar, ar_included;
function calcPrice(f) {

	if(f){
		var the_form = eval(f);
		var the_form_text = f;
	} else {
		var the_form = document.orderform;
		var the_form_text = 'document.orderform';
	}

	if( !the_form ) return;

	var ar_div = document.getElementById('ar_div');

	// ***
	if (the_form.mv_order_coating && !the_form.mv_order_tint.value) {
		var my_lens_materials = new Array();
		my_lens_materials = checkedVals(the_form.mv_order_lenstype);
		var my_lens_material = my_lens_materials[0];
		if(!my_lens_material){
			my_lens_material = the_form.mv_order_lenstype[0].value;
		}
		if( my_lens_material.indexOf('SUN') !=-1 || my_lens_material.indexOf('POLS') != -1 || my_lens_material.indexOf('TRAN') != -1 ) {
			//Check to see if the selected package is a SmartColors package
			if( lens_material == 'TRAN10' || lens_material == 'BIF_TRAN11' || lens_material == 'PROGE_TRAN12' || lens_material == 'PROG_TRAN13' ){
				the_form.mv_order_tint.value = 'G15';
			}else{
				the_form.mv_order_tint.value = 'GREY';
			}
		} else {
			the_form.mv_order_tint.value = 'CLEAR';
		}
	}
	// ***

	// check whether A/R is allowed or not
	if (the_form.mv_order_coating && the_form.mv_order_lenstype) {

		var the_lens_materials = new Array();
		the_lens_materials = checkedVals(the_form.mv_order_lenstype);
		the_lens_material = the_lens_materials[0];
		if(!the_lens_material){
			the_lens_material = the_form.mv_order_lenstype[0] ? the_form.mv_order_lenstype[0].value : '';
		}

		//if (the_lens_material == 'SUN1' || the_lens_material == 'SUN3' || the_lens_material == 'SUN3_2' || the_lens_material == 'SUN9' || the_lens_material == 'SUN10' || the_lens_material == 'SUN9_2' || the_lens_material == 'BIF_SUN11' || the_lens_material == 'BIF_SUN12' || the_lens_material == 'BIF_SUN13' || the_lens_material == 'PROGE_SUN7' || the_lens_material == 'PROGE_SUN7_2' || the_lens_material == 'PROGE_SUN18' || the_lens_material == 'PROGE_SUN19') {
		if (the_lens_material.indexOf('SUN') !=-1 || the_lens_material == 'POLS12' || the_lens_material == 'POLS13' || the_lens_material == 'PROG_TRAN8' || the_lens_material == 'PROGE_TRAN6' || the_lens_material == 'BIF_TRAN5' )
		{
			// packages C, D, I
			// don't allow you to select A/R
			var the_coatings = new Array();
			the_coatings = checkedVals(the_form.mv_order_coating);
			the_coating = the_coatings[0];

			ar_div.style.display = 'none';
			allow_ar = !1;
			if (the_coating == 'COA1') {
				the_form.mv_order_coating[0].checked = true;
				//alert('Anti-reflective coating is not available with this package\nPlease select another package if you require anti-reflective coating');
			}
		}
		else{
			ar_div.style.display = 'block';
			$('#choose_ar').show();
			$('#ar_included').hide();
			allow_ar = !0;
		}

		if (the_form.mv_order_coating.checked) {
			// package T, C2 & D2
			if (the_lens_material == 'SUN8' || the_lens_material == 'SUN3' || the_lens_material == 'SUN10' || the_lens_material == 'BIF_SUN13' || the_lens_material == 'PROGE_SUN19') {
				// turn off A/R
				//alert('Anti Reflective Coating is not available with this package');
				the_form.mv_order_coating.checked = false;

				// swap the divs over
				ar_div.style.display = 'none';
				allow_ar = !1;
				//return;
			}
		} else {
			// if user clicks elsewhere, turn A/R back on if it's otherwise mandatory
			if (the_form.mv_order_coating.value == 'COA4' && the_form.mv_order_coating.checked == false
				&& the_lens_material != 'SUN8' && the_lens_material != 'SUN1-2' && the_lens_material != 'SUN3' && the_lens_material != 'SUN3_2' && the_lens_material != 'SUN10' && the_lens_material != 'BIF_SUN13' && the_lens_material != 'PROGE_SUN19') {
				the_form.mv_order_coating.checked = true;

				// swap the divs over
				ar_div.style.display = 'block';
				$('#choose_ar').show();
				$('#ar_included').hide();
				allow_ar = !0;
				//return;
			}
		}
		if(the_lens_material == 'OPT4' || the_lens_material == 'OPT10' || the_lens_material == 'OPT11'){
			ar_div.style.display = 'none';
			allow_ar = !1;
		}
	}
	// end of A/R bit

	// get current prices
	//var your_cost = base_price;
	//var avg_retail = base_retail;
	var your_cost = 0, avg_retail = 0;
	var num_groups = check_groups.length;
	for(var idx=0; idx < num_groups; idx++) {
		form_group = eval(the_form_text+"."+check_groups[idx]);
		if(form_group) {
			var j = form_group.length;
			var jtype = form_group.type;
			if((jtype != 'radio') && (form_group.name == 'mv_order_case')){
                your_cost += cost[form_group.name][form_group.value];
                avg_retail += retail[form_group.name][form_group.value];
			} else {
				// if there are multiple options for this radio group
				if(j > 1) {
					for(var i=0; i< j; i++) {
						input_el = eval(the_form_text+"."+check_groups[idx] + '[' + i + ']');
						if(input_el.checked) {
							your_cost += cost[input_el.name][input_el.value];
							avg_retail += retail[input_el.name][input_el.value];
							break;
						}
					}
				// if there is one option for this radio group
				} else {
					input_el = eval(the_form_text+"."+check_groups[idx]);
					if(input_el.checked) {
						your_cost += cost[input_el.name][input_el.value];
						avg_retail += retail[input_el.name][input_el.value];
					}
				}
			}
		}
	}
	if(avg_retail == 0){
		avg_retail = base_retail;
	}
	if(your_cost == 0){
		your_cost = base_price;
	}else if (sunglasses){
	   your_cost += 9.95; 
	}
	
	// set price boxes
	var you_save = avg_retail - your_cost;
	you_save = roundCurrency(you_save);

	the_form.your_cost.value = roundCurrency(your_cost);
	the_form.avg_retail.value = roundCurrency(avg_retail);
	the_form.you_save.value = roundCurrency(you_save);

	$('.youcost_box .youcost_bcprice').html('$' + roundCurrency(your_cost));
	$('.youcost_box .avg_retail').html('$' + roundCurrency(avg_retail));
	$('.youcost_box .yousave').html(' $' + roundCurrency(you_save));
	if(the_form.your_cost2) {
		the_form.your_cost2.value = roundCurrency(your_cost);
		the_form.avg_retail2.value = roundCurrency(avg_retail);
		the_form.you_save2.value = roundCurrency(you_save);
	}
	//var cart_subtotal = 0;
	//if (typeof total_cost !== 'undefined'){
	//	cart_subtotal = total_cost;
	//}
	if( ( $('#lenspackage:visible, #lensoptions:visible').length > 0 || window.location.hash == '#lenspkg' || window.location.hash == '#tintopt' ) && 
			( ( parseFloat(roundCurrency(your_cost)) ) >= 99 )
	){
		$('#ship_msg').show();
	} else {
		$('#ship_msg').hide();
	}
}

function errClear(step){
	errordisplay = document.getElementById('form_error_display');
	if(errordisplay) errordisplay.style.display='none';
}
// if any errors were found in the form, display the error window
function errCheck(locale, step) {
	step = step || '';
	var err_count = errs.length;
	if(err_count > 0) {
		var err_string;
		if(locale == 'es_ES'){
			err_string = '<b>Hay un peque�o problema</b><ul>';
		} else {
			err_string = '<b>There is a slight problem:</b><ul>';
		}
		//err_string += errs.join('<br />');
		errordisplay = document.getElementById('form_error_display');
		errormsg = document.getElementById('form_error_msg');

		//hide the errorblock
		errClear(step);

		// minimum v4.x browsers
		//if (parseInt(navigator.appVersion) >= 4) {
		//	popWin('/messages/error.php?' + err_string + '&locale=' + locale,'error');
		//}
		if(errordisplay && errormsg){
			for(i=0;i<err_count;i++){
				err_string += '<li>'+form_error_array[errs[i]]+'</li>';
			}
			errormsg.innerHTML = err_string+'</ul>';
			errordisplay.style.display='block';
		} else {
			//for some reason we dont have and object to display the error message
			//use stand alert inthis case
			err_string = "Error:\n";
			for(i=0;i<err_count;i++)
			{
				// cameron added input name debugging
				// err_string += '(' + errs[i] + ') ' + form_error_array[errs[i]]+"\n";
				err_string += form_error_array[errs[i]]+"\n";
				alert(err_string);
			}
		}
		$.each(input_errs,function(i,v){
			if (v == 'mv_order_pupd'){
				$('#ospd, #odpd').addClass('error');
				return !0;
			}
			$('input[name='+v+'],select[name='+v+']').addClass('error');
		});
		input_errs = [];
		// clear the error stack
		errs = new Array();
		return false;
	}
	return true;
}

function doChecks(el, halt) {
	if(tintCheck(el, halt)) {
		if(el.name == 'mv_order_lenstype'){
			selectLensPackage($(el).val());
		}
		calcPrice();
		return true;
	} else {
		return false;
	}
}

var all_tints = new Array('GREY', 'BROWN', 'G15', 'BLUE', 'ROSE', 'ORANGE', 'YELLOW','CLEAR');

function hideTints() {
	for(i=0;i<all_tints.length;i++) {
    	$('#tint_'+all_tints[i]).hide();
    }
	$('#lensoptions').hide();
	$('#colorcombinations').hide();
}

function clearTints() {
	$('ul.lens_tint li a').each(function(){
		$(this).removeClass('selected');
	});
}

var currentTint;
function setTint(tc) {
	clearTints();
	$('#form_error_display').hide();
	currentTint = document.getElementById('mv_order_tint').value;
	if(tc && tc != 'CLEAR'){
		$('#tint_'+tc).addClass('selected');
	}
	document.getElementById('mv_order_tint').value = tc;
	currentTint = tc;
	$('li.tintOption').removeClass('selected');
	$('li.tintOption input').removeAttr('checked');           
	$('li.tintOption[data-val='+the_form.mv_order_tint.value+']')
	 .addClass('selected').children('input').attr('checked','checked');
	 $('#tintPreviewer').attr('src', $('#tintPreviewer').attr('src').replace(/CLEAR/,tc) )
}

function clearFields(){
	$('.prescription_table select').each(function(){
		$(this).val([]);
	});
	$('#monocularPD').prop('checked',!1);
	$('#ospd').hide();
	$('#rxcomments').val('');
}

function previewTints(){
	$('.tintOption').unbind().click(function(e){
		e.stopImmediatePropagation();
		e.preventDefault();
		$('.tintOption.selected input').removeAttr('checked');
		$('.tintOption').removeClass('selected');
		$(this).addClass('selected');
		$('#tintPreviewer').attr('src',$(this).attr('data'));
		$('#orderform input[name=mv_order_tint]').val($(this).attr('data-val'));
		setTimeout("$('.tintOption.selected input').attr('checked','checked');", 100);
		return false;
	}).hover(function(){
		$('#tintPreviewer').attr('src',$(this).attr('data'));
	},function(){
		$('#tintPreviewer').attr('src',$('.tintOption.selected').attr('data'));
	});
}
var allowed_tints_count = 0;

function tintCheck(el, halt) {

	var the_form = document.orderform;
	// clr: clear lens
	// sun: regular sun lenses
	// pol: polarized sun lenses
	// trn: transitions

	// these lens types have only certain tints allowed
	var allowed_tints = new Array();
	allowed_tints_count = 0;

	allowed_tints['clear'] = new Array();
	allowed_tints['sun'] = new Array('BROWN', 'GREY', 'G15', 'BLUE', 'ROSE', 'ORANGE', 'YELLOW');
	allowed_tints['pol'] = new Array('BROWN', 'GREY');
	allowed_tints['tran'] = new Array('BROWN', 'GREY');
	allowed_tints['proge'] = new Array('BROWN', 'GREY', 'G15');

	//New color array for Smart Colors lens packages
	allowed_tints['smart_colors'] = new Array('G15', 'BLUE', 'ROSE', 'ORANGE', 'YELLOW');

	//package specific tints
	allowed_tints['PROGE_TRAN5'] = new Array('GREY');

	// get lens material code
	var lens_materials = new Array();
	lens_materials = checkedVals(the_form.mv_order_lenstype);
	lens_material = lens_materials[0];
	var lens_material_code;

	// OPT are always clear++++++++++++++++++
	if((lens_material.indexOf('OPT') == 0)
		|| (lens_material.indexOf('OPT') == 4)
		|| (lens_material.indexOf('OPT') == 5)
		|| (lens_material.indexOf('OPT') == 6)) {
		lens_material_code = 'clear';
	}

	// SUN 1 and 3 can have any tint
	//if(lens_material == 'SUN1' || lens_material == 'SUN3' || lens_material == 'SUN3_2' || lens_material == 'SUN8' || lens_material == 'SUN9' || lens_material == 'SUN10') {
	// any other SUN are polycarbonate, can only have CLEAR, BROWN or GREY
	if (lens_material.indexOf('SUN') !=-1) {
		lens_material_code = 'sun';
	} else if((lens_material.indexOf('SUN') == 0)|| (lens_material.indexOf('SUN') == 4) || (lens_material.indexOf('SUN') == 5)) {
		lens_material_code = 'pol';
	}
	if (lens_material.indexOf('POLS')!=-1) {
		lens_material_code = 'pol';
	}
	if(lens_material.indexOf('TRAN') !=-1) {
		lens_material_code = 'tran';
	}

	//If the package is in the list of Smart Colors packages
	//then limit the available tints to only those available on SC pkgs
	if( lens_material == 'TRAN10' || lens_material == 'BIF_TRAN11' || lens_material == 'PROGE_TRAN12' || lens_material == 'PROG_TRAN13' ){
		lens_material_code = 'smart_colors';
	}


	// get lens tint checked
	var lens_tints = new Array();
	lens_tint = document.getElementById('mv_order_tint').value;

	if(el.name != 'mv_order_varilux') {
		clearTints();
	}

	// if a clear lens material has been checked, set mv_order_tint to clear
	// otherwise unset mv_order_tint
	if(el.name == 'mv_order_lenstype') {
		if(lens_material_code == 'clear') {
			setTint('CLEAR');
		}else if(lens_material_code == 'smart_colors' ) {
			setTint('G15');
		}else{
			setTint('GREY');
		}
	}

	// if they've checked a lens tint (or clear) make sure it matches the material
	// if a lens tint or lens material has just been checked, make sure the tints are allowed tints
	if(el == 'mv_order_tint' || el.name == 'mv_order_lenstype') {
		// if there's no lens tint checked, we can skip this
		if(lens_tint) {
			// does this material only allow certain tints?
			var count = $('body').data('tintCount',0);
			for(i=0;i<all_tints.length;i++) {
				var allowed_array = allowed_tints[lens_material_code];
				if( $.isArray(allowed_tints[el.value]) ){
					allowed_array = allowed_tints[el.value];
				}
				if($.inArray(all_tints[i], allowed_array) >= 0) {
					$('#tint_'+all_tints[i]).show();
					++allowed_tints_count;
					var count = jQuery('body').data('tintCount');
					$('body').data('tintCount', ++count);
				} else {
					$('#tint_'+all_tints[i]).hide();
				}
			}
		}
		$('#colorcombinations').show();
	}
	if (!allowed_tints[lens_material_code].length){
		$('#tintPreviewPanel').hide();
		$('#slide_ar_div').show();
		no_tint = !0;
	}
	$('#ar_included, #drivewear_tint_info').hide();
	$('#lensoptions_title').text('Customize your lenses further');
	$('#varilux_div').show();
	$('#arVal').show();
	if(lens_material == 'POLS9' || lens_material == 'BIF_POLS10' || lens_material == 'PROG_POLS11'){
		clearTints();
		hideTints();
		setTint("GREY");
		//show the drivewear information
		//Your Drivewear lenses change from:
		$('body').data('tintCount', 0)
		$('#lensoptions_title').text('Your Drivewear lenses change from:');
		$('#tintPreviewPanel').hide();
		$('#drivewear_tint_info').show();
		$('#varilux_div').hide();

	} else if (el.value == 'OPT4' || el.value == 'OPT10' || el.value == 'OPT11') {
		clearTints();
		hideTints();
		setTint("CLEAR");
		$('body').data('tintCount', 0)
		$('#lensoptions_title').text('Our premium anti-reflective coating is included');
		$('#ar_div, #ar_included, #coatoptpanel #ar_included').show();
		ar_included = true;
		$('#include_ar').val('COA4');
		$('#varilux_div').hide();
		$('#arVal').hide();
		selected_coat = !0;
	}

	// Varilux checks
	varilux = eval('document.orderform.mv_order_varilux');
	sola_ok = eval('document.orderform.sola_ok');

	// if sola's not ok, this field can't be changed
	if(!sola_ok && varilux) {
		varilux.checked = true;
	}
	return true;
}

/* Begin what was step123.js */
var mousePos, t,
xmouse = 0,
ymouse = 0,
xoffset = 200,
yoffset = 350,
largeCaseOn = false,
selectedCase = 'CLAMSHELL',
firstSelected = false,
showingOptions = false,
shownCaseSelect = true,
selectingLens = false,
firstSelectedType = false,
selectingLensType = false,
selectingPrescription = false,
showingPrescription = false,
selectingNosePos = false,
showingNosePos = false,
selectsHidden = false,
loadedprescription = false,
shownosepos = false,
rxchecked = false,
rxerror = false;

function hideSelects(theaction) {
	if( theaction != 'visible' ){
		selectsHidden = true;
	}else{
		selectsHidden = false;
	}
	$('select').toggle();
}

function proceed_button_swap(btnobj,state){
	if(state == 'over') {
		document.getElementById(btnobj).className = 'proceedButtonOver';
	} else {
		document.getElementById(btnobj).className = 'proceedButton';
	}
}

//google analytics
var ga_package_name;
var ga_price;
var ga_pckg_cat;
function selectLensType(){
	if(!$('input[name=mv_order_lens]:checked').length){
		return;
	}

	var the_form = document.orderform, selectedLensType;
	if(the_form.mv_order_lens.length) {
		selectedLensType = checkedVals(the_form.mv_order_lens);
	} else {
		selectedLensType = the_form.mv_order_lens.value;
	}
	var nosepos = $('input[name=mv_order_nosepos]:checked').val();
	$('#currentLensTypeSelection').html(
		$('#mv_order_lens_'+selectedLensType).next().text() + 
		(nosepos ? ('<br /><span class="nosepos">&nbsp;-'+$('input[name=mv_order_nosepos]:checked').attr('data')+'</span>') : '')
	);
	$('#mvol_display').fadeIn('fast','linear');
	$('#orderform input[name=mvold]').val(
		$('#mv_order_lens_'+selectedLensType).next().text()
	);
	var crumb =$('#hash_lenstype').show().clone();
	crumb.attr('id', 'crumb_'+crumb.attr('id')).text('Lens Type');
	if(!$('#'+crumb.attr('id')).length){
		$('#stepCrumbs ul').append($('<li />').append(crumb).prepend('<span>&raquo;</span>'));
	}
	if(selectedLensType){
		errClear();
		update_noseposition();
		if(!shownosepos){
			hideLensTypeSelection();
			//google analytics
            try{
                if(_gaq){
                    var elem = document.getElementsByName('mv_order_lens');
                    var len = elem.length;
                    var id_name;
                    for (var i=0;i<len;i++){   
                        if(elem[i].checked){
                            id_name = 'st' + i;
                            ga_price = document.getElementById(id_name.toString()).innerHTML.toString().substring(1);
                            ga_package_name = elem[i].value.toString();
                            _gaq.push(['_trackEvent', 'Frame Rx', ga_package_name.toString(), ga_price.toString()]);
                        }                    
                    }  
                }
            }catch(err){}
		}
		calcPrice();
	}
}

function loadPrescription(step){
	if(!loadedprescription){
		$('#prescription_data').load( '/step2_prescription.html?r=' + Math.random()*100, function(response,status){
			$('#prescription_loading').hide();
			$('#prescription_data .prescription_slide').slideDown();
			$('#nextStep').show();
			if( status == 'error' ) {
				$('#prescription_loading').css('border','1px solid red').text('Failed to load your prescription, please try again. If you encounter this error again please contact customer service.');
			}
		});
	}
	loadedprescription = true;
}

function reloadNosePos(){
	var the_form = document.orderform;
	var selectedNosePos = checkedVals(the_form.mv_order_nosepos);
	if(selectedNosePos){
		selectNosePosition();
	}
}

function showPrescriptionBox(){
	if( (selectingLensType == false) && (showingPrescription == false) ){
		$('#prescriptionbox').slideDown(700, 'linear');
		selectingLensType = true;
		showingPrescription = true;
		setTimeout("selectingLensType = false;",700);
		pageload.ignorehashchange = true;
		location.hash = '#prescription';
		setTimeout(function(){pageload.ignorehashchange = false;}, 100);
		if( !loadedprescription ){
			//wait 1 second for animation to finish then load the prescription
			setTimeout("loadPrescription();",1000);
		}
	}
}

function hideLensTypeSelection(){
	if(!$('input[name=mv_order_lens]:checked').length){
		return;
	}

	if(selectingLensType == false){
		var the_form = document.orderform,selectedLensType;
		if(the_form.mv_order_lens.length) {
			selectedLensType = checkedVals(the_form.mv_order_lens);
		} else {
			selectedLensType = the_form.mv_order_lens.value;
		}

		try{
			var effects = new Array();
			selectingLensType = true;

			effects.push( function(){
				$('#lensbox').slideUp(400,'linear',function(){
					selectingLensType = false;
					showPrescriptionBox();
				});
			});

			//setTimeout("selectingLensType = false; showPrescriptionBox();",400);
			if((!selectingNosePos) && (showingNosePos)){
				selectedLensType = selectedLensType.toString();
				if((selectedLensType.indexOf('PROG') != -1) || (selectedLensType.indexOf('PROGE') != -1) || (selectedLensType.indexOf('BIF') != -1)){
					selectingNosePos = true;
					showingNosePos = false;

					//clearHighlight();
					$('#nosepositionbox').slideUp(300,'linear',effects.pop());
					setTimeout("selectingNosePos = false;",300);
				}else{
					if($('#nosepositionbox').is(':visible') ){
						showingNosePos = false;
						$('#nosepositionbox').slideUp(300, 'linear', effects.pop() );
					}
				}
			}
			$(effects).each( function(i,func){
				func();
			});
		}catch(error){
			selectingLensType = false;
			setTimeout(hideLensTypeSelection,500);
			return;
		}
	}
}

var RxCommentsOn = false;

function toggleRxComments(){
	$('#manualRx').toggle();
	return true;
}

function changeTypeSelection(){
	if(selectingLensType == false){
		jQuery('input[type=radio][name=mv_order_lens]:checked, input[type=radio][name=mv_order_nosepos]:checked').attr('checked','');
		var the_form = document.orderform, selectedLensType;
		if(the_form.mv_order_lens.length) {
			selectedLensType = checkedVals(the_form.mv_order_lens);
		} else {
			selectedLensType = the_form.mv_order_lens.value;
		}

		// Queue up the replacement of the lens type selection with the currently selected lens type
		// (This is queued so that the noseposition box is animated before if it needs to be)
		$('#form_error_display').hide();
		selectingLensType = true;
		$('#lensbox').slideDown(300, 'linear', function(){ 
			selectingLensType = false; 
		});

		if((selectedLensType.indexOf('PROG') != -1) || (selectedLensType.indexOf('PROGE') != -1) || (selectedLensType.indexOf('BIF') != -1)){
			selectingNosePos = true;
			showingNosePos = true;

			$('#nosepositionbox').delay(600).slideDown(300,'linear',function(){ 
				selectingNosePos = false; 
			});
		}
	}
	return false;
}

function selectNosePosition(){
	errClear();
	if(!selectingLensType){
		hideLensTypeSelection();
		//google analytics
        try{
            if(_gaq){
                var elem = document.getElementsByName("mv_order_lens");
                var len = elem.length;
                var id_name;
                for (var i=0;i<len;i++){   
                    if(elem[i].checked){
                        id_name = "st" + i;
                        ga_price = document.getElementById(id_name.toString()).innerHTML.toString().substring(1);
                        ga_package_name = elem[i].value.toString();
                        _gaq.push(['_trackEvent', 'Frame Rx', ga_package_name.toString(), ga_price.toString()]);
                    }                    
                }
            }
        }catch(err){}
		if(!showingPrescription){
			$('#prescriptionbox').delay(1000).slideDown(300,'linear',function(){
				selectingPrescription = true;
				showingPrescription = true;
				selectingPrescription = false;
			});

			if(!loadedprescription){
				setTimeout("loadPrescription();",1000);
			}
		}
	}
	var the_form = document.orderform, selectedLensType;
	if(the_form.mv_order_lens.length) {
		selectedLensType = checkedVals(the_form.mv_order_lens);
	} else {
		selectedLensType = the_form.mv_order_lens.value;
	}
	var nosepos = $('input[name=mv_order_nosepos]:checked').val();
	$('#currentLensTypeSelection').html(
		$('#mv_order_lens_'+selectedLensType).next().text() + 
		(nosepos ? ('<br /><span class="nosepos">&nbsp;-'+$('input[name=mv_order_nosepos]:checked').attr('data')+'</span>') : '')
	);
}

function changeSelection(){
	if(selectingLens == false){
		$('#currentLensSelection').slideUp(300,'linear',function(){
			selectingLens = true;
			$('#fullLensSelection').slideDown(700,'linear',function(){
				selectingLens = false;
			});
		});
	}
	return false;
}

function selectLensPackage(lens_pkg){
	errClear();
	$('#form_error_display').hide()
	if(selectingLens == false){
		$('#lenspackage').slideUp(700,'linear',function(){
		    var type = $('input[name=mv_order_lenstype]:checked').val();
            if(type === 'OPT10' || type === 'OPT4' || type == 'OPT11'){
                $('#ar_included').show();   
            }
			selectingLens = true;
			selectingLens = false;
			showCustomizeStep();
			pageload.ignorehashchange = true;
			location.hash = '#tintopt';
			setTimeout(function(){pageload.ignorehashchange = false;}, 100);
			var crumb =$('#hash_lenspkg').show().clone();
			crumb.attr('id', 'crumb_'+crumb.attr('id')).text('Lens Package');
			if(!$('#'+crumb.attr('id')).length){
				$('#stepCrumbs ul').append($('<li />').append(crumb).prepend('<span>&raquo;</span>'));
			}
		});
	}
	
	$('input[name=mv_order_lenstype]').each(function(){
		$(this).prop('checked',this.value == lens_pkg ? !0 : !1);
	});
	$('input[name=mv_order_lenstype]:checked').val(lens_pkg);
	$('#lp_display div.chooselenses_tctleft').html($('#lensLabel_' + lens_pkg).text() + ' - ' + $('#lensLabelLink_' + lens_pkg).text() )
	$('#lp_display').fadeIn('fast','linear');
	$('#changeLens').show();
	return false;
}

function showCustomizeStep(){
	var safety_frame = $('#safety_frame_val').val();
	if( !firstSelected ){
		if(allowed_tints_count > 0){
			if (showingOptions && shownCaseSelect){
				$('#lensoptions').slideDown(1200,'linear',function(){
					previewTints();
				});
			}
			if(!showingOptions){
				$('#lensoptions').slideDown(1200,'linear',function(){
					previewTints();
				});
				showingOptions = true;
			}
			if(!shownCaseSelect){
				shownCaseSelect = true;
				setTimeout("showCaseSummaryStep()",700);
			}
		}
		else{
			if(safety_frame > 0){
				$('#lensoptions').hide();
				showingOptions = false;
			}
			else if(!showingOptions){
				$('#lensoptions').slideDown(1200,'linear',function(){
					previewTints();
				});
				showingOptions = true;
				if(!shownCaseSelect){
					shownCaseSelect = true;
					setTimeout("showCaseSummaryStep()",700);
				}
			}
			if(!shownCaseSelect){
				shownCaseSelect = true;
				showCaseSummaryStep();
			}
		}
	}
}

function showCaseSummaryStep(){
	//$('#caseSummaryContainer').slideDown(700,'linear');
}

function selectCase(selectCase){
	toggleCaseSelection();
	setTimeout("selectCaseChange('"+selectCase+"');",400);
}

function selectCaseChange(selectCase){
	var currentCaseName = document.getElementById('currentCaseName');
	var currentCasePrice = document.getElementById('currentCasePrice');
	var selectedCaseName = document.getElementById('caseName_'+selectCase);
	var currentCaseImage = document.getElementById('currentCaseImage');
	currentCaseName.innerHTML = selectedCaseName.innerHTML;
	framePrice = cost['mv_order_case'][selectCase];
	if(framePrice == 0) {
		framePrice = 'Included FREE!';
	} else {
		framePrice = '$'+framePrice;
		framePrice += ' additional';
	}
	currentCasePrice.innerHTML = framePrice;
	currentCaseImage.src = caseImages[selectCase].src;

	$('#mv_order_case').val(selectCase);
	selectedCase = selectCase;
	calcPrice();
}

showCaseSelectionState = 0; //define case state as hidden
function toggleCaseSelection(){
	var caseLayer = document.getElementById('selectCase');
	var aleft=document.all? caseLayer.scrollLeft : caseLayer.pageXOffset
	var atop=document.all? caseLayer.scrollTop : caseLayer.pageYOffset

	caseChoose = document.getElementById('chooseCaseContainer');
	caseLayer = document.getElementById('selectCase');

	var cleft = 0;
	var ctop = 0;

	while (caseChoose.offsetParent){
		cleft += caseChoose.offsetLeft;
		ctop += caseChoose.offsetTop;
		caseChoose = caseChoose.offsetParent;
	}

	cleft += caseChoose.offsetLeft;
	ctop += caseChoose.offsetTop;

	layertop = ctop - 140;
	layerleft = cleft - 236;

	if(layertop < 0)
		layertop = 10;

	caseLayer.style.top = layertop + "px";
	caseLayer.style.left = layerleft + "px";

	if(showCaseSelectionState){
		$('#selectCase').toggle(500);
		showCaseSelectionState = 0;
	}else{
		$('#selectCase').toggle(500);
		showCaseSelectionState = 1;
	}
}

showCasePreviewState = 0; //define case state as hidden
function previewCase(){
	var caseLayer = document.getElementById('casePreview');
	var aleft=document.all? caseLayer.scrollLeft : caseLayer.pageXOffset
	var atop=document.all? caseLayer.scrollTop : caseLayer.pageYOffset

	caseChoose = document.getElementById('chooseCaseContainer');
	caseLayer = document.getElementById('casePreview');

	var casePreviewImage = document.getElementById('casePreviewImage');
	casePreviewImage.src = caseLargeImages[selectedCase].src;

	var cleft = 0;
	var ctop = 0;

	while (caseChoose.offsetParent){
		cleft += caseChoose.offsetLeft;
		ctop += caseChoose.offsetTop;
		caseChoose = caseChoose.offsetParent;
	}

	cleft += caseChoose.offsetLeft;
	ctop += caseChoose.offsetTop;

	layertop = ctop - 220;
	layerleft = cleft - 200;

	if(layertop < 0){
		layertop = 10;
		layerleft = cleft - 600;
	}

	caseLayer.style.top = layertop + "px";
	caseLayer.style.left = layerleft + "px";

	if(showCasePreviewState){
		$('#casePreview').toggle(500);
		showCasePreviewState = 0;
	}
	else{
		$('#casePreview').toggle(500);
		showCasePreviewState = 1;
	}
}

//noseposition
function update_noseposition(){
	orderlens = document.orderform.mv_order_lens;
	shownosepos = false;
	if(orderlens){
		for(i=0;i < orderlens.length;i++){
			if(((orderlens[i].value=='BIF') || (orderlens[i].value=='PROG') || (orderlens[i].value=='PROGE')) && (orderlens[i].checked)){
				shownosepos = true;
			}
		}
	}

	if(shownosepos){
		if( $('#nosepositionbox').is(':hidden') ){
			selectingNosePos = true;
			showingNosePos = true;
			//clearHighlight();
			$('#nosepositionbox').slideDown(300,'linear',function(){ 
				selectingNosePos = false; 
			});
		}
		//document.getElementById('nosepositionbox').style.display='block';
		loggedIn();
	}else{
		if( !document.getElementById('nosepositionbox').style.display ){
			selectingNosePos = true;
			showingNosePos = false;
			$('#nosepositionbox').slideUp(300,'linear',function(){ 
				selectingNosePos = false; 
			});
		}
	}
}

function checkStep2Form() {
	$('.stepmain input, .stepmain select').removeClass('error');
	var the_form = document.orderform;
	//hide the errorblock
	errordisplay = document.getElementById('form_error_display');
	if(errordisplay) errordisplay.style.display='none';
	check_fields = new Array('mv_order_lens', 'mv_order_pupd', 'mv_order_odsph', 'mv_order_ossph', 'mv_order_odcyl', 'mv_order_oscyl', 'mv_order_odaxi', 'mv_order_osaxi');
	required_fields = new Array('mv_order_lens', 'mv_order_pupd', 'mv_order_odsph', 'mv_order_ossph');

	if(shownosepos){
		check_fields.push('mv_order_nosepos');
		check_fields.push('mv_order_osadd');
		check_fields.push('mv_order_odadd');

		required_fields.push('mv_order_nosepos');
		required_fields.push('mv_order_osadd');
		required_fields.push('mv_order_odadd');
	}

	field_on = new Array();

	// check for required fields
	for(c=0;c < check_fields.length;c++){
		var check_field = check_fields[c];
		var $form_el = $('#' + check_field).length ? $('#' + check_field) : $('[name=' + check_field + ']'); //eval('document.orderform.' + check_field);
		var checked = false;

		if( check_field == 'mv_order_pupd' ){
			if( $form_el.val() ){
				checked = true;
				continue;
			}
		}else if ($form_el[0]){
			var el_type = $form_el[0].tagName; 
			switch (el_type){
				case 'SELECT':
					if( $form_el.val() ){
						checked=true;
					}
					// these are null fields
					if( $form_el.val() == 'SPH' || $form_el.val() == 'D.S.' || $form_el.val() == '+0.00' ) {
						checked=false;
					}
					break;

				// check radio buttons
				case 'INPUT':
					if( $form_el.attr('type').toLowerCase() == 'radio' ){
						checked = $('input[name=' + $form_el.attr('name') + ']:checked').length > 0;
					}
					break;
			}
		}

		if(!checked) {
			if(inArray(check_field, required_fields)){
				errs.push('2_nc_' + check_field);
				input_errs.push(check_field)
			}

			field_on[check_field] = false;
		} else {
			field_on[check_field] = true;
		}
	}

	$(the_form).find('.has-tooltip').each(function(){
		//Loop through tooltip inputs, if the value is the tooltip then blank it out.
		if(this.value == $(this).attr('data-default') ){
			this.value = '';
		}
	});

	if($('#addPrism').is(':checked')){
		var anyVal = false;
		var errorBase = 'prism_missing_o';

		$('#prismEntry :input').each(function(){
			if(this.value){
				anyVal = true;
				return false;
			}
		});
		var prism = {
			od: $('#mv_order_prism_od').val() || false,
			os: $('#mv_order_prism_os').val() || false
		};
		if( anyVal && !( prism.od || prism.os ) ){
			for( var eye in prism ){
				if( !prism[eye] ){
					var which = eye.replace(/o([sd])/,"$1");
					var idBase = '#mv_order_' + eye + '_prism_';
					if( $(idBase + 'vertical_magnitude').val() && !$(idBase + 'vertical_direction').val() ){
						errs.push(errorBase + which + "_v_direction");
					}else if( $(idBase + 'vertical_direction').val() && !$(idBase + 'vertical_magnitude').val() ){
						errs.push(errorBase + which + '_v_magnitude');
					}else if( $(idBase + 'horizontal_magnitude').val() && !$(idBase + 'horizontal_direction').val() ){
						errs.push(errorBase + which + "_h_direction");
					}else if( $(idBase + 'horizontal_direction').val() && !$(idBase + 'horizontal_magnitude').val() ){
						errs.push(errorBase + which + '_h_magnitude');
					}
				}
			}
		}

		if(
			(String(prism.od).match(/BI/) && String(prism.os).match(/BO/)) ||
			(String(prism.od).match(/BO/) && String(prism.os).match(/BI/))
		){
			errs.push('prism_opposing_directions');
		}

		if( String(prism.od).match(/BU/) && String(prism.os).match(/BU/) ){
			errs.push('prism_bu_in_both');
		} else if( String(prism.od).match(/BD/) && String(prism.os).match(/BD/) ){
			errs.push('prism_bd_in_both');
		}
	}

	// if the cylinder is set for either eye, make sure the axis is also set
	if((!field_on['mv_order_odcyl'] && field_on['mv_order_odaxi']) || (field_on['mv_order_odcyl'] && !field_on['mv_order_odaxi'])) {
    	errs.push('2_od_ca');
    	input_errs.push('mv_order_odcyl');
    	input_errs.push('mv_order_odaxi');
	}

	if((!field_on['mv_order_oscyl'] && field_on['mv_order_osaxi']) || (field_on['mv_order_oscyl'] && !field_on['mv_order_osaxi'])) {
        errs.push('2_os_ca');
        input_errs.push('mv_order_oscyl');
    	input_errs.push('mv_order_osaxi');
	}

	pdvalue = $('#mv_order_pupd').val();
	odsph = $('input[name=mv_order_odsph]').val();
	ossph = $('input[name=mv_order_ossph]').val();
	odcyl = $('input[name=mv_order_odcyl]').val();
	oscyl = $('input[name=mv_order_oscyl]').val();

	var isBinocular;

	//If this is a two part pd
	if( String(pdvalue).match(/\//) ){
		var tmp = new Object;
		tmp["od"] = pdvalue.replace(/([\d.]+)\/.*/,"$1");
		tmp["os"] = pdvalue.replace(/.*?\/([\d.]+)/,"$1");
		pdvalue = tmp;
	}

	if($('input[name=mv_order_lens]:checked').length && $('input[name=mv_order_lens]:checked').val().match(/PROG|BIF/) && $('select[name=mv_order_odadd]').val() && 
			( Number( $('select[name=mv_order_odadd]').val() ) < .75 || Number( $('select[name=mv_order_osadd]').val() ) < .75 )
	){
		errs.push('minimum_add');
		input_errs.push('mv_order_odadd');
    	input_errs.push('mv_order_osadd');
	}

	if(!rxchecked){
		if(pdvalue){
			if(kids){

				if(
					(typeof pdvalue == 'object' && (pdvalue["od"] < 20 || pdvalue["os"] < 20) )
					|| pdvalue < 40.0
				)
				{
					errs.push('2_pd_toolow');
					input_errs.push('odpd')
					input_errs.push('ospd')
					rxerror = true;
				}
				if(
					(typeof pdvalue == 'object' && (pdvalue["od"] > 31.5 || pdvalue["os"] > 31.5) )
				|| pdvalue > 61.5)
				{
					errs.push('2_pd_toohigh');
					input_errs.push('odpd')
					input_errs.push('ospd')
					rxerror = true;
				}
			} else {

				if( typeof pdvalue == 'object' ){
					if( pdvalue.od >= 50.5 && pdvalue.os >= 50.5 ){
						isBinocular = true;

						if( pdvalue.od - pdvalue.os > 5 ){
							errs.push('pd_gap');
							input_errs.push('odpd')
							input_errs.push('ospd')
							rxerror = true;
						}
					}
				}else{
					isBinocular = true;
				}

				if( ( typeof pdvalue == 'object' && ( pdvalue.od < 27.5 || pdvalue.os < 27.5 ) ) || pdvalue < 53.5 ){
					errs.push('2_pd_toolow');
					input_errs.push('odpd')
					input_errs.push('ospd')
					rxerror = true;
				}
				// Check if the PD is too high
				if( ( typeof pdvalue == 'object' && ( !isBinocular && (pdvalue.od > 35.5 || pdvalue.os > 35.5) ) ) || pdvalue > 70.5 ){
					errs.push('2_pd_toohigh');
					input_errs.push('odpd')
					input_errs.push('ospd')
					rxerror = true;
				}
			}
		}

		if(odsph != 'PLANO' && ossph != 'PLANO'){
			if(((odsph && odsph < 0) && (ossph && ossph > 0)) || ((odsph && odsph > 0) && (ossph && ossph < 0))){
				errs.push('2_sph_opposite');
				input_errs.push('mv_order_odsph')
				input_errs.push('mv_order_ossph')
				rxerror = true;
			}
		}
		if(((odcyl && odcyl < 0) && (oscyl && oscyl > 0)) || ((odcyl && odcyl > 0) && (oscyl && oscyl < 0))){
			errs.push('2_cyl_opposite');
			input_errs.push('mv_order_oscyl');
			input_errs.push('mv_order_odcyl');
			rxerror = true;
		}
	}

	if(errCheck(locale)) {
	    //google analytics
        try{
            if(_gaq){
                _gaq.push(["_trackPageview", "/virt/step-3"]);
            }
	    }catch(err){}
		$.ajax({
			url: '/step3_ajax.html',
			data: $('#orderform').serialize()+'&r='+Math.random(),
			dataType: 'html',
			beforeSend: function(){
			},
			success: function(data){
				$('input[name=mv_nextpage],input[name=mv_form_profile],input[name=mv_orderpage]').remove();
				$('#prescriptionbox').slideUp('slow', 'linear', function(){
					$('#stepData').append(data);
					$.each(['od','os'],function(i,v){
						$.each(['sph','cyl','axi','add'],function(i2,v2){
							$('#rx_display tr.'+v+' td.'+v2).html(
								$('#prescription_data select[name=mv_order_'+v+v2+']').val()
							);
						});
					});
					$('#rx_display tr.pd td.pd').html($('#prescription_data input[name=mv_order_pupd]').val())
					$('#rx_display').slideDown('fast','linear',function(){
						//when its hidden, widths will be zero and if done after
						//the slide down you see it happen so I show it set it and hide it.
						$('#lenspackage').show();
						var width = 0;
						$('#fullLensSelection span.left').each(function(){
							var w = $(this).width();
							width = w > width ? w : width;
						});
						$('#lenspackage').hide();
						$('#fullLensSelection span.left').css('width', (width + 210)+'px');
						$('#lenspackage').slideDown('slow','linear',function(){
							var crumb = $('#hash_prescription').show().clone();
							crumb.attr('id', 'crumb_'+crumb.attr('id')).text('Prescription');
							if (!$('#'+crumb.attr('id')).length){
								$('#stepCrumbs ul').append($('<li />').append(crumb).prepend('<span>&raquo;</span>'));
							}
							if ($('#noPackages').val()){
								$('#nextStep').hide();
							}else{
								$('#nextStep').show();
							}
							$('#moneyBox').slideDown('fast','linear');
						});
						$('#fullLensSelection span.left.unclickable').css('width', '575px');
						$('input.unclickable').attr('disabled','disabled');
					});
					var default_comments = $('#rxcomments').val().match(/you have any questions or comments/);
					if($('#rxcomments').val() && !default_comments){
						var val = $('#rxcomments').val();
						var orig_val = val;
						if(val.length > 120){
							val = val.substr(0,120);
							val += '&hellip;&nbsp;<a href="#" class="rxCommentPop">more</a>';
						}
						$('#rxcomment_block p ').html(val);
						$('#fullComment p').html(orig_val);
						$('#rxcomment_block').show();
						bindRxPop();
					}else{
						if (default_comments){
							$('#rxcomments').val('')
						}
					}
					$('#lensoptions').hide();
					$('div.step_cont ul li').removeClass('current');
					$('div.step_cont ul li.last').addClass('current');
					$('#nextStep').unbind().click(function(e){
						e.preventDefault();
						checkStep3Form();
						return !1;
					});
					bindLensHovers();
				});
				pageload.ignorehashchange = true;
				location.hash = '#lenspkg';
				setTimeout(function(){pageload.ignorehashchange = false;}, 100);
			}
		});
		//document.orderform.submit();
	} else{
		rxchecked = rxerror ? !0 : !1;
		$('html,body').animate({
			scrollTop: ($('#form_error_display').offset().top - 10)
		},'fast');
	}
}
function bindLensHovers(){
	$('span.left').hover(function(){
		$(this).addClass('highlight').next('span.right').addClass('highlight');
	},function(){
		$(this).removeClass('highlight').next('span.right').removeClass('highlight');
	});
}
function bindRxPop(){
	$('a.rxCommentPop').unbind().click(function(e){
		e.preventDefault();
		var template = $('div.popupTemplate.original')
		 .clone()
		  .removeClass('original');
		template.find('div.mypd_cont').empty().html($('#fullComment').html())
		$('#modal_overlay').show();
		$('#popup').empty().append(template).css({
			position: 'fixed',
			top: $(window).height() + 50,
			'z-index': '999999',
			width: '300px',
			display: 'block'
		}).draggable().click(function(e){
			e.stopImmediatePropagation();
		});
		$('#popup div.popupTemplate').show();
		$('#popup a.modalCloseImg').show();
		pop_top = (($(window).height() - ($('#popup').height()))/2)+'px';
		pop_left = (($(window).width() - ($('#popup').width()))/2)+'px';
		$('#popup').css({
			left: pop_left
		}).animate({
			top: pop_top
		},'fast','linear');
		$('#modal_overlay, a.modalCloseImg').unbind().click(function(){
			$('#modal_overlay , #popup').fadeOut('fast','linear');
			return false;
		});
		return !1;
	})
}
function checkStep3Form() {
  if (!$('input[name=mv_order_lenstype]:checked').length){
    $('#form_error_display').show().html('Please select a lens package.');
    $('html,body').animate({
			scrollTop: ($('#form_error_display').offset().top - 10)
		},'fast');
    return false;
  }
	$('#form_error_display').hide();
	if (!no_tint && allow_ar && !$('#slide_ar_div:visible').length){
		$('#tintPreviewPanel').slideUp('slow',function(){
			var tint = $('input[name=mv_order_tint]').val();
			tint = tint ? tint.toLowerCase() : tint;
			tint = tint.charAt(0).toUpperCase() + tint.substr(1);
			$('#lt_display').children('.chooselenses_tctleft').text(tint).end().show();
			var crumb =$('#hash_lenstint').show().clone();
			crumb.attr('id', 'crumb_'+crumb.attr('id')).text('Lens Tint');
			if(!$('#'+crumb.attr('id')).length){
				$('#stepCrumbs ul').append($('<li />').append(crumb).prepend('<span>&raquo;</span>'));
			}
			if(include_ar || ($('input[name=mv_order_lenstype]:checked').val() && $('input[name=mv_order_lenstype]:checked').val().match(/^OPT(4|10)$/))){
			    $('#ar_included').show();
			}
			$('#slide_ar_div').slideDown('fast');
		});
		selected_tint = !0;		
		return !1;
	}
	else if (!no_tint && !$('#mv_order_tint').val()) {
		$('#form_error_display').html('Please select a tint color.');
		return!1;
	}
	if ($('#ar_included:visible').length){
		$('#orderform input[name=mv_order_coating]:checked').val('COA4');
	}
	if($('#mv_order_prescription_label').val() && $('#mv_order_prescription_label').val().match(/^e\.g\./)){
		$('#mv_order_prescription_label').val('');
	}
	check_fields = new Array('mv_order_lenstype');
	required_fields = new Array('mv_order_lenstype');

	field_on = new Array();

	if( String( $('input[name=mv_order_lenstype]:checked').val() ).match(/^SUN1|SUN3|SUN9|SUN10|BIF_SUN11|BIF_SUN12|PROG_SUN17|PROGE_SUN18|BIF_SUN15|PROG_SUN19|PROG_SUN20|PROGE_SUN23|PROG_SUN24|PROG_SUN25|PROGE_SUN26|PROGE_SUN27$/) ){
		$('input[name=mv_order_coating]').each(function(){ var $this = $(this); $this.attr('checked', ($this.val() == 'COA3' ? 'checked' : '') ); });
	}

	// check for required fields
	for(c=0; c < check_fields.length; c++){
		var check_field = check_fields[c];
		var form_el = eval('document.orderform.' + check_field);

		var el_type = eval('document.orderform.' + check_field +'.type');
		if(!el_type) {
			el_type = eval('document.orderform.' + check_field +'[0].type');
		}

		var checked = false;

		switch (el_type){
			case 'select-one':
				var the_field = eval('document.orderform.' + check_field + ' .options[document.orderform.' + check_field + '.selectedIndex]');
				if(the_field.text) {
					checked=true;
				}
                // these are null fields
                if(the_field.value == 'SPH' || the_field.value == 'D.S.') {
                    checked=false;
                }
				break;

			// check radio buttons
			case 'radio':
				var j = form_el.length;
				for(var i=0; i< j; i++) {
					if(form_el[i].checked) {
						checked = true;
						ga_pckg_cat = form_el[i].value.toString();
						break;
					}
				}
		}

		if(!checked) {
			//if(inArray(check_field, check_fields)){
			if(inArray(check_field, required_fields)){
				errs.push('3_nc_' + check_field);
			}

			field_on[check_field] = false;
		} else {
			field_on[check_field] = true;
		}
	}
	if($('#rxcomments').val().match(/any questions or comments about your prescription/)){
		$('#rxcomments').val(undefined);
	}
	if(errCheck(locale)){
	    //google analytics
        try{
            if(_gaq){
                _gaq.push(['_trackEvent', 'Frame Rx', ga_pckg_cat.toString(), ga_package_name.toString(), parseInt(ga_price)]);
                _gaq.push(['_trackPageview', '/virt/step-4']);
            }
        }catch(err){}
		document.orderform.submit();
	}
}


function Browser() {
  var ua, s, i;

  this.isIE    = false;
  this.isNS    = false;
  this.version = null;

  ua = navigator.userAgent;

  s = "MSIE";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isIE = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  s = "Netscape6/";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  // Treat any other "Gecko" browser as NS 6.1.

  s = "Gecko";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = 6.1;
    return;
  }
}

var browser = new Browser();
 
var rxHelpQuestion;

function toggleRxQuestion(questionNumber){
	//hide any previous element if it is not the same as the current
	if(rxHelpQuestion){
		if(rxHelpQuestion.id != 'rxQuestion'+questionNumber){
			rxHelpQuestion.style.display='none';
		}
	}

	//set the object to the newly select question
	rxHelpQuestion = document.getElementById('rxQuestion'+questionNumber);
	if(rxHelpQuestion.style.display == 'block'){
		rxHelpQuestion.style.display='none';
	} else {
		rxHelpQuestion.style.display='block';
	}
}

var tipArray = {
	od: {
		title: 'O.D.',
		detail: 'Right eye'
	},
	os: {
		title: 'O.S.',
		detail: 'Left eye'
	},
	sph: {
		title: 'SPH (sphere)',
		detail: 'The "overall" power.  Can be minus-power or plus-power.'
	},
	cyl: {
		title: 'CYL (cylinder)',
		detail: 'The amount of astigmatism.  Can be minus-power or plus-power.<br /><br />If you have no astigmatism, sometimes doctors will write a place-holder here like "SPH" (sphere) or "D.S." (diopters sphere) to signify that there is only a sphere power needed.',
		width:300
	},
	axis: {
		title: 'AXIS',
		detail: 'Usually abbreviated with a letter "X" in front of it, this tells us the direction of the astigmatism in degrees.<br /><br />If you have no astigmatism, there will be nothing written on your prescription.  Leave it blank on our form if this is the case.',
		width:300
	},
	prism: {
		title: 'PRISM',
		detail: 'A component a doctor may add to a prescription for people with double-vision or certain special types of eyestrain.  If you see something here or a number followed by a triangle (?), type it into your order comments.  If you are still having trouble call us and we can take this order over the phone. Putting prism in your eyeglasses is an additional $19.95.',
		width:300
	},
	add: {
		title: 'ADD',
		detail: 'How much power gets added to the distance Rx for your single-vision reading-only eyeglasses OR for the bottom half of your bifocals/progressives.',
		width:300
	},
	dist: {
		title: 'DIST',
		detail: 'The distance portion of your prescription.  This is for distance eyeglasses only (or for people who do not need bifocals or progressives yet).',
		width:300
	},
	near: {
		title: 'NEAR',
		detail: 'The near (reading-only) portion of your prescription.  If it is for bifocals or progressives, sometimes the ADD power is written here.  If you are not sure, type exactly what you see in the order comments box or call us for assistance.',
		width:300
	}
};


var currentObj;
var tipObj;
var tipContent;
var tipLeft;
var tipTop;

document.onmousemove = function(ev){
	mouseMove(ev);
	moveRxHelpTip();
}

//show the rx help tips
function showRxHelpTip(item){
	//set the content
	tipContent = '<b>' + tipArray[item].title + '</b><br />' + tipArray[item].detail;
	$('#tipLayer').html( tipContent );
	//Element.update('tipLayer',tipContent);

	tipWidth = 'auto';
	if(tipArray[item].width){
		tipWidth = tipArray[item].width + 'px';
	}

	tipLeft = mousePos.x + 30;
	tipTop = mousePos.y;

	$('#tipLayer').css({ left: tipLeft, top: tipTop, width: tipWidth }).show();
	//Element.setStyle('tipLayer',{display:'block',left:tipLeft + 'px',top:tipTop + 'px',width:tipWidth});
}

//hide the rx help tips
function hideRxHelpTip(){
	$('#tipLayer').hide();
}

function moveRxHelpTip(){
	if( jQuery('#tipLayer').is(':visible') ){
		tipLeft = mousePos.x + 30;
		tipTop = mousePos.y;
		jQuery('#tipLayer').css({ left: tipLeft, top: tipTop });
	}
}
/****** START - CAPTURE MOUSE MOVEMENT AND DRAG ********/

function mouseMove(ev){
	ev = ev || window.event;
	mousePos = mouseCoords(ev);
}

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.documentElement.scrollLeft + document.body.scrollLeft,
		y:ev.clientY + document.documentElement.scrollTop + document.body.scrollTop
	};
}

$(function(){
	$('html').css('overflow-y','scroll');
	$(window).hashchange(pageload.loadUrl);
	$('a.changeSelection').live('click',function(e){
		e.preventDefault();
		var $this = $(this);
		$('div.stepContainer:visible').slideUp('slow','linear',function(){
			if($this.attr('data') === 'lensoptions'){
				$('#slide_ar_div').hide();
				$('#tintPreviewPanel').show();
			}
			$('#'+$this.attr('data')).slideDown('slow','linear',function(){
				try{
					eval($this.attr('data-func')+'()');
					$('#nextStep').show();
				}catch (ex){}
			});
		});
		pageload.ignorehashchange = true;
		location.hash = '#'+$(this).attr('id').replace(/^(crumb_)?hash_/,'');
		setTimeout(function(){pageload.ignorehashchange = false;}, 100);
	});
	$('#summaryToggle').click(function(e){
		e.preventDefault();
		if ($('#summaryDisplay').is(':visible')){
			$('#summaryDisplay').stop(true,true).slideUp(400,'linear',function(){
				$('#summaryToggle').html('View Summary')
				$('#summaryTab').removeClass('expanded')
			});
		}
		else {
			$('#summaryTab').addClass('expanded')
			$('#summaryDisplay').stop(true,true).slideDown(400,'linear',function(){
				$('#summaryToggle').html('Hide Summary')
			});
		}
		return !1;
	});
	$('a.enlarge_link').click(function(e){
		e.preventDefault();
		$('#popup').empty().html($('.enlargeTemplate').clone());
		$("#popup img.enlarge").attr('src', $('#largeImg').attr('src'));
		$('#popup div.enlargeTemplate').show();
		var pop_left = (($(window).width() - $('#popup').width() )/2)+'px';
		$('#popup').css({
			position: 'fixed',
			top: ($(window).height()+20)+'px',
			left: pop_left,
			height: '300px',
			width: '800px',
			'z-index': '999999',
			display: 'block'
		});
		var pop_left = (($(window).width() - $('#popup').width() )/2)+'px';
		$('#popup').css({
			left: pop_left
		}).draggable().click(function(e){
			e.stopImmediatePropagation();
		});
		var pop_top = (($(window).height() - $('#popup').height() )/2)+'px';
		$('#popup').animate({
			top: pop_top
		},'fast','linear');
		$('#modal_overlay').removeAttr('style').show();
		$('a.modalCloseImg,#modal_overlay').unbind().click(function(e){
			e.preventDefault();
			$('#popup').fadeOut('slow');
			$('#modal_overlay').fadeOut('fast');
		});
		return !1;
	});
});

function clearLens(){
	$('#step3').remove();
	selectingLensType = false; 
	showingPrescription = false;
	selected_tint = !0;
	$('#nextStep').unbind().click(function(e){
		e.preventDefault();
		checkStep2Form();
		return !1;
	});
	errClear();
	$('input[name=mv_order_lens]').prop('checked',!1);
	$('#lensoptions,#lenspackage').remove();
	$('div.step3 input[type=hidden]').remove();
	$('<input name="mv_nextpage" type="hidden" />').val('step3_ajax').appendTo('#orderform');
	$('<input name="mv_form_profile" type="hidden" />').val('check_step2').appendTo('#orderform');
	$('#moneyBox').hide();
	ar_included = false;
}
function clearTint(){
	$('#nextStep').unbind().click(function(e){
		e.preventDefault();
		checkStep3Form();
		return !1;
	});
	selected_tint = !0;
	$('input[name=mv_order_tint]').val('')
	ar_included = false;
}
function clearScript(){
	$('#nextStep').unbind().click(function(e){
		e.preventDefault();
		checkStep2Form();
		return !1;
	});
	selected_tint = !0;
	$('#step3').remove();
	errClear();
	$('div.step3 input[type=hidden]').remove();
	$('<input name="mv_nextpage" type="hidden" />').val('step3_ajax').appendTo('#orderform');
	$('<input name="mv_form_profile" type="hidden" />').val('check_step2').appendTo('#orderform');
	$('#moneyBox').hide();
	ar_included = false;
}

function clearPkg(){
	$('#nextStep').unbind().click(function(e){
		e.preventDefault();
		checkStep3Form();
		return !1;
	});
	selected_tint = !0;
	$('input[name=mv_order_lenstype]').removeAttr('checked')
	$('#tintPreviewPanel').show();
	$('#slide_ar_div').hide();
	firstSelected = false;
	showingOptions = false;
	ar_included = false;
}