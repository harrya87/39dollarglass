function saveThis(form, valu, log){
	form.save_cart.value = valu;
	if (log==0)
	{
		form.mv_nextpage.value = "login";
		form.saving_cart.value = "1";
		form.mv_doit.value = "return";
	}
	form.submit();
}
function goCheckout(form, valu, todo, login){
	form.mv_nextpage.value = valu;
	form.mv_doit.value = todo;
	form.no_login.value = login;
	form.submit();
}
function refreshPage(){
	document.location.reload();
}
var hasBeenWarned = false;
jQuery(function($){
	$('input.recal').click(function(){
		var $this = $(this);
		if( !hasBeenWarned && $this.siblings('input[name^=quantity]').first().val() == 0 ){
			$('#quantity-zero-warning').css({
				'margin-top': -(jQuery('#quantity-zero-warning').height() + 80) / 2,
				'margin-left': -(jQuery('#quantity-zero-warning').width() + 80) / 2
			});
			$('#white_overlay').show().css({
				'height': $(document).height()
			});
			$('#quantity-zero-warning').show();
			hasBeenWarned = true;
			return false;
		}
		return true;
	});
	$('#yes-continue').click(function(){ $('#basket').submit(); return false; });
	$('#no-stop, #white_overlay').click(function(){ $('#quantity-zero-warning').hide(); $('#white_overlay').hide(); });
	jQuery('.upload_picslide').jcarousel();
	$('.frame_tab').easytabs();
	$('.upload_pics').easytabs();
	$('.product_tab').easytabs({
		animationSpeed: 225
	});
});