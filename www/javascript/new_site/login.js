function ajaxLogon(form_id, preventReload,success_callback) {
    var username = $("input[name='mv_username']").val();
    username.match(/^(?:[^0-9a-zA-Z]+|)(.*?)(?:[^0-9a-zA-Z]+|)$/gi);
    username = RegExp.$1;
    $("input[name='mv_username']").val(username);
    $('.loginPop input').removeClass('errBorder')
    $('.loginPop p.error').hide();
    if (document.location.protocol == 'http:') {
        var iframe = $('<iframe>'),
            postForm = $('form#'+form_id).clone(true),
            frameName = ('resp' + Math.random()).replace('.', '');
        iframe.css('display', 'none')
            .attr('name', frameName)
            .attr('id', 'logoniframe')
            .appendTo($('.loginPop'));
        iframe[0].contentWindow.name = frameName;
        postForm.unbind()
            .css('display', 'none')
            .attr('target', frameName)
            .appendTo($('body'));
        postForm.submit();
        $('#logoniframe').one('load', function () {
            $('#loginForm').data('submitting','');
            var result = $(iframe).contents().find('html body').html();
            if (result.match(/ajax_error/)) {
                $('.loginPop p.error').show();
                $('.loginPop input[type=text], .loginPop input[type=password]').addClass('errBorder');
                iframe.remove();
                postForm.remove();
            } else if (result){
                if (typeof preventReload == 'undefined') {
                    var string = window.location.href;
                    if (string.match(/process/)){
                        window.location.href = '//'+document.location.hostname + (document.location.port != '80' ? (':' + document.location.port) : '/');
                    }else{
                        window.location.href = string.replace(/#$/, '');
                    }
                }else {
                    try { success_callback(); }catch(ex){}
                }
            }
        });
    } else {
        $.ajax({
            url: 'https://' + document.location.hostname + ':' + document.location.port + '/login_ajax.html',
            dataType: 'json',
            type: 'POST',
            data: $('form#'+form_id).serialize()+'&secure_protocol=1',
            success: function (data) {
                $('#loginForm').data('submitting','');
                if (data.error) {
                    $('.loginPop p.error').show();
                    $('.loginPop input[type=text], .loginPop input[type=password]').css('border','1px solid #C30000');
                } else {
                    if (typeof preventReload == 'undefined') {
                        var string = window.location.href;
                        if (string.match(/process/)){
                            window.location.href = '//'+document.location.hostname + (document.location.port != '80' ? (':' + document.location.port) : '/');
                        }else{
                            window.location.href = string.replace(/#$/, '');
                        }
                    }else {
                        try { success_callback(); }catch(ex){}
                    }
                }
            },
            error: function (error) {
                window.location.href = 'https://' + document.location.hostname + ':' + document.location.port + '/login.html';
            }
        });
    }
}

function ajaxLogout(preventReload) {
    $.ajax({
        url: document.location.protocol + '//' + document.location.hostname + ':' + document.location.port + '/login_ajax.html',
        dataType: 'json',
        data:{
            logout: 1
        },
        type: 'POST',
        success: function (data) {
            $('#loginForm').data('submitting','');
            if (typeof preventReload == 'undefined') {
                var string = window.location.href;
                if (string.match(/process/)){
                    window.location.href = '//'+document.location.hostname + (document.location.port != '80' ? (':' + document.location.port) : '/');
                }else{
                    window.location.href = string.replace(/#$/, '');
                }
            }
        },
        error: function (error) {
           window.location.href = 'https://' + document.location.hostname + ':' + document.location.port + '/logout';
        }
    });
    return false;
}