$(function(){
    docReady();
});
function bindCombo(){
	$('li.tintOption').unbind().hover(function(){
		$('div.mainImage img').attr('src',$(this).attr('data'));
	},function(){
		$('div.mainImage img').attr('src',$('div.mainImage img').attr('data-src'));
	}).click(function(e){
		e.preventDefault();
		$(this).addClass('selected')
		$('div.mainImage img').attr('src',$(this).attr('data'));
		$('div.mainImage img').attr('data-src',$(this).attr('data'));
	});
	$('a.modalCloseImg').unbind().click(function(e){
		e.preventDefault();
		$('#popup').fadeOut('slow');
		$('#modal_overlay').fadeOut('slow')
	});
	$('li.frameColor').unbind().click(function(e){
		e.preventDefault();
		$.ajax({
			url: '/color_combo.html',
			data: {
				sku: $(this).attr('data')
			},
			//beforeSend:function(){
			//	var width = $('#popup div.mypd_cont').width(), height = $('#popup div.mypd_cont').height();
			//	$('#popup div.mypd_cont').html('<div style="display: block; width: '+width+'px; height: '+height+'px"><center><img style="margin: auto; margin-top: 150px;width: 220px; height: 25px;" src="/store/images/new_site/292.gif" align="absmiddle" /></center></div>');
			//},
			dataType: 'html',
			success: function(data){
				document.getElementById('popup').innerHTML = data;
				var width = 0, counter = 0;
				$('ul.color_opt li').each(function(){
					width += $(this).width();
					++counter;
				});
				width += (20 * counter);
				$('ul.color_opt').css({
					width: width +'px',
					margin: 'auto'
				})
				$('ul.lens_tint li').each(function(){
					width += $(this).width();
					++counter;
				});
				width += (5 * counter);
				$('ul.lens_tint').css({
					width: (width > 823 ? '844' : width)+'px',
					margin: 'auto'
				})
				bindCombo();
			}
		});
	})
}

function checkFlypageForm() {
	$('#form_error_display').hide();
	if(!$('input[name=thiscolor]').is(':checked')){
		errs.push('1_nc_fc');
	}
	if(errCheck()) {
		$('#orderform').submit();
	}
}

function pdfpop() {
	var URL = "nopdf.html";
	var id = 46;
	eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=300,height=300');");
}

function docReady(){
	$('.upload_picslide').css({
		visibility: 'visible',
		height: 'auto',
		overflow: 'hidden'
	}).jcarousel({
    	wrap: 'circular'
    });
	$('.product_tab').easytabs({
		animationSpeed: 225
	});
	$('a.switchImg').click(function(e){
		e.preventDefault();
		$('#previewImg').attr('src', $(this).children('img').attr('data-src'));
		return !1;
	});
	$('.continue_step').click(function(e){
		$('#form_error_display').hide();
		e.preventDefault();
		if (!$('#orderform input[name=thiscolor]:checked').val()){
			$('#form_error_display').show();
		} else {
		    //google analytics
		    try{
                    if(_gaq){
                        var inputs = document.getElementsByName('thiscolor');
                        for (var i = 0; i < inputs.length; i++){
                            if (inputs[i].checked){
                                var regex = /\d+_(\w+)/;
                                var ga_color = regex.exec(inputs[i].value);
                            }
                        }
                        var product_name = ga_name;
                        _gaq.push(['_trackEvent', 'Frame Rx',product_name.toString(),ga_color[1].toString()]); 
                        _gaq.push(['_trackPageview', '/virt/step-2']);
                    }
                }
                catch (err){}
			$('#orderform').submit();
		}
		return !1;
	});
	$('#viewCombo').click(function(e){
		$.ajax({
			url: '/color_combo.html?sku='+prod_sku,
			dataType: 'html',
			type: 'GET',
			success: function(response){
				$('#modal_overlay').removeAttr('style').show();
				document.getElementById('popup').innerHTML = response;
				$('#popup').prepend($('<div id="handle" />'));
				$('#popup').removeAttr('style').css({
					position: 'fixed',
					top: ($(window).height()+20)+'px',
					left:'20px',
					'z-index': '999999',
					display: 'block'
				});
				if (528 > $(window).height()){
					$('#popup').css({
						height: (($(window).height() - 50 )+'px'),
						'overflow-y': 'scroll'
					})
				}
				var pop_left = (($(window).width() - $('#popup').width() )/2)+'px';
				$('#popup').css({
					left: pop_left
				}).draggable({ handle: '#handle' }).click(function(e){
					e.stopImmediatePropagation();
				});
				var pop_top = (($(window).height() - $('#popup').height() )/2)+'px';
				$('#popup').animate({
					top: pop_top
				},'fast','linear');
				$('#popup a.modalCloseImg,#modal_overlay').click(function(e){
					e.preventDefault();
					$('#popup').html('').fadeOut('fast','linear');
					$('#modal_overlay').fadeOut('fast');
				});
				var width = 0, counter = 0;
				$('ul.color_opt li').each(function(){
					width += $(this).width();
					++counter;
				});
				width += (20 * counter);
				$('ul.color_opt').css({
					width: width +'px',
					margin: 'auto'
				});
				$('ul.lens_tint li').each(function(){
					width += $(this).width();
					++counter;
				});
				width += (5 * counter);
				$('ul.lens_tint').css({
					width: (width > 823 ? '844' : width)+'px',
					margin: 'auto'
				});
				bindCombo();
			}
		});
		return !1;
	});
	$('input[name=thiscolor]').click(function(e){
		var $this = this;
		$('#form_error_display').hide();
		$('.selectframe_colortext span.showntext').hide();
		$('.selectframe_colortext span.s'+$($this).attr('id')).show();
		$.ajax({
			url: '/alt_color.html',
			data: {
				sku: $this.value,
				tint: $('#active_sun').val() || undefined
			},
			dataType: 'json',
			success: function(images){
				$.each(['quarter','side','front'],function(i,v){
					if (images[v] && images[v]['original']){
						$('li.'+v+' img.frameThumb').show().attr('src',images[v]['thumb']);
						$('img.swap.large.'+v).attr('src',images[v]['original']);
					} else {
						$('li.'+v+' img.frameThumb').hide();
					}
				});
				if (images['pdf']['original']){
					$('img.swap.pdf').attr('src',images['pdf']['original']);
				}
			}
		});
	});
	$('ul.frame_tablink li').click(function(e){
		e.preventDefault();
		var $this = $(this);
		if (($this.attr('class').toString().trim()=='no_click')){
		    window.open('/pdfs/' + $this.attr('id').toString().trim() + '.pdf');
			return !1;
		}else if (($this.hasClass('active'))){
			return !1;
		}else{
			$('.frame_tablimg img:visible').fadeOut('fast',function(){
				$('.frame_tablimg img').removeClass('active').each(function(){
					if($(this).attr('data') == $this.attr('data')){
						$(this).addClass('active').fadeIn('fast');
						return !1;
					}
				});
				$('ul.frame_tablink li').removeClass('active');
				$this.addClass('active');
			});
		}
		return !1;
	});
	$('a.enlarge_link, div.frame_tablimg').click(function(e){
		e.preventDefault();
		$('#popup').empty().html($('.enlargeTemplate').clone());
		$("#popup img.enlarge").attr('src', $('.frame_tablimg img.active').attr('src'));
		$('#popup div.enlargeTemplate').show();
		var pop_left = (($(window).width() - $('#popup').width() )/2)+'px';
		$('#popup').removeAttr('style').css({
			position: 'fixed',
			top: ($(window).height()+20)+'px',
			left: pop_left,
			height: '300px',
			width: '800px',
			'z-index': '999999',
			display: 'block'
		});
		if (300 > $(window).height()){
			$('#popup').css({
				height: (($(window).height() - 50 )+'px'),
				'overflow-y': 'scroll'
			})
		}
		var pop_left = (($(window).width() - $('#popup').width() )/2)+'px';
		$('#popup').css({
			left: pop_left
		}).draggable({ handle: '#handle' }).click(function(e){
			e.stopImmediatePropagation();
		});
		var pop_top = (($(window).height() - $('#popup').height() )/2)+'px';
		$('#popup').animate({
			top: pop_top
		},'fast','linear');
		$('#modal_overlay').removeAttr('style').show();
		$('a.modalCloseImg,#modal_overlay').unbind().click(function(e){
			e.preventDefault();
			$('#popup').fadeOut('slow');
			$('#modal_overlay').fadeOut('fast');
		});
	});
	$('a.inches_link').click(function(e){
		e.preventDefault();
		if (!$(this).data('active') || $(this).data('active') == 'millimeters' ){
			$('span.mm').hide();
			$('span.in').show();
			$(this).html('Millimeters');
			$(this).data('active', 'inches');
		}else{
			$('span.in').hide();
			$('span.mm').show();
			$(this).html('Inches');
			$(this).data('active', 'millimeters');
		}
		return !1;
	});
	$('#triggerPop').click( function(e){
	      e.preventDefault();
	      $.get('/tryon_gather_profile.html', function(data){
	        modal(data, 470);
	        step_one();
	      });
	});
}

function changeLensTint(isSunGlasses,queryString)
{
    if(isSunGlasses=='1'){
        return document.location = "/sun-glasses" + queryString;
    } else {
        return;
    }
}