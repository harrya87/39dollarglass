$(function(){
	var search_param = $('#searchParam').val();
	search_param = search_param ? search_param : '1.0';
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		data: {
			v: search_param
		},
		url: '/search_terms.html',
		success: function(data){
			var $search = $('#search');
			$search.autocomplete({
				lookup: data,
				delimiter: /,/,
				minChars: 2,
				width: 400,
				dataHash: !0,
				autoSubmit: !0,
				left: $search.offset().left - $search.width() + 33,
				onSelect: function(){
					$('#searchForm').submit();
				}
			}).enable();
		}
	});
	$('#searchForm').submit(function(e){
		if (!$search.val() || $search.val() == 'Search '){
			e.preventDefault();
			return !1;
		}
		return !0;
	});
});