/*
	Called from sidenav_filters, must appear on any page that uses the left side filter properties
*/
function fadeOutItems(){
	jQuery('#items').css('position','relative').prepend( jQuery('<div id="white_overlay"></div>').css('position','absolute') ).find('#white_overlay').show()
}

jQuery(function($){
	$('span.filterDisplay a').click(function(){
		fadeOutItems();
		var checkbox = $(this).closest('span').find('input[type=checkbox]');
		var checked = checkbox.is(':checked') ? '' : 'checked';
		checkbox.attr('checked', checked );
	});

	$('form.sort select').change(function(){
		fadeOutItems();
		$(this).closest('form').submit();
	});

	$('input[id^=filter]').click(function(){
		fadeOutItems();
		window.location = $(this).attr('data-href');
	});
	$('a.categoryBar').click(function(){
		$(this).siblings('div.categoryContent').toggle('fast');
		$(this).find('div.minus, div.plus').toggle();
	});
	$('a.pageNumber').click(function(){
		$('input[name=page_num]').val($(this).attr("name"));
		$('.sort').submit();
	});
	$('div.item').mouseleave(function(){
		$(this).children('div.tryon, div.buttons').css('display', 'none');
	}).mouseenter(function(){
		$(this).children('div.tryon, div.buttons').css('display', 'block');
	});
	$('#showAll .cont label').click(function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$('input[name=show_all]').removeAttr('checked');
		$(this).prev('input').attr('checked','checked');
		$('#showAll').submit();
	});
	$('#showAll .cont input').change(function(e){
		e.stopImmediatePropagation();
		$('#showAll').submit();
	});
});
