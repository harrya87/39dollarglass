var shownosepos = false;
var rxchecked = false;
var rxerror = false;
var kids[if scratch cross_category =~ /kids/i]=1[/if];

//noseposition
function update_noseposition()
{
	orderlens = document.orderform.mv_order_lens;
	shownosepos = false;
	if(orderlens)
	{
		for(i=0;i < orderlens.length;i++)
		{
			if(((orderlens[i].value=='BIF') || (orderlens[i].value=='PROG') || (orderlens[i].value=='PROGE')) && (orderlens[i].checked))
			{
				shownosepos = true;
			}
		}
	}

	if(shownosepos)
	{
		if( jQuery('#nosepositionbox').is(':hidden') )
		{
			selectingNosePos = true;
			showingNosePos = true;
			clearHighlight();
			addHighlight( jQuery('#nosepositionbox').slideDown(300, function(){ selectingNosePos = false; } ) );
		}
		//document.getElementById('nosepositionbox').style.display='block';
		[if session logged_in]
		try{
			if (pres_nosepos[idx] == 'higher') {
				document.orderform.mv_order_nosepos[0].checked=1;
			} else if (pres_nosepos[idx] == 'top') {
				document.orderform.mv_order_nosepos[1].checked=1;
			} else if (pres_nosepos[idx] == 'lower') {
				document.orderform.mv_order_nosepos[2].checked=1;
			}  else {
				document.orderform.mv_order_nosepos[0].checked=false;
				document.orderform.mv_order_nosepos[1].checked=false;
				document.orderform.mv_order_nosepos[2].checked=false;
			}
		}catch(ex){ }
		[/if]
	}
	else
	{
		if( !document.getElementById('nosepositionbox').style.display )
		{
			selectingNosePos = true;
			showingNosePos = false;
			jQuery('#nosepositionbox').slideUp(300, function(){ selectingNosePos = false; } );
		}
	}
}

function checkStep2Form() {
	var the_form = document.orderform;
	//hide the errorblock
	errordisplay = document.getElementById('form_error_display');
	if(errordisplay) errordisplay.style.display='none';

	check_fields = new Array('mv_order_lens', 'mv_order_pupd', 'mv_order_odsph', 'mv_order_ossph', 'mv_order_odcyl', 'mv_order_oscyl', 'mv_order_odaxi', 'mv_order_osaxi');
	required_fields = new Array('mv_order_lens', 'mv_order_pupd', 'mv_order_odsph', 'mv_order_ossph');

	if(shownosepos)
	{
		check_fields.push('mv_order_nosepos');
		check_fields.push('mv_order_osadd');
		check_fields.push('mv_order_odadd');

		required_fields.push('mv_order_nosepos');
		required_fields.push('mv_order_osadd');
		required_fields.push('mv_order_odadd');
	}

	field_on = new Array();

	// check for required fields
	for(c=0;c < check_fields.length;c++)
	{
		var check_field = check_fields[c];
		var $form_el = jQuery('#' + check_field).length ? jQuery('#' + check_field) : jQuery('[name=' + check_field + ']'); //eval('document.orderform.' + check_field);
		var checked = false;

		if( check_field == 'mv_order_pupd' ){
			if( $form_el.val() ){
				checked = true;
				continue;
			}
		}else{
			var el_type = $form_el[0].tagName; //form_el.type;

			// check select boxes
			//alert('field: ' + check_field + ' type:' + el_type);

			switch (el_type){
				case 'SELECT':
					if( $form_el.val() ){
						checked=true;
					}
					// these are null fields
					if( $form_el.val() == 'SPH' || $form_el.val() == 'D.S.' || $form_el.val() == '+0.00' ) {
						checked=false;
					}
					break;

				// check radio buttons
				case 'INPUT':
					if( $form_el.attr('type').toLowerCase() == 'radio' )
						checked = jQuery('input[name=' + $form_el.attr('name') + ']:checked').length > 0;
			}
		}

		if(!checked) {
			//if(inArray(check_field, check_fields)){
			if(inArray(check_field, required_fields)){
				errs.push('2_nc_' + check_field);
			}

			field_on[check_field] = false;
		} else {
			field_on[check_field] = true;
		}
	}

	jQuery(the_form).find('.has-tooltip').each(function(){
		//Loop through tooltip inputs, if the value is the tooltip then blank it out.
		if( jQuery(this).val() == jQuery(this).attr('data-default') ) jQuery(this).val('');
	});

	if( jQuery('#addPrism').is(':checked') ){
		var anyVal = false;
		var errorBase = 'prism_missing_o';

		jQuery('#prismEntry :input').each(function(){
			if( jQuery(this).val() ){ anyVal = true; return false; }
		});
		var prism = { od: jQuery('#mv_order_prism_od').val() || false, os: jQuery('#mv_order_prism_os').val() || false };
		if( anyVal && !( prism.od || prism.os ) ){
			for( var eye in prism ){
				if( !prism[eye] ){
					var which = eye.replace(/o([sd])/,"$1");
					var idBase = '#mv_order_' + eye + '_prism_';
					if( jQuery(idBase + 'vertical_magnitude').val() && !jQuery(idBase + 'vertical_direction').val() ){
						errs.push(errorBase + which + "_v_direction");
					}else if( jQuery(idBase + 'vertical_direction').val() && !jQuery(idBase + 'vertical_magnitude').val() ){
						errs.push(errorBase + which + '_v_magnitude');
					}else if( jQuery(idBase + 'horizontal_magnitude').val() && !jQuery(idBase + 'horizontal_direction').val() ){
						errs.push(errorBase + which + "_h_direction");
					}else if( jQuery(idBase + 'horizontal_direction').val() && !jQuery(idBase + 'horizontal_magnitude').val() ){
						errs.push(errorBase + which + '_h_magnitude');
					}
				}
			}
		}

		if(
			(String(prism.od).match(/BI/) && String(prism.os).match(/BO/)) ||
			(String(prism.od).match(/BO/) && String(prism.os).match(/BI/))
		){
			errs.push('prism_opposing_directions');
		}

		if( String(prism.od).match(/BU/) && String(prism.os).match(/BU/) )
			errs.push('prism_bu_in_both');

		else if( String(prism.od).match(/BD/) && String(prism.os).match(/BD/) )
			errs.push('prism_bd_in_both');
	}

	// if the cylinder is set for either eye, make sure the axis is also set
	if((!field_on['mv_order_odcyl'] && field_on['mv_order_odaxi']) || (field_on['mv_order_odcyl'] && !field_on['mv_order_odaxi'])) {
    	errs.push('2_od_ca');
	}

	if((!field_on['mv_order_oscyl'] && field_on['mv_order_osaxi']) || (field_on['mv_order_oscyl'] && !field_on['mv_order_osaxi'])) {
        errs.push('2_os_ca');
	}

	pdvalue = jQuery('#mv_order_pupd').val();
	odsph = jQuery('input[name=mv_order_odsph]').val();
	ossph = jQuery('input[name=mv_order_ossph]').val();
	odcyl = jQuery('input[name=mv_order_odcyl]').val();
	oscyl = jQuery('input[name=mv_order_oscyl]').val();

	var isBinocular;

	//If this is a two part pd
	if( String(pdvalue).match(/\//) ){
		var tmp = new Object;
		tmp["od"] = pdvalue.replace(/([\d.]+)\/.*/,"$1");
		tmp["os"] = pdvalue.replace(/.*?\/([\d.]+)/,"$1");
		pdvalue = tmp;
	}

	if( jQuery('input[name=mv_order_lens]:checked').val().match(/PROG|BIF/) &&
			( Number( jQuery('select[name=mv_order_odadd]').val() ) < .75 || Number( jQuery('select[name=mv_order_osadd]').val() ) < .75 )
	){
		errs.push('minimum_add');
	}

	if(!rxchecked)
	{
		if(pdvalue)
		{
			if(kids){

				if(
					(typeof pdvalue == 'object' && (pdvalue["od"] < 20 || pdvalue["os"] < 20) )
					|| pdvalue < 40.0
				)
				{
					errs.push('2_pd_toolow');
					rxerror = true;
				}
				if(
					(typeof pdvalue == 'object' && (pdvalue["od"] > 31.5 || pdvalue["os"] > 31.5) )
				|| pdvalue > 61.5)
				{
					errs.push('2_pd_toohigh');
					rxerror = true;
				}
			}
			else
			{

				if( typeof pdvalue == 'object' ){
					[comment]
					// Check if both values are above the minimum threshold for a binocular pd
					[/comment]
					if( pdvalue.od >= 50.5 && pdvalue.os >= 50.5 ){
						isBinocular = true;

						if( pdvalue.od - pdvalue.os > 5 ){
							errs.push('pd_gap');
							rxerror = true;
						}
					}
				}else{
					isBinocular = true;
				}

				if( ( typeof pdvalue == 'object' && ( pdvalue.od < 27.5 || pdvalue.os < 27.5 ) ) || pdvalue < 53.5 ){
					errs.push('2_pd_toolow');
					rxerror = true;
				}
				// Check if the PD is too high
				if( ( typeof pdvalue == 'object' && ( !isBinocular && (pdvalue.od > 35.5 || pdvalue.os > 35.5) ) ) || pdvalue > 70.5 ){
					errs.push('2_pd_toohigh');
					rxerror = true;
				}
			}
		}

		if(odsph != 'PLANO' && ossph != 'PLANO')
		{
			if(((odsph && odsph < 0) && (ossph && ossph > 0)) || ((odsph && odsph > 0) && (ossph && ossph < 0)))
			{
				errs.push('2_sph_opposite');
				rxerror = true;
			}
		}
		if(((odcyl && odcyl < 0) && (oscyl && oscyl > 0)) || ((odcyl && odcyl > 0) && (oscyl && oscyl < 0)))
		{
			errs.push('2_cyl_opposite');
			rxerror = true;
		}
	}

	if(errCheck(locale))
	{
		document.orderform.submit();
	}
	else
	{
		if(rxerror)
		{
			rxchecked = true;
		}
		else
		{
			rxchecked = false;
		}
	}
}
