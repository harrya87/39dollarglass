function holdSubmit(){
    $.ajax({
		url: '/admin/ajax_controller.html',
		data: 'r=' + Math.random() + '&project=order-manager&task=add-hold-email&' + $(this).closest('form').serialize(),
		dataType: 'text',
		success: function(response){
			if( response.indexOf('1') == 0 ){
				$('#fade').click();
				$('#save-hold-email').hide();
				$('#delete-hold-email').show();
				return;
			}else{
				alert("[OL] Hold e-mail *NOT* added, something went awry. Please grab someone from the web group to review your screen.");
			}
		},
		error: function(obj,status,error){
			alert('Some sort of error:\n' + status + "\n----\n" + error);
		}
	});
}
function flagSubmission(){
  var flagForm = $('#popup #flagForm').serializeArray();
  var rf;
  $.each(['comments','flag_group','username','email','csr'],function(i,v){
      $.each(flagForm, function(i2,v2){
          if (flagForm['name'] == v && !flagForm['value']){
            alert('Please fill in all the required fields. Missing: '+V);
            rf = true;
            return false;
          }
      });
  });
  if (rf){
    return false;
  }
  $.ajax({
      url: '/admin/flag_customer.html',
      data: flagForm,
      dataType: 'json',
      beforeSend: function(){
        $('#popup #flagSubmit').val('Submitting...');
      },
      success: function(response){
        $('#popup #flagSubmit').val('Flag Customer');
        alert(response.message);
        if (!response.error){
          $('#popup #flagForm select[name=account_group]').val([]);
          $('#popup #flagForm textarea').val('');
          $('#popup a.modalCloseImg').click();
        }
      }
  });
}
$(function(){
	$('#select-reject-reason').click(function(){
		if( $('input[name=reject-reason]:checked').length ){
			$('button[value=amb_reject], #amb_reverse').trigger('click');
		}else{
			$(this).siblings('ul').css('border','1px dashed red');
		}
	});
	$('#amb-reviewing').click(function(e){
		var reason;
		while( !reason ){
			reason = prompt('Why are you placing this order under review? (Notes only)','');
		}
		$('#review-reason').val( reason );
		return true;
	});
	/* Handler for the 'Save on hold e-mail' button */
	$('#hold-submit').click(function(){
		holdSubmit();
	});

	/* Handler for the 'Delete current hold e-mail'*/
	$('#delete-hold-email').click(function(){
		$.ajax({
			url: '/admin/ajax_controller.html',
			data: 'r=' + Math.random() + '&project=order-manager&task=delete-hold-email&order_number=' + $('input[name=order]').val(),
			dataType: 'text',
			success: function(response){
				if( response.indexOf('1') == 0 ){
					$('#delete-hold-email').hide();
				}else{
					alert("[OL] Hold e-mail *NOT* deleted, something went awry. Please grab someone from the web group to review your screen.");
				}
			},
			error: function(obj,status,error){
				alert('Some sort of error:\n' + status + "\n----\n" + error);
			}
		});
	});

	$('#cancel-order').click(function(){
		return confirm('Please be sure to cancel this order in RONVision as well. A note will be made denoting the cancellation of this order.');
	});

	$('[data-default]').each( function(){ $(this).val( $(this).attr('data-default') || '' ); });

	$('a[class^="edit-"]').click(function(e){
		var $this = $(this).hide();
		$this.siblings('span[class^=value]').hide();
		if( $this.siblings('div[class^=edit]').attr('class').match('shipping') ){
			$this.siblings('div[class^=edit]').css('display','inline');
		}else{
			$this.siblings('div[class^=edit]').show();
		}
		e.preventDefault();
	});

	$('a[class^=cancel]').click(function(e){
		var $this = $(this);
		var $parent = $(this).closest('div[class^="edit-"]').hide();

		$parent.siblings('span[class^="value-"], a[class^="edit-"]').show();

		$this.closest('form').find('[data-default]').each(function(){
			var $ele = $(this);
			$ele.val( $ele.attr('data-default') );
		});

		e.preventDefault();
	});

	/*
		Setup handlers for orderline comments editing
	*/
	$('form.transaction-comments-form').submit(function(){
		var $this = $(this);

		//Disable the submit button temporarily
		$this.find('input[type=submit]').attr('disabled','disabled');

		//Re-enable the submit button after a 2.5 second delay
		window.setTimeout(function(){ $this.find('input[type=submit]').attr('disabled',''); }, 2500);

		$.ajax({
			url: '/admin/ajax_controller.html',
			data: 'r=' + Math.random() + '&project=order-manager&task=update-transaction-comments&' + $this.serialize(),
			dataType: 'json',
			success: function(response){
				if( response.success ){
					$this.parent().hide()
							 .siblings('span.value-transaction-comments')
							 .text(response.response)
							 .show()
							 .siblings('a.edit-link-transaction-comments').show();
				}else{
					alert("[OL] Comments *NOT* updated, something went awry. Please grab someone from the web group to review your screen.");
					$this.show();
				}
			},
			error: function(obj,status,error){
				alert('Some sort of error:\n' + status + "\n----\n" + error);
				$this.show();
			}
		});
		return false;
	});


	/*
		Setup handlers for shipping method editing
	*/
	$('#form-shipping_method').submit(function(){
		var $this = $(this);

		//Disable the submit button temporarily
		$this.find('input[type=submit]').attr('disabled','disabled');

		//Re-enable the submit button after a 2.5 second delay
		window.setTimeout(function(){ $this.find('input[type=submit]').attr('disabled',''); }, 2500);

		$.ajax({
			url: '/admin/ajax_controller.html',
			data: 'project=order-manager&task=update-shipping_method&' + $this.serialize(),
			dataType: 'json',
			success: function(response){
				if( response.success ){
					$this.parent().hide()
							 .siblings('span.value-shipping_method')
							 .text(response.response)
							 .show()
							 .siblings('a.edit-link-shipping_method').show();
				}else{
					alert("[SM] Shipping Method NOT updated, please contact the web team.");
					$this.show();
				}
			},
			error: function(obj,status,error){
				alert('Some sort of error:\n' + status + "\n----\n" + error);
				$this.show();
			}
		});
		return false;
	});

	/*
		Setup handlers for orderline comments editing
	*/
	$('form.orderline-comments-form').submit(function(){
		var $this = $(this);

		//Disable the submit button temporarily
		$this.find('input[type=submit]').attr('disabled','disabled');

		//Re-enable the submit button after a 2.5 second delay
		window.setTimeout(function(){ $this.find('input[type=submit]').attr('disabled',''); }, 2500);

		$.ajax({
			url: '/admin/ajax_controller.html',
			data: 'project=order-manager&task=update-orderline-comments&' + $this.serialize(),
			dataType: 'json',
			success: function(response){
				if( response.success ){
					$this.parent().hide()
							 .siblings('span.value-orderline-comments')
							 .text(response.response)
							 .show()
							 .siblings('a.edit-link-orderline-comments').show();
				}else{
					alert("[OL] Comments *NOT* updated, something went awry. Please grab someone from the web group to review your screen.");
					$this.show();
				}
			},
			error: function(obj,status,error){
				alert('Some sort of error:\n' + status + "\n----\n" + error);
				$this.show();
			}
		});
		return false;
	});


	/*
		Setup handlers for the nosepos editing
	*/
	$('form.nosepos-form').submit(function(){
		var $this = $(this);

		//Disable the submit button temporarily
		$this.find('input[type=submit]').attr('disabled','disabled');

		//Re-enable the submit button after a 2.5 second delay
		window.setTimeout(function(){ $this.find('input[type=submit]').attr('disabled',''); }, 2500);

		$.ajax({
			url: '/admin/ajax_controller.html',
			data: 'project=order-manager&task=update-nosepos&' + $this.serialize(),
			dataType: 'json',
			success: function(response){
				if( response.success ){
					$this.parent().hide()
							 .siblings('span.value-nosepos')
							 .text(response.response)
							 .show()
							 .siblings('a.edit-link-nosepos').show();
				}else{
					alert("Nose position *NOT* updated, something went awry. Please grab someone from the web group to review your screen.");
					$this.show();
				}
			},
			error: function(obj,status,error){
				alert('Some sort of error:\n' + status + "\n----\n" + error);
				$this.show();
			}
		});
		return false;
	});


	/*
		Setup the handlers for the prescription editing
	*/
	$('form.prescription-form').submit(function(){
		var $this = $(this);

		//Disable the submit button temporarily
		$this.find('input[type=submit]').attr('disabled','disabled');

		//Re-enable the submit button after a 2.5 second delay
		window.setTimeout(function(){ $this.find('input[type=submit]').attr('disabled',''); }, 2500);

		$.ajax({
			url: '/admin/ajax_controller.html',
			data: 'project=order-manager&task=update-prescription&' + $this.serialize(),
			dataType: 'json',
			success: function(response){
				if( response.success ){
					$this.parent().hide()
							 .siblings('span.value-prescription')
							 .text(response.response)
							 .show()
							 .siblings('a.edit-link-prescription').show();
				}else{
					alert("Prescription *NOT* updated, something went awry. Please grab someone from the web group to review your screen.");
					$this.show();
				}
			},
			error: function(obj,status,error){
				alert('Some sort of error:\n' + status + "\n----\n" + error);
				$this.show();
			}
		});
		return false;
	});

	/*
		Setup the PD editing handlers
	*/
	//Add handler for when the pd is modified (for display of error text)
	$('form.pd-form').find('select').change(function(){
		var $this = $(this);
		if( $this.val() > 37.5 && $this.val() < 42.5 ){
			$this.closest('form').siblings('span.pd-error').show();
		}else{
			$this.closest('form').siblings('span.pd-error').hide();
		}
	});

	//Add handler for pd form submit
	$('form.pd-form').submit(function(){
		var $this = $(this);

		if( !$this.find('select[name^="odpd-"]').val() ){
			alert('You must select at least one PD');
			return false;
		}

		$this.hide();

		//Disable the submit button temporarily
		$this.find('input[type=submit]').attr('disabled','disabled');

		//Re-enable the submit button after a 2.5 second delay
		window.setTimeout(function(){ $this.find('input[type=submit]').attr('disabled',''); }, 2500);

		$.ajax({
			url: '/admin/ajax_controller.html',
			data: 'project=order-manager&task=update-pd&' + $this.serialize(),
			dataType: 'json',
			success: function(response){
				if( response.success ){
					$this.closest('div.edit-pd').siblings('span.value-pd')
								.text(response.response).show()
								.siblings('a.edit-link-pd').show();
				}else{
					alert("PD *NOT* updated, something went awry. Please grab someone from the web group to review your screen.");
					$this.show();
				}
			},
			error: function(obj,status,error){
				alert('Some sort of error:\n' + status + "\n----\n" + error);
				$this.show();
			}
		});
		return false;
	});

	//Add handler for seg_height form submit
	$('form.seg_height-form').submit(function(){
		var $this = $(this);

		if( !$this.find('select[name^="seg_height-"]').val() ){
			alert('You must select a seg height');
			return false;
		}

		$this.closest('div[class^=edit]').hide();

		//Disable the submit button temporarily
		$this.find('input[type=submit]').attr('disabled','disabled');

		//Re-enable the submit button after a 2.5 second delay
		window.setTimeout(function(){ $this.find('input[type=submit]').attr('disabled',''); }, 2500);

		$.ajax({
			url: '/admin/ajax_controller.html',
			data: 'project=order-manager&task=seg_height&' + $this.serialize(),
			dataType: 'json',
			success: function(response){
				if( response.success ){
					$this.closest('div.edit-seg_height').siblings('span.value-seg_height')
								.text(response.response).show()
								.siblings('a.edit-link-seg_height').show();
				}else{
					alert("Seg height *NOT* updated, something went awry. Please grab someone from the web group to review your screen.");
					$this.show();
				}
			},
			error: function(obj,status,error){
				alert('Some sort of error:\n' + status + "\n----\n" + error);
				$this.show();
			}
		});
		return false;
	});

	$('#notes-quickview').click(function(e){
		if( $('#notes table').length == 0 ){
			$('#notes').load('/admin/ajax_controller.html?r=' + Math.random() + '&project=order-manager&task=list-notes&order_number=' + $('input[name=order]').val(),
				function(){
					$('#notes').show();
				}
			);
		}else{
			$('#notes').show();
		}
		$('#notes').css({
			'top': e.pageY + 10,
			'left': e.pageX + 10
		});
	}).mousemove(function(e){
		if( $('#notes').is(':visible') ){
			$('#notes').css({
				'top': e.pageY + 10,
				'left': e.pageX + 10
			});
		}
	}).mouseout(function(){ $('#notes').hide(); });
	
});
