var iebody=(document.compatMode && document.compatMode != "BackCompat")? document.documentElement : document.body

function getObjectPos(objectid) {
	//*** get the x/y position of the browse products link
	//get the current document scroll amount	
	var scrollx=document.all? iebody.scrollLeft : window.pageXOffset
	var scrolly=document.all? iebody.scrollTop : window.pageYOffset
	var objectx = 0;
	var objecty = 0;
	
	theObject = jQuery('#' + objectid);
	
	while (theObject.offsetParent){
		objectx += theObject.offsetLeft;
		objecty += theObject.offsetTop;
		theObject = theObject.offsetParent;
	}
	
	objectx += theObject.offsetLeft;
	objecty += theObject.offsetTop;
	
	return {x:objectx, y:objecty};
}

/*
START - TOP NAV HOVER MENUS
*/
//initiate vars
var menuInit = false;
var showingMenu = false;
var menu_timer;
var menuArrow = new Image();
menuArrow.src = '/store/images/reloaded/top_navarrow.gif';
var menuArrowHover = new Image();
menuArrowHover.src = '/store/images/reloaded/top_navarrow_h.jpg';

//menu initialise function
function initMenu()
{	
	jQuery('div.top_nav_menu_list > li').mouseover(function(){ highlight(this,'on'); }).mouseout(function(){ highlight(this,'off') });

/*
	//get the menu objects
	menuLists = $$('div.top_nav_menu_list');
	
	for(m=0;m<menuLists.length;m++)
	{		
		//inspect each menu object and get all li items for the current menu
		menuItems = $(menuLists[m]).getElementsBySelector('li');
		for(i=0;i<menuItems.length;i++)
		{
			//apply mouse overs to each menu object
			menuItems[i].onmouseover = function()
			{
				highlight(this, 'on');				
			}
			menuItems[i].onmouseout = function()
			{
				highlight(this, 'off');
			}
		}
	}
*/
	menuInit = true;
}

//show menu when hovered
function showMenu(selectedMenu)
{
	if(menuInit)
	{
		if(showingMenu)
		{
			killMenuTimer();
			hideMenusDo();		
		}
		showingMenu = selectedMenu;	
		//fiest get the x/y of the hovered menu
		
		menuPos = jQuery('#button_'+selectedMenu).offset();	
		menux = menuPos.left;
		menuy = menuPos.top + 30;

		jQuery('#menu_' + selectedMenu).css({ top: menuy + 'px', left: menux + 'px' }).show();
		jQuery('#button_' + selectedMenu).addClass('hover');
		jQuery('#arrow_' + selectedMenu).attr('src',menuArrowHover.src);

/*
		$('menu_'+selectedMenu).setStyle({top:menuy+'px',left:menux+'px'});
		$('menu_'+selectedMenu).show();
		$('button_'+selectedMenu).addClassName('hover');
		$('arrow_'+selectedMenu).src = menuArrowHover.src;
*/
	}
}

//start timer to hide menu
function hideMenus()
{
	menu_timer = setTimeout("hideMenusDo();",250);
}

//interupt hide timer - continues to show menu
function killMenuTimer() {
	if(menu_timer)
	{
		clearTimeout(menu_timer);
	}
}

//do the hiding of the currently open menu
function hideMenusDo()
{
	if(showingMenu)
	{	
		jQuery('#menu_' + showingMenu).hide();
		jQuery('#button_' + showingMenu).removeClass('hover');
		jQuery('#arrow_' + showingMenu).attr('src', menuArrow.src);
/*
		$('menu_'+showingMenu).hide();
		$('button_'+showingMenu).removeClassName('hover');
		$('arrow_'+showingMenu).src = menuArrow.src;
*/
		showingMenu = false;
	}
}

//highlight menu items
function highlight(el, status) {
	jQuery(el).css('background-color', (status == 'on' ? '#7E9EDA' : '#8EAADF') );
}


/*
END - TOP NAV HOVER MENUS
*/
