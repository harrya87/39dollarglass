Object.extend(Element, {
	setWidth: function(element,w) {
	   	element = $(element);
    	element.style.width = w +"px";
	},
	setHeight: function(element,h) {
   		element = $(element);
    	element.style.height = h +"px";
	}
});

function getPageSize()
{
	
	var xScroll, yScroll;
	
	if (window.innerHeight && window.scrollMaxY) {	
		xScroll = window.innerWidth + window.scrollMaxX;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	
	var windowWidth, windowHeight;

	if (self.innerHeight) {	// all except Explorer
		if(document.documentElement.clientWidth){
			windowWidth = document.documentElement.clientWidth; 
		} else {
			windowWidth = self.innerWidth;
		}
		windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}	
	
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight){
		pageHeight = windowHeight;
	} else { 
		pageHeight = yScroll;
	}

	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth){	
		pageWidth = xScroll;		
	} else {
		pageWidth = windowWidth;
	}

	arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight) 
	return arrayPageSize;
}

function inspection_failed(lab)
{
	$('shipping_inspection').hide();
	$('shipping_inspection_failed').show();
}

function check_message()
{
	if($('message_select').options[$('message_select').selectedIndex].value)
	{
	 return true;	
	}
	alert('Select a reason for Remake');
	return false;		
}

function unloadPage()
{
	alert("Note: Please be sure to upload your Dazzle postage log before the end of the day!");
	window.location = 'http://www.39dollarglasses.com/lab/shipping_login.html';
}

var multiship;

function multiship_message()
{
	var arrayPageSize = getPageSize();
	Element.setWidth('shipping_inspection_overlay',arrayPageSize[0]);
	Element.setHeight('shipping_inspection_overlay',arrayPageSize[1]);
	new Effect.Appear('shipping_inspection_overlay', { duration: 0.1, from: 0.0, to: 0.2 });
	$('shipping_multi_cont').show();
	multiship = {
		'yes': function() {
			document.shipform.submit();	
		},		
		'no': function() {
			$('shipping_inspection_overlay').hide();
			$('shipping_multi_cont').hide();	
		}		
	};		
}

var highlighted_now;
function highlight_order()
{	
	if(highlighted_now)
	{
		$(highlighted_now).setStyle({backgroundColor:'white'});
	}
	if($('order_search').value != "")
	{
		$('lab_data').select('td.yui-dt-col-order').each(function(s) {
			if(s.innerHTML == $('order_search').value)
			{
				highlighted_now = s.ancestors()[0];			
				$(highlighted_now).setStyle({backgroundColor:'yellow'});
			}	
		});
	}
	$('order_search').focus();
	//$('order_'+$('order_search').value).setStyle({backgroundColor:'yellow'});
	//highlighted_now = $('order_search').value;
	//$('order_search').value = '';
	//$('order_search').focus();
}