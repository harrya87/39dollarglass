jQuery(function ($) {
	$('#basic-modal .basic').click(function (e) {
		e.preventDefault();
		popup(); //end popup 
	});
});





function popup(callback, ele_id, src) {

    $("#basic-modal-content").modal({
        autoResize: false,
        persist:false,
        autoPosition: true,
        opacity: 50,

        
        onOpen: function (dialog) {
	      dialog.overlay.fadeIn('100', function () {
		      dialog.data.hide();
		      dialog.container.offset( { top: dialog.container.offset().top + jQuery(document).scrollTop() } );
		      dialog.container.fadeIn('100', function () {
			      dialog.data.slideDown('500');
		      });
	      });
        },

        onClose: function (dialog) {

        	if($('#tryonDisplayFrame').length){
        		window.frames.tryonDisplayFrame.location.href = '/tryon_display.html';
        	} 	

	      dialog.data.fadeOut('50', function () {
		      dialog.container.fadeOut('50', function () {
		  	      dialog.overlay.fadeOut('50', function () {
				      $.modal.close();
				      
			      });
		      });
	      });
        },

		  onShow: function (dialog) {
			  var modal = this;
/*
			  // if the user clicks "resizemodal800" button
			  $('.resizemodalbig', dialog.data[0]).click(function () {
              //need to set form variables, then submit form in this iframe. then
              document.getElementById( 'basic-modal-content').style.width = '800px'
              dialog.container.width(800);
              $(window).trigger('resize.simplemodal');
              
			  }); //end if resizemodal800 is clicked


			  // if the user clicks "resizemodal420" button
			  $('.resizemodalsmall', dialog.data[0]).click(function () {
              //need to set form variables, then submit form in this iframe. then
              document.getElementById( 'basic-modal-content').style.width = '420px'
              dialog.container.width(420);
              $(window).trigger('resize.simplemodal');
              
			  }); //end if resizemodal420 is clicked
*/

/*
			  // if the user clicks "savealbum" button
			  $('.resizemodal', dialog.data[0]).click(function () {
              //need to set form variables, then submit form in this iframe. then
              document.getElementById( 'basic-modal-content').style.width = '420px'
              dialog.container.width(430);
              $(window).trigger('resize.simplemodal');
              
			  }); //end if savealbum is clicked
*/

			  // if the user clicks "close" button
			  $('.closemodal', dialog.data[0]).click(function () {
			  		  $.modal.close();
			  });
			  
			  if (ele_id && src){
			    $('#'+ele_id).attr('src', src);
			  }
			  
			  //end if close is clicked
			  if(callback){
			    callback();
			  }
			  
		  } //end onShow 
    });	
}




function popupLg(callback) {

    $("#tryon-modal-lg-display").modal({
//        autoResize: true,
        persist:false,
        autoPosition: true,
        opacity: 50,
        onOpen: function (dialog) {
	      dialog.overlay.fadeIn('100', function () {
		      dialog.data.hide();
		      dialog.container.offset( { top: dialog.container.offset().top + jQuery(document).scrollTop() } );
		      dialog.container.fadeIn('100', function () {
			      dialog.data.slideDown('500');
		      });
	      });
        },

        onClose: function (dialog) {	

	      dialog.data.fadeOut('50', function () {
		      dialog.container.fadeOut('50', function () {
		  	      dialog.overlay.fadeOut('50', function () {
				      $.modal.close();
				      
			      });
		      });
	      });
        },

		  onShow: function (dialog) {
			  var modal = this;

			  // if the user clicks "close" button
			  $('.closemodal', dialog.data[0]).click(function () {
			  		  $.modal.close();
			  }); //end if close is clicked
			  
		  } //end onShow 
    });	
}






function popupWarning(callback) {

    $("#tryon-modal-warning").modal({
//        autoResize: true,
        persist:false,
        autoPosition: true,
        opacity: 50,
        onOpen: function (dialog) {
	      dialog.overlay.fadeIn('100', function () {
		      dialog.data.hide();
		      dialog.container.offset( { top: dialog.container.offset().top + jQuery(document).scrollTop() } );
		      dialog.container.fadeIn('100', function () {
			      dialog.data.slideDown('500');
		      });
	      });
        },

        onClose: function (dialog) {	

	      dialog.data.fadeOut('50', function () {
		      dialog.container.fadeOut('50', function () {
		  	      dialog.overlay.fadeOut('50', function () {
				      $.modal.close();
				      
			      });
		      });
	      });
        },

		  onShow: function (dialog) {
			  var modal = this;

			  // if the user clicks "close" button
			  $('.closemodal', dialog.data[0]).click(function () {
			  		  $.modal.close();
			  }); //end if close is clicked
			  
		  } //end onShow 
    });	
}





function popupShare(callback) {

    $("#tryon-modal-share").modal({
//        autoResize: true,
        persist:false,
        autoPosition: true,
        opacity: 50,
        onOpen: function (dialog) {
	      dialog.overlay.fadeIn('100', function () {
		      dialog.data.hide();
		      dialog.container.offset( { top: dialog.container.offset().top + jQuery(document).scrollTop() } );
		      dialog.container.fadeIn('100', function () {
			      dialog.data.slideDown('500');
		      });
	      });
        },

        onClose: function (dialog) {	

	      dialog.data.fadeOut('50', function () {
		      dialog.container.fadeOut('50', function () {
		  	      dialog.overlay.fadeOut('50', function () {
				      $.modal.close();
				      
			      });
		      });
	      });
        },

		  onShow: function (dialog) {
			  var modal = this;

			  // if the user clicks "close" button
			  $('.closemodal', dialog.data[0]).click(function () {
			  		  $.modal.close();
			  }); //end if close is clicked

			  if(callback){
			    callback();
			  }
			  
		  } //end onShow 
    });	
}






/*


var c = function closer() {
    $.modal.close();
    alert("Done!"); 
}

function popupResize(newHeight, newWidth) {

                $("#simplemodal-container").css({height: newHeight, width: newWidth});


//  $("#basic-modal-content").modal({

//    onShow: function (dialog) {
//        var modal = this; // you now have access to the SimpleModal object

        // do stuff here
//        $("#simplemodal-container").css({height: newHeight, width: newWidth});

        // re-position
//        modal.setPosition();
//    }
//  });

//    $("#basic-modal-content").modal({
//        minWidth: width,
//        minHeight: height,
//        autoResize: true,
//        autoPosition: true,
//    });	
}

*/


