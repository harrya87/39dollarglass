//global variable for google analytics
var gender;
function step_one() {
    $('div.gender-selection > label[data-gender]').hover(function () {
        $(this).find('img').attr('src', '/img/tryon/' + $(this).attr('data-gender') + '-highlighted.png');
    }, function() {
        $(this).find('img').attr('src', '/img/tryon/' + $(this).attr('data-gender') + '.png');
    }).click(function (e) {
        e.preventDefault();
        $(this).find('input').attr('checked', true).change();
        //populate global variable for google analytics
        gender = $(this).attr('data-gender');
        $.ajax({
            url: $('#tryon_profile').attr('action'),
            data: $('#tryon_profile').serialize(),
            type: 'POST',
            beforeSend: addOverlay,
            success: function (data) {
                modal(data, 470);
                step_two();
            }
        });
    });

    //Preload '-highlighted' images
    $('div.gender-selection label[data-gender]').each(function () {
        $('<img src="/img/tryon/' + $(this).attr('data-gender') + '-highlighted.png" />').appendTo($('#image_preload'));
    });

    $('div.gender-selection input[name=tryon_profile]').change();

    $('#buttonLabel').click(function () {
        $(this).siblings('input').click();
    });
    generic_step_bind('one');
}

function step_two(){
  $('#uploadframe').remove();
  var iframe = $('<iframe>'),
    postForm = $('#file_upload'),
        frameName = ('resp' + Math.random()).replace('.', '');
        $('body').css('z-index',2);
  iframe
   .css({ height: '1px', width: '1px', 'z-index': 0, border: 0 })
    .attr('name', frameName)
     .attr('id', 'uploadframe')
      .attr('frameBorder',0)
       .attr('src', '/tryon_upload_form.html')
        .appendTo('body');

  iframe[0].contentWindow.name = frameName;
  
  /* Check Tablet/Mobile */
  var iOS = /iPad|iPhone|iPod/.test(navigator.platform);

  if(iOS == true)
  {
    /*$("#uploadButton").attr("multiple","multiple");
    $("#uploadButton").prop("multiple","multiple");
    $("#processFile").change(function(){

    })*/
  }

  $(".upload_photo").unbind('click').click(function(e){
    $(iframe).contents().find('#uploadButton').click();
    return !1;
  });
  
  $(".use_photo").unbind('click').click(function(e){
    e.preventDefault();
    $.ajax({
        url: '/tryon_sample.html',
        //data: $('#crop_image').serialize(),
        type: 'GET',
        beforeSend: addOverlay,
        success: function (data) {
            modal(data, 470);
            step_two_one();
        }
    });
  });

  $('#upload-overlay').unbind('click').click(function(e){
         $(iframe).contents().find('#uploadButton').click();
         return !1;
  }).hover(function(){
    $(this).css({
        background: 'url("/img/tryon/upload-button-hover.png") top left'
    });
  }, function(){
    $(this).css({
        background: 'url("/img/tryon/upload-button.png") top left'
    });
  });

  $('#start-over-link').unbind('click').click(function () {
      $(iframe).remove();
      $('#previous_step_but').click();
      return !1;
  });
  $('#error_display a.close').unbind('click').click(function(e){
      e.preventDefault();
      $('#error_display').fadeOut('slow', function(){
        $('#error_display').remove();
      });
  });
  generic_step_bind('two');
}
function step_two_one(){
  $("#sample_photo ul li a").unbind('click').click(function(e){
    imgURL=$("img", this).attr("data-src");
    filename=$("img", this).attr("data-file-name");
    e.preventDefault();
    $.ajax({
        url: '/tryon_crop_image.html',
        data: $('#currentImage').serialize() + "&upload_file=" + imgURL + "&filename=" + filename,
        type: 'POST',
        beforeSend: addOverlay,
        success: function (data) {
            modal(data, 470);
            step_two_one();
        }
    });
  });
  $(".model_nav a").unbind('click').click(function(e){
    e.preventDefault();
    var name = $(this).attr("data-class");
    $("#sample_photo li."+name).show();
    $("#sample_photo li:not(." + name + ")").hide();
  });
  generic_step_bind('two');
}
function submitForm(iframe) {
    addOverlay();
    var iframe = $('#uploadframe');
    $(iframe).load(function () {
        var result = $(iframe).contents().find('#error_display');
        if (result.length){
            $('#contentHolder').append($(result));
            $('#white_overlay').remove();
            step_two();          
        }else{
            result = $(iframe).contents().find('body').html();
            result = result.replace(/(\n|\r|\s)+$/, '');
            var matches = result.match(/(\.[^\.]+)$/);
            result = result.replace(/(\.[^\.]+)$/,'');
            result = result.replace(/[^\w]/g,'');
            result += matches[1];
            $.ajax({
                type: 'POST',
                url: '/tryon_crop_image.html',
                data: $('#file_upload').serialize() + '&upload_file=' + (result || ' '),
                dataType: 'html',
                beforeSend: addOverlay,          
                success: function (data) {
                    var height = parseInt($(data).find('#imgheight').val()) + 55;
                    if (height < 375){ height = 470 }else { height += 7;}
                    modal(data, height);
                    $(iframe).remove();
                },
                error: function(){
                    $.get('/tryon_upload_image.html?error=1', function(data){
                        modal(data);
                        step_two();
                    });
                }
            });
        }
    });
    $(iframe).contents().find('#submit').click();
};
function step_three(imgw, imgh, vx, vy, vx2, vy2, p1, p2) {
    $('#crop-button, .crop-button').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/tryon_arrange_dots.html',
            data: $('#crop_image').serialize(),
            beforeSend: addOverlay,
            success: function (data) {
                modal(data, parseInt($(data).find('#imgheight').val()) + 62);
            }
        });
    });

    var i, ac, 
      jcrop_api = $.Jcrop('#cropbox',{
          aspectRatio: 2 / 3,
          onChange: showCoords,
          onSelect: showCoords,
          minSize: [50, 75],
          bgOpacity: .4
      });
    if (p1 == 1) {
        jcrop_api.animateTo([vx, vy, vx2, vy2]);
    } else if (p1 == 2) {
        jcrop_api.animateTo([
        (200 - (((imgh - 20) / 1.5) / 2)),
        10, (200 + (((imgh - 20) / 1.5) / 2)),
        imgh - 10]);
    } else if (p1 == 4 || p1 == 3) {
        jcrop_api.animateTo([
        40, ((imgh / 2) - 240),
        340, ((imgh / 2) + 240)]);
    }
    $('#resetCrop').click(function(e){
        if (p1 == 1) {
            jcrop_api.animateTo([vx, vy, vx2, vy2]);
        } else if (p1 == 2) {
            jcrop_api.animateTo([
            (200 - (((imgh - 20) / 1.5) / 2)),
            10, (200 + (((imgh - 20) / 1.5) / 2)),
            imgh - 10]);
        } else if (p1 == 4 || p1 == 3) {
            jcrop_api.animateTo([
            40, ((imgh / 2) - 240),
            340, ((imgh / 2) + 240)]);
        }
       return false;
    });
    generic_step_bind('three');
}
function showCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);
}
    
function step_four(imgw, imgh, lx, ly ,rx, ry){
  $( '#anima-drag-left-eye' ).draggable({
     containment: '#page', 
     scroll: false,
     cursorAt: { bottom: -8 },
     stop: function() {
         var p = $('#page').position();
         var leftEyePosition = $(this).position();
         var rightEyePosition = $('#anima-drag-right-eye').position();  
         var leftEyeX = ((leftEyePosition.left + 5) *1) - p.left;
         var leftEyeY = ((leftEyePosition.top + 2) *1) - p.top;
         var rightEyeX = ((rightEyePosition.left + 5) *1) - p.left;
         var rightEyeY = ((rightEyePosition.top + 2) *1) - p.top;
         document.getElementById('LeftEyeMouseXPosition').value = (leftEyePosition.left) - p.left;
         document.getElementById('LeftEyeMouseYPosition').value = (leftEyePosition.top) - p.top;
         document.getElementById('RightEyeMouseXPosition').value = (rightEyePosition.left) - p.left;
         document.getElementById('RightEyeMouseYPosition').value = (rightEyePosition.top) - p.top;
     },
     start: function() {
       checkContinue('left' );
       $(this).find('img').css('background','none').attr('src','/img/tryon/eye-dot.png');
       $('#warningdisplay').fadeOut('200');
       $('#pddisplay').fadeOut('200');
     }   
  });

  //Drag right eye position 
  $( '#anima-drag-right-eye' ).draggable({
     containment: '#page', 
     scroll: false,
     cursorAt: { bottom: -8 },
     stop: function() {
         var p = $('#page').position();
         var rightEyePosition = $(this).position();
         var leftEyePosition = $('#anima-drag-left-eye').position(); 
         var leftEyeX = ((leftEyePosition.left + 5) *1) - p.left;
         var leftEyeY = ((leftEyePosition.top + 2) *1) - p.top;
         var rightEyeX = ((rightEyePosition.left + 5) *1) - p.left;
         var rightEyeY = ((rightEyePosition.top + 2) *1) - p.top;
         document.getElementById('LeftEyeMouseXPosition').value = (leftEyePosition.left) - p.left;
         document.getElementById('LeftEyeMouseYPosition').value = (leftEyePosition.top) - p.top;
         document.getElementById('RightEyeMouseXPosition').value = (rightEyePosition.left) - p.left;
         document.getElementById('RightEyeMouseYPosition').value = (rightEyePosition.top) - p.top;
     },
     start: function() {
       checkContinue('right');
       $(this).find('img').css('background','none').attr('src','/img/tryon/eye-dot.png');
       $('#warningdisplay').fadeOut('200');
       $('#pddisplay').fadeOut('200');
     }
  });

  
  $('#continue-button').click(function(e){
    e.preventDefault();
    if ($(this).data('enabled')){
      $.ajax({
        type: 'POST',
        url: '/tryon_adjust_glasses.html',
        data: $('#arrange_dots').serialize(),
        beforeSend: addOverlay,
        success: function(data){
          var imgh = parseInt($(data).find('#imgh').val());
          modal(data, imgh + 64);
          step_five(imgh);
        }
      });
      e.stopPropagation();
    }else{
        e.stopPropagation();
    }
  }); 
  
  var p = $('#page').position();
  $('#anima-drag-left-eye').animate({
       left: Math.round(p.left + (lx)),
       top: Math.round(p.top + (ly))
     }, 0, function() {
        $('#anima-drag-left-eye').fadeIn(50);
   });
  
  $('#anima-drag-right-eye').animate({
     left: Math.round(p.left + (rx)),
     top: Math.round(p.top + (ry))
     }, 0, function() {
        $('#anima-drag-right-eye').fadeIn(50);
   });

  $('.pd_pop').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('#pd_help').show();
      $(document).one('click', function(e){
         e.preventDefault();
         $('#pd_help').fadeOut('fast');
      });
  });
  $('#replayImage').click(function(e){
      e.preventDefault();
      replay();
  });
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  if(check==true)
  {
    $("#anima-drag-left-eye img").css("width", 30);
    $("#anima-drag-right-eye img").css("width", 30);
  }
  $('.help_button, #showImage, #hideImage').click(function(e){
      e.preventDefault();
      if ($('#picture_contain').is(':visible')){
        $('#picture_contain').slideUp();
      } else {
        $('#picture_contain').slideDown(function(){
           setTimeout("replay();", 500);   
        });
      }
  });
  $("#switchButton").click(function(e){
    if( $(this).html() == 'Enter PD')
    {
      $(".arrangeDot").css("left", "-430px");
      $(this).html("Mark Pupil")
    } else
    {
      $(".arrangeDot").css("left", "0px");
      $(this).html("Enter PD")
    }
  });
  init();
  generic_step_bind('four');
}
var pupils = {
    left: {
        top: 25,
        left: 50,
        '_left': 53,
        '_top': 136
    },
    right: {
        top: 25,
        left: 155,
        '_left': 123,
        '_top': 134
    }
};
var mouse = {
  top: '200px',
  right: '50px'
};
function init() {
  $('#cursor').css({
    top: mouse['top'],
    right: mouse['right']
  }).show();
  $('.pupil').each(function() {
      $(this).css({
          top: pupils[$(this).attr('data')]['top'] + 'px',
          left: pupils[$(this).attr('data')]['left'] + 'px'
      });
  });
  $('img.crop').attr('src', '/img/tryon/pupil_round.png');
}

function replay(){
  init();
  animate();
}
function animate(){
  $('#cursor').animate({
      top: (pupils['left']['top'] + 7) + 'px',
        left: (pupils['left']['left'] + 8) + 'px'
    }, 800, 'linear', function(){
      $('#cursor').animate({
        top: (pupils['left']['_top'] + 7) + 'px',
          left: (pupils['left']['_left'] + 8) + 'px'
      }, 800, 'linear');
      $('#left_pupil img.crop').attr('src', '/img/tryon/eye-dot.png');
      $('#left_pupil').animate({
        top: (pupils['left']['_top'] ) + 'px',
          left: (pupils['left']['_left'] ) + 'px'
      }, 800, 'linear', function(){
        $('#cursor').animate({
            top: (pupils['right']['top'] + 7 ) + 'px',
            left: (pupils['right']['left'] + 8 )+ 'px'
        }, 800, 'linear', function() {
          $('#cursor').animate({
                top: (pupils['right']['_top'] + 7)+ 'px',
                left: (pupils['right']['_left'] + 8)+ 'px'
            }, 800, 'linear');
            $('#right_pupil img.crop').attr('src', '/img/tryon/eye-dot.png');
            $('#right_pupil').animate({
                top: pupils['right']['_top'] + 'px',
                left: pupils['right']['_left'] + 'px'
            }, 800, 'linear', function(){
              $('#cursor').fadeOut('fast');
            });
        });
      });
    });
}
function step_six(imgh) {

}
function step_five(imgh){
  $('div.tint-button').click(function(e){
    e.preventDefault();
    $('#mv_click').val($('#' + $(this).attr('data-button') ).attr('name').replace('mv_click.',''));
    $.ajax({
          url: '/tryon_adjust_glasses.html',
          type: 'POST',
          data: $('#adjust_glasses').serialize()+'&adjust=1',
          beforeSend: addOverlay,
          success: function(data){
             modal(data, parseInt($('#imgh').val()) + 55);
             step_five();
          }
        });
    return false;
  });
  $(".help_button").click(function(e){
    $(".help_text").toggle();
  });
  $("#tryon-popup-body").click(function(e){
    $(".help_text").hide();
  });
  $('#previous-link').click(function(){ 
    $('#previous-button').click(); 
    return false; 
  });
  
  $('#save-button').click(function(e){
    e.preventDefault();
      //google analytics routine  
      if(gender){
          try{
                var image_path = $(tryonToolInnerFrame.document.getElementById('tryframe')).attr('src').toString().trim();
                var sku = image_path.substring(image_path.lastIndexOf("/") + 1,image_path.lastIndexOf("_")); 
                _gaq.push(['_trackEvent','Try On',('Try On ' + (gender == 'man' ? 'Men' : (gender == 'woman' ? 'Women' : 'Children'))),sku]);
            }catch(err){}
      }
    $('#mv_click').val('adjust_glasses_but');
    var frame = $('#tryonDisplayFrame').hide(),
      frameName = 'adjust_glasses';
      
        frame.attr('name', frameName);
        frame[0].contentWindow.name = frameName;
        
        $('#adjust_glasses').append(
            $('<input>').attr({
                 type: 'hidden',
                 name: 'display',
                 value: '1'
            })
        ).attr('target', frameName).submit();
        
        addOverlay();

        $(frame).one('load', function () {
              $(frame).show();

              $.ajax({
                type: 'POST',
                data: $(frame).contents().find('#sp').serialize()+'&mv_click=share_but',
                url: '/tryon_display.html',
                dataType: 'html',
                success: function(data){
                  modal(data);
                  step_six();
                }
              });

              $('#imgSlide').slideUp('fast', 'linear', function(){
                $('#frameHolder').slideDown('fast','linear',function(){
                  if ($(frame).contents().find('#showBubble').length){
                      var offset = $('#frameHolder').offset(),
                        width = ($('#frameHolder').width() + 17);
                      $('#didyouknow').css({
                        top: (offset.top + 156 ) + 'px',
                        left: (offset.left  - $('#didyouknow').width() + 210 ) + 'px',
                        position: 'absolute'
                      }).prependTo($('#mid')).fadeIn().delay(9000).fadeOut();
                      $('#closeTip').click(function(){
                        $('#didyouknow').clearQueue().fadeOut('fast');
                        return !1;
                      });
                  }
                });
              });
        });
  });
  $('.arrowlink, .rotate').hover(function(){
    var matches = $(this).find('img').attr('src').match(/^(.+)\.(jpg|png|jpeg|gif)$/i);
    matches[1] = matches[1].replace(/-hover/g,'');
    $(this).find('img').attr('src', matches[1]+'-hover.'+matches[2]);
  }, function(){
    var matches = $(this).find('img').attr('src').match(/^(.+)-hover\.(jpg|png|jpeg|gif)$/i);
    $(this).find('img').attr('src', matches[1]+'.'+matches[2]);
  });
  generic_step_bind('five');
}
function checkContinue(dir){
    $( '#anima-drag-'+dir+'-eye' ).data('moved',1);
    if ($( '#anima-drag-left-eye' ).data('moved') && $( '#anima-drag-right-eye' ).data('moved')){
      $('#continue-button')
       .data('enabled', 1)
        .css({
                opacity: 1,
                filter: 'alpha(opacity=100)'
            }).find('input').removeAttr('disabled');
    }
}

  //left button - moves glasses to left 
function arrowleft(){
     var framePos = $( tryonToolInnerFrame.document.getElementById('tryframepos') ).position();      
     var posX = framePos.left; 
       var moveX = posX - 1;
       myPosX = moveX;
       document.getElementById('tryon_moved_noseX').value = (moveX);
       $( tryonToolInnerFrame.document.getElementById('tryframepos') ).animate({    
         left: moveX
       }, 10, function() {
        // Animation complete.
       });           

}

// up button - moves glasses up 
function arrowup(){
     var framePos = $( tryonToolInnerFrame.document.getElementById('tryframepos') ).position();
     var posY = framePos.top;
       var moveY = posY - 1;
       myPosY = moveY;
       document.getElementById('tryon_moved_noseY').value = (moveY);
       $( tryonToolInnerFrame.document.getElementById('tryframepos') ).animate({ 
         top: moveY
       }, 10);
}

// right button - moves glasses to right 
function arrowright(){
     var framePos = $( tryonToolInnerFrame.document.getElementById('tryframepos') ).position();
     var posX = framePos.left;
       var moveX = posX + 1;
       myPosX = moveX;
       document.getElementById('tryon_moved_noseX').value = (moveX);
       $( tryonToolInnerFrame.document.getElementById('tryframepos') ).animate({ 
         left: moveX
       }, 10);
}

//down button - moves glasses down 
function arrowdown(){
     var framePos = $( tryonToolInnerFrame.document.getElementById('tryframepos') ).position();
     var posY = framePos.top; 
       var moveY = posY + 1;
       myPosY = moveY;
       document.getElementById('tryon_moved_noseY').value = (moveY);
       $( tryonToolInnerFrame.document.getElementById('tryframepos') ).animate({ 
         top: moveY
       }, 10);

}

// rotate right button - rotates glasses clockwise 
function rotateright(){
    var frameAngle = document.getElementById('frameAngle').value * 1,
      rotateangle = Math.round(frameAngle + 1);
    myrotation = rotateangle;
    $( tryonToolInnerFrame.document.getElementById('tryframe') ).rotateAnimation(rotateangle);
    document.getElementById('frameAngle').value = rotateangle;
    document.getElementById('tryon_moved_rotation').value = rotateangle;
}


// rotate left button - rotates glasses counter clockwise 
function rotateleft(){ 
     var frameAngle = document.getElementById('frameAngle').value * 1; 
     var rotateangle = Math.round(frameAngle - 1);
     myrotation = rotateangle;
    $( tryonToolInnerFrame.document.getElementById('tryframe') ).rotateAnimation(rotateangle);
    document.getElementById('frameAngle').value = rotateangle;
    document.getElementById('tryon_moved_rotation').value = rotateangle;
}
function modal(data, height, width){
  var top;
  //hate IE
  var scrolltop = document.documentElement.scrollTop ? 
              document.documentElement.scrollTop : 
              document.body.scrollTop;
  if ($(window).height() < ((height || 460)+ 10)){
       top = scrolltop;
  }else{
     top = ((($(window).height() - (height || 460)) /2 )  + scrolltop );
  }
  $('#modal_overlay').show();
  $('#contentHolder')
    .html(data || '')
     .css('width',(width || 820 )+'px')
      .css({
       height: ((height && height > 460) ? height : 460)+'px',
        top: top + 'px',
        left: (($(window).width() - ($('#contentHolder').width() || 790) ) /2 )+'px',
        'z-index' : 99999,
        position: 'absolute'
  }).show().draggable({ handle: '#tryon-popup-header'});
}

function generic_step_bind(){
  $('.close_dialog, #modal_overlay').unbind('click').click(function(e){
     e.preventDefault();
     $('#contentHolder').fadeOut('fast'); 
     $('#modal_overlay').hide();
  });
  $('#contentHolder').click(function(e){
    e.stopPropagation();
    e.preventDefault();
    return !1;
  })
  $('#previous-link-div, .previous-link-div').unbind('click').click(function(e){
       e.preventDefault();
       $.ajax({
        type: 'POST',
        data: {
            mv_action: 'process',
            mv_todo: 'return',
            mv_click: 'previous_step_but'
        },
        url: '/process.html',
        beforeSend: addOverlay,
        success: function(data){
            var val = $(data).find('#imgheight').val();
            if (val){ try{ val = parseInt(val); val += 55; } catch(err){} }
            modal(data, val || 0);
            var step = $('#current_step').val();
            if (step && step != 'four' && step != 'three'){
                eval('step_'+step+'()');
            }
        }
       });
       e.stopPropagation();
  });
  $('div.continue').unbind('hover').hover(function(e){
     if ($('#current_step').val() == 'four' && !$('#continue-button').data('enabled')){
       return !1;
     }
     e.stopPropagation();
     var matches = $(this).find('input').attr('src').match(/^(.+)\.(jpg|png|jpeg|gif)$/i);
     matches[1] = matches[1].replace(/-hover/g,'');
     $(this).find('input').attr('src', matches[1]+'-hover.'+matches[2]);
     return !1;
  }, function(e){
    if ($('#current_step').val() == 'four' && !$('#continue-button').data('enabled')){
       return !1;
     }
    e.stopPropagation();
    var matches = $(this).find('input').attr('src').match(/^(.+)-hover\.(jpg|png|jpeg|gif)$/i);
    matches[1] = matches[1].replace(/-hover/g,'');
    $(this).find('input').attr('src', matches[1]+'.'+matches[2]);
    return !1;
  });
}

function addOverlay(offset){
    var tryon_pop = $('#tryon-popup-body');
    $('#white_overlay').remove();
    var div = $('<div id="white_overlay" />').css({
        width: (tryon_pop.width() ) + 'px',
        height: (tryon_pop.height()) + 'px'
    });
    div.append(
      $('.image_preload img.load')
       .clone()
        .attr('id','image_loader')
         .css({
             top: ((tryon_pop.height() - 120 ) /2 )+'px'
         })
    );
    return $('#tryon-popup-body').append(div);
}

function touchHandler(event){
       var touches = event.changedTouches,
               first = touches[0],
               type = "";

       switch(event.type){
               case "touchstart": type = "mousedown"; break;
               case "touchmove":  type="mousemove"; break;
               case "touchend":   type="mouseup"; break;
               default: return;
       }

    //initMouseEvent(type, canBubble, cancelable, view, clickCount,
    //           screenX, screenY, clientX, clientY, ctrlKey,
    //           altKey, shiftKey, metaKey, button, relatedTarget);

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent(type, true, true, window, 1,
       first.screenX, first.screenY,
       first.clientX, first.clientY, false,
       false, false, false, 0/*left*/, null);

    first.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}