sfHover = function() {
	var sfEls = document.getElementById("top_nav_menu").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);
function hideMenu(){
	jQuery('#loginMenu, #howtoorderMenu' ).hide();
	return false;
}
function openMenu(menu)
{
	alert('wtf')
	document.getElementById(menu).style.display='block';
	return false;
}

jQuery(function(){
	jQuery('a.poplight[href^=#]').click(function() {

			var popID = jQuery(this).attr('rel'); //Get Popup Name
			var popURL = jQuery(this).attr('href'); //Get Popup href to define size
	
			//Pull Query & Variables from href URL
			var query= popURL.split('?');
			var dim= query[1].split('&');
			var popWidth = dim[0].split('=')[1]; //Gets the first query string value
	
			//Fade in the Popup and add close button
			jQuery('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }); //.prepend('<a href="#" class="close"><img src="close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>');
	
			//Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
			var popMargTop = (jQuery('#' + popID).height() + 80) / 2;
			var popMargLeft = (jQuery('#' + popID).width() + 80) / 2;
	
			//Apply Margin to Popup
			jQuery('#' + popID).css({
					'margin-top' : -popMargTop,
					'margin-left' : -popMargLeft
			});
			//Fade in Background
			jQuery('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
			jQuery('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 

			//Close Popups and Fade Layer
			jQuery('#fade').click( function(){ //When clicking on the close or fade layer...
					jQuery('#fade , .popup_block').fadeOut(function() {
							jQuery('#fade').remove();  //fade them both out
					});
					return false;
			});
			
			return false;
	});
});
