jQuery(function(){
	jQuery('a.poplight[href^=#]').click(function() {
			var $this = jQuery(this);
			var popID = $this.attr('rel'); //Get Popup Name
			var popURL = $this.attr('href'); //Get Popup href to define size
	
			//Pull Query & Variables from href URL
			var query= popURL.split('?');
			var dim= query[1].split('&');
			var popWidth = $this.attr('data-width') || dim[0].split('=')[1]; //Use either data-width or query string
	
			if( !popID ){
				//Otherwise it's an ajax popup, treat it as such
				jQuery('#infoLayer').load( $this.attr('data-url') );
				popID = 'infoLayer';
			}
	
			//Fade in the Popup
			jQuery('#' + popID).fadeIn().css({ 'width': Number( popWidth ) });
			
			//Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
			var popMargTop = (jQuery('#' + popID).height() + 80) / 2;
			var popMargLeft = (jQuery('#' + popID).width() + 80) / 2;
	
			//Apply Margin to Popup
			jQuery('#' + popID).css({
					'margin-top' : -popMargTop,
					'margin-left' : -popMargLeft
			});
			//Fade in Background
			jQuery('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
			jQuery('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 

			//Close Popups and Fade Layer
			jQuery('#fade').click( function(){ //When clicking on the close or fade layer...
					jQuery('#fade , .popup_block').fadeOut(function() {
							jQuery('#fade').remove();  //fade them both out
					});
					return false;
			});
			
			return false;
	});
});
