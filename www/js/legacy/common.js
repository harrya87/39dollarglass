//START - FLYPAGE FORM CHECK

function checkFlypageForm() {
	var the_form = document.getElementById('orderform');
	// var the_form = document.orderform;
	
	//hide the errorblock
	errordisplay = document.getElementById('form_error_display');
	if(errordisplay) errordisplay.style.display='none';

	var checked = new Array();
	checked['thiscolor'] = false;
	/*checked['mv_order_lens'] = false;*/

	// is thiscolor checked?
	// if there's only one radio button
	if(the_form.thiscolor.checked) {
		checked['thiscolor'] = true;
	}

	var j = the_form.thiscolor.length;
	for(var i=0; i< j; i++) {
		if(the_form.thiscolor[i].checked) {
			checked['thiscolor'] = true;
			break;
		}
	}
	
	/*
	// is mv_order_lens checked?
	// if there's only one radio button
	if(the_form.mv_order_lens.checked) {
		checked['mv_order_lens'] = true;
	}

	var j = the_form.mv_order_lens.length;
	for(var i=0; i< j; i++) {
		if(the_form.mv_order_lens[i].checked) {
			checked['mv_order_lens'] = true;
			break;
		}
	}
	*/


	// populate global array of error codes
	if(!checked['thiscolor']) {
		errs.push('1_nc_fc');
	}
	/*
	if(!checked['mv_order_lens']) {
		errs.push('1_nc_lt');
	}
	*/
	
	if(errCheck())
	{
		the_form.submit();
		// document.orderform.submit();
	}	
}
//END - FLYPAGE FORM CHECK


//START - FLYPAGE POPUP PDF
function pdfpop() {
var URL = "nopdf.html";
var id = 46;
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=300,height=300');");
}
//END - FLYPAGE POPUP PDF


// start common functions
function inArray(the_val, the_array) {
	for(idx in the_array) {
		if(the_array[idx] == the_val) {
			return true;
		}
	}
	return false;
}

function objHasArray(obj) {
	if(obj[0]) {
		return true;
	} else {
		return false;
	}
}

function checkedVals(el) {
	vals = new Array();

	j = el.length;
	for(var i=0; i<j; i++) {
		if(el[i].checked) {
			vals.push(el[i].value);
		}
	}
	if(vals.length > 0) {
		return vals;
	} else {
		return false;
	}
}

function roundCurrency(n) {
	var s = "" + Math.round(n * 100) / 100;
	var i = s.indexOf('.');
	if (i < 0) { return s + ".00"; }

	var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3);
	if (i + 2 == s.length) {
		t += "0";
	}
	return t;
}

var pop_window;
function popWin(pop_url, wintype) {

	// window property definitions
	if(wintype == 'package_info') {
		pop_window = open(pop_url, 'package_info',',width=400,height=450,scrollbars=yes,resizable=yes');
	} else if(wintype == 'external') {
		pop_window = open(pop_url, '',',width=625,height=400,scrollbars=yes,resizable=yes');
	} else if(wintype == 'colors') {
		pop_window = open(pop_url, '',',width=725,height=520');
	} else if(wintype == 'update_prescription') {
		pop_window = open(pop_url, 'update_prescription',',width=550,height=400');
	} else if(wintype == 'smartfit') {
		pop_window = open(pop_url, 'smartfit',',width=300,height=400');
	} else if(wintype == 'open_frame') {
		pop_window = open(pop_url, '',',width=800,height=400');
	} else if(wintype == 'info') {
		pop_window = open(pop_url, 'info',',width=550,height=620');
	} else if(wintype == 'fsainfo') {
		pop_window = open(pop_url, 'info',',width=550,height=500');
	} else if(wintype =='error') {
		pop_window = open(pop_url, 'error',',width=300,height=300');
	} else if(wintype =='telecheck') {
		pop_window = open(pop_url, 'telecheck',',width=780,height=580,scrollbars=yes,resizable=yes');
	} else {
		pop_window = open(pop_url, 'thewindow',',width=400,height=500');
	}

	// minimum v4.x browsers
	if (parseInt(navigator.appVersion) >= 4) {
		setTimeout('focusPop()',1000);
	} else {
		// low tech version of the page
		top.location=pop_url;
	}
}

function focusPop(){
	pop_window.focus();	
}

