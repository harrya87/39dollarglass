//this function can be used to create js radio buttons

var jsRadioOn = new Image();
var jsRadioOff = new Image();
jsRadioOn.src = '/store/images/radio_on.gif';
jsRadioOff.src = '/store/images/radio_off.gif';

// will be populated with appropriate error codes
// by checkForm() function in different forms
errs = new Array();

function calcPrice() {
/*
	if(f){
		var the_form = eval(f);
		var the_form_text = f;
	} else {
		var the_form = document.orderform;
		var the_form_text = 'document.orderform';
	}
*/
	var the_form = document.getElementById('orderform');
	var the_form_text = 'document.getElementById(\'orderform\')';
	var ar_div = document.getElementById('ar_div');
	
	// ***	
	if (the_form.mv_order_coating) {
		var my_lens_materials = new Array();
		my_lens_materials = checkedVals(the_form.mv_order_lenstype);
		var my_lens_material = my_lens_materials[0];		
		if(!my_lens_material)
		{
			my_lens_material = the_form.mv_order_lenstype[0].value;
		}		
		if ((my_lens_material.indexOf('SUN') !=-1) || (my_lens_material.indexOf('POLS') !=-1) || (my_lens_material.indexOf('TRAN') !=-1)) { 
			the_form.mv_order_tint.value = 'GREY';
		} else {
			the_form.mv_order_tint.value = 'CLEAR';
		}
	}
	// ***

	// check whether A/R is allowed or not
	if (the_form.mv_order_coating) {
		
		var the_lens_materials = new Array();
		the_lens_materials = checkedVals(the_form.mv_order_lenstype);
		the_lens_material = the_lens_materials[0];
		if(!the_lens_material)
		{
			the_lens_material = the_form.mv_order_lenstype[0].value;
		}		
		
		//if (the_lens_material == 'SUN1' || the_lens_material == 'SUN3' || the_lens_material == 'SUN3_2' || the_lens_material == 'SUN9' || the_lens_material == 'SUN10' || the_lens_material == 'SUN9_2' || the_lens_material == 'BIF_SUN11' || the_lens_material == 'BIF_SUN12' || the_lens_material == 'BIF_SUN13' || the_lens_material == 'PROGE_SUN7' || the_lens_material == 'PROGE_SUN7_2' || the_lens_material == 'PROGE_SUN18' || the_lens_material == 'PROGE_SUN19') {
		if (the_lens_material.indexOf('SUN') !=-1 || the_lens_material == 'POLS9' || the_lens_material == 'BIF_POLS10' || the_lens_material == 'PROG_POLS11'
			|| the_lens_material == 'POLS12' || the_lens_material == 'POLS13')
		{
			// packages C, D, I
			// don't allow you to select A/R
			var the_coatings = new Array();
			the_coatings = checkedVals(the_form.mv_order_coating);
			the_coating = the_coatings[0];

			ar_div.style.display = 'none';

			if (the_coating == 'COA1') {
				the_form.mv_order_coating[0].checked = true;
				//alert('Anti-reflective coating is not available with this package\nPlease select another package if you require anti-reflective coating');

				
			}
		}
		else
		{
//			if(rimless_frame > 0) {
//			}
//			else {
				ar_div.style.display = 'block';
//			}
		}

		if (the_form.mv_order_coating.checked) {
			// package T, C2 & D2
			if (the_lens_material == 'SUN8' || the_lens_material == 'SUN3' || the_lens_material == 'SUN10' || the_lens_material == 'BIF_SUN13' || the_lens_material == 'PROGE_SUN19') {
				// turn off A/R 
				//alert('Anti Reflective Coating is not available with this package');
				the_form.mv_order_coating.checked = false;

				// swap the divs over
				ar_div.style.display = 'none';
				//return;
			}
		} else {
			// if user clicks elsewhere, turn A/R back on if it's otherwise mandatory
			if (the_form.mv_order_coating.value == 'COA4' && the_form.mv_order_coating.checked == false
				&& the_lens_material != 'SUN8' && the_lens_material != 'SUN1-2' && the_lens_material != 'SUN3' && the_lens_material != 'SUN3_2' && the_lens_material != 'SUN10' && the_lens_material != 'BIF_SUN13' && the_lens_material != 'PROGE_SUN19') {
				the_form.mv_order_coating.checked = true;

				// swap the divs over
				ar_div.style.display = 'block';
				//return;
			}
		}


		if(the_lens_material == 'OPT4')
		{
			ar_div.style.display = 'none';
		}


	}
	// end of A/R bit

	// get current prices
	var your_cost = base_price;
	var avg_retail = base_retail;

	var num_groups = check_groups.length;
	for(var idx=0; idx < num_groups; idx++) {
		form_group = eval(the_form_text+"."+check_groups[idx]);
		if(form_group) {
			var j = form_group.length;
			var jtype = form_group.type;
			if((jtype != 'radio') && (form_group.name == 'mv_order_case'))
			{
                                your_cost += cost[form_group.name][form_group.value];
                                avg_retail += retail[form_group.name][form_group.value];
			}
			else
			{

				// if there are multiple options for this radio group
				if(j) {
					for(var i=0; i< j; i++) {
						input_el = eval(the_form_text+"."+check_groups[idx] + '[' + i + ']');					
						if(input_el.checked) {
							your_cost += cost[input_el.name][input_el.value];
							avg_retail += retail[input_el.name][input_el.value];
						
							//alert(input_el.name+' '+input_el.value);
							break;
						}
					}
				// if there is one option for this radio group
				} else {
					input_el = eval(the_form_text+"."+check_groups[idx]);
					if(input_el.checked) {
						your_cost += cost[input_el.name][input_el.value];
						avg_retail += retail[input_el.name][input_el.value];
					}
				}
			}
		}
	}

	// set price boxes
	var you_save = avg_retail - your_cost;
	you_save = roundCurrency(you_save);

	the_form.your_cost.value = roundCurrency(your_cost);
	the_form.avg_retail.value = roundCurrency(avg_retail);
	the_form.you_save.value = roundCurrency(you_save);

	if(document.getElementById('ycost2'))
	{
		document.getElementById('ycost2').innerHTML = '$' + roundCurrency(your_cost);
		document.getElementById('avg_retail2').innerHTML = avg_retail_txt + ' $' + roundCurrency(avg_retail);
		document.getElementById('you_save2').innerHTML = you_save_txt + ' $' + roundCurrency(you_save);
	}
	else if(the_form.your_cost2)
	{
		the_form.your_cost2.value = roundCurrency(your_cost);
		the_form.avg_retail2.value = roundCurrency(avg_retail);
		the_form.you_save2.value = roundCurrency(you_save);	
	}

	var sel = document.getElementById('ship_msg');
	if(sel == undefined) 
		return;

	if (roundCurrency(your_cost) > 99){
		sel.style.display = 'block';
	} else {
		sel.style.display = 'none';
	}

	// update multi-currency bit
	do_multi();

}

function errClear(step)
{
	errordisplay = document.getElementById('form_error_display_'+step);
	if(errordisplay) errordisplay.style.display='none';
}
// if any errors were found in the form, display the error window
function errCheck(step) {
	var err_count = errs.length;
	if(err_count > 0) {
		var err_string;
		err_string = '<b>There is a slight problem:</b><ul>';
		//err_string += errs.join('<br />');
		errordisplay = document.getElementById('form_error_display_' + step);
		errormsg = document.getElementById('form_error_msg_' + step);
		
		//hide the errorblock
		errClear(step);

		// minimum v4.x browsers
		//if (parseInt(navigator.appVersion) >= 4) {
		//	popWin('/messages/error.php?' + err_string + '&locale=' + locale,'error');
		//}
		
		if(errordisplay && errormsg)
		{
			for(i=0;i<err_count;i++)
			{
				err_string += '<li>'+form_error_array[errs[i]]+'</li>';
			}
			errormsg.innerHTML = err_string+'</ul>';
			errordisplay.style.display='block';
		}
		else
		{
			//for some reason we dont have and object to display the error message
			//use stand alert inthis case
			err_string = "Error:\n";
			for(i=0;i<err_count;i++)
			{
				// cameron added input name debugging
				// err_string += '(' + errs[i] + ') ' + form_error_array[errs[i]]+"\n";
				err_string += form_error_array[errs[i]]+"\n";
				alert(err_string);
			}
		}
		
		// clear the error stack
		errs = new Array();
		return false;
	}
	return true;
}

function doChecks(el) {
	if(tintCheck(el)) {
		if(el.name == 'mv_order_lenstype')
		{
			selectLensPackage();		
		}
		calcPrice();
		return true;
	} else {
		return false;
	}
}

var all_tints = new Array('BROWN', 'GREY', 'G15', 'BLUE', 'ROSE', 'ORANGE', 'YELLOW');

function hideTints() {
	for(i=0;i<all_tints.length;i++) {
       		document.getElementById('tint_'+all_tints[i]).style.display = 'none';
        }
	document.getElementById('tint_color_title').style.display='none';
	document.getElementById('colorcombinations').style.display = 'none';
}

function clearTints() {
	var the_form = document.getElementById('orderform');
	// var the_form = document.orderform;
	
	//clear all the tint buttons
	for(i=0; i < all_tints.length; i++) {
		if(document.getElementById('tint_'+all_tints[i]))
		{
			document.getElementById('tint_'+all_tints[i]).className = 'tintButton';	
			document.getElementById('mv_order_tint_'+all_tints[i]).src = jsRadioOff.src;
		}
	}		
}

var currentTint;
function setTint(tc) {
	clearTints();	
	currentTint = document.getElementById('mv_order_tint').value;
	if(tc != 'CLEAR')
	{			
		document.getElementById('tint_'+tc).className = 'tintButtonSel';
		document.getElementById('mv_order_tint_'+tc).src = jsRadioOn.src;	
	}	
	document.getElementById('mv_order_tint').value = tc;	
	currentTint = tc;
}

var allowed_tints_count = 0;

function tintCheck(el) {

	var the_form = document.getElementById('orderform');
	// var the_form = document.orderform;
	// clr: clear lens
	// sun: regular sun lenses
	// pol: polarized sun lenses
	// trn: transitions

	// these lens types have only certain tints allowed
	var allowed_tints = new Array();
	allowed_tints_count = 0;
	
	allowed_tints['clear'] = new Array();
	allowed_tints['sun'] = new Array('BROWN', 'GREY', 'G15', 'BLUE', 'ROSE', 'ORANGE', 'YELLOW');
	allowed_tints['pol'] = new Array('BROWN', 'GREY');
	allowed_tints['tran'] = new Array('BROWN', 'GREY');
	allowed_tints['proge'] = new Array('BROWN', 'GREY', 'G15');
	
	//package specific tints
	allowed_tints['PROGE_TRAN5'] = new Array('GREY');

	// get lens material code
	var lens_materials = new Array();
	lens_materials = checkedVals(the_form.mv_order_lenstype);
	lens_material = lens_materials[0];

	var lens_material_code;

	// OPT are always clear
	if((lens_material.indexOf('OPT') == 0) 
		|| (lens_material.indexOf('OPT') == 4)
		|| (lens_material.indexOf('OPT') == 5)
		|| (lens_material.indexOf('OPT') == 6)) {
		lens_material_code = 'clear';
	}
	
	// SUN 1 and 3 can have any tint
	//if(lens_material == 'SUN1' || lens_material == 'SUN3' || lens_material == 'SUN3_2' || lens_material == 'SUN8' || lens_material == 'SUN9' || lens_material == 'SUN10') {
	if (lens_material.indexOf('SUN') !=-1) {
		lens_material_code = 'sun';
		//alert('sun1');
	// any other SUN are polycarbonate, can only have CLEAR, BROWN or GREY
	} else if((lens_material.indexOf('SUN') == 0)
		|| (lens_material.indexOf('SUN') == 4)
		|| (lens_material.indexOf('SUN') == 5)) {
		lens_material_code = 'pol';
		//alert('sun2');
	} 
	if (lens_material.indexOf('POLS')!=-1) {
		lens_material_code = 'pol';
		//alert('pol');
	}
	if(lens_material.indexOf('TRAN') !=-1) {
		lens_material_code = 'tran';
		//alert('tran');
	}

	// get lens tint checked
	var lens_tints = new Array();
	lens_tint = document.getElementById('mv_order_tint').value;
	
	if(el.name != 'mv_order_varilux') {
		clearTints();
	}
	
	// if a clear lens material has been checked, set mv_order_tint to clear
	// otherwise unset mv_order_tint
	if(el.name == 'mv_order_lenstype') {
		if(lens_material_code == 'clear') {			
			setTint('CLEAR');
			//clear all the tint buttons		
		} else {
			setTint('GREY');
		}
	}

	// if they've checked a lens tint (or clear) make sure it matches the material
	
	// if a lens tint or lens material has just been checked, make sure the tints are allowed tints
	if(el == 'mv_order_tint' ||
		el.name == 'mv_order_lenstype') {
			
		// if there's no lens tint checked, we can skip this
		if(lens_tint) {
			// does this material only allow certain tints?
			for(i=0;i<all_tints.length;i++) {
				//alert('checking '+all_tints[i]+' '+lens_material_code);
				
				var allowed_array = allowed_tints[lens_material_code];
				if(Object.isArray(allowed_tints[el.value]))
				{
					allowed_array	= allowed_tints[el.value];
				}				
				if(inArray(all_tints[i], allowed_array)) {
					document.getElementById('tint_'+all_tints[i]).style.display = 'block';
					allowed_tints_count++;		
				} else {
					document.getElementById('tint_'+all_tints[i]).style.display = 'none';		
				}
			}
			if(allowed_tints[lens_material_code].length > 0)
			{
				document.getElementById('colorcombinations').style.display = 'block';		
			}
			else
			{
				document.getElementById('colorcombinations').style.display = 'none';		
			}
		}
		if(allowed_tints_count > 0) {
			document.getElementById('tint_color_title').style.display='block';	
		} else {
			document.getElementById('tint_color_title').style.display='none';		
		}
		
	}
	
	//new drivewear lenses cannot have tints
	$('ar_included').hide();
	$('drivewear_tint_info').hide();
	$('lensoptions_title').update('Customize your lenses further');
	if($('varilux_div'))
	{
		$('varilux_div').show();
	}
	if(lens_material == 'POLS9' || lens_material == 'BIF_POLS10' || lens_material == 'PROG_POLS11')
	{
		clearTints();
		hideTints();
		setTint("CLEAR");
		//show the drivewear information
		//Your Drivewear lenses change from:
		$('lensoptions_title').update('Your Drivewear lenses change from:');
		$('drivewear_tint_info').show();
		if($('varilux_div'))
		{
			$('varilux_div').hide();
		}
	}
	else if (el.value == 'OPT4')
	{
		clearTints();
		hideTints();
		setTint("CLEAR");
		$('lensoptions_title').update('Our premium anti-reflective coating is included');
		$('ar_included').show();
		if($('varilux_div'))
		{
			$('varilux_div').hide();
		}
	}
	
	// Varilux checks
	varilux = eval('document.orderform.mv_order_varilux');
	sola_ok = eval('document.orderform.sola_ok');

	// if sola's not ok, this field can't be changed
	if(!sola_ok && varilux) {
		varilux.checked = true;
	}

	return true;
}

