var mousePos;
var xmouse = 0;
var ymouse = 0;
var xoffset = 200;
var yoffset = 350;
var largeImageOn = false;
var largeCaseOn = false;
var infoLayerOn = false;
var selectedCase = 'CLAMSHELL';
var t;
var firstSelected = false;
var showingOptions = false;
var shownCaseSelect = true;
var selectingLens = false;
var firstSelectedType = false;
var selectingLensType = false;
var selectingPrescription = false;
var showingPrescription = false;
var selectingNosePos = false;
var showingNosePos = false;
var selectsHidden = false;
var loadedprescription = false;

function hideSelects(theaction) {	
	ua = navigator.userAgent;
  s = "MSIE";
  if ((i = ua.indexOf(s)) >= 0) {
    version = parseFloat(ua.substr(i + s.length));
    if(version < 7)
    {
			if (theaction!='visible') {
				theaction='hidden';
				selectsHidden = true;
			} else {
				selectsHidden = false;
			}	
			for (var S = 0; S < document.forms.length; S++) {
				for (var R = 0; R < document.forms[S].length; R++) {
					if (document.forms[S].elements[R].options) {
						document.forms[S].elements[R].style.visibility = theaction;
					}
				}
			}
		}
	}
}

function proceed_button_swap(btnobj,state)
{
	if(state == 'over') {
		document.getElementById(btnobj).className = 'proceedButtonOver';
	} else {
		document.getElementById(btnobj).className = 'proceedButton';
	}
}

function largeFrameView(framecolor)
{	
	var largeFrameLayer;
	var largeFrameImage;
	if(!largeImageOn)
	{		
		var iebody=(document.compatMode && document.compatMode != "BackCompat")? document.documentElement : document.body;
		var bodyScrollLeft=document.all? iebody.scrollLeft : pageXOffset;
		var bodyScrolltop=document.all? iebody.scrollTop : pageYOffset;
		
		var largeFrameSelect = document.getElementById('largeFrameLink'+framecolor);
				
		var fleft = 0;
		var ftop = 0;
	
		while (largeFrameSelect.offsetParent){
			fleft += largeFrameSelect.offsetLeft;
			ftop += largeFrameSelect.offsetTop;
			largeFrameSelect = largeFrameSelect.offsetParent;
		}
		
		fleft += largeFrameSelect.offsetLeft;
		ftop += largeFrameSelect.offsetTop;
		
		layertop = ftop - 320;
		layerleft = fleft - 200;
		
		if(layertop < 0)
		{
			layertop = 10;
		}

		largeFrameLayer = document.getElementById('largeFrameLayer');				
		largeFrameLayer.style.top  = (layertop + bodyScrolltop) + 'px';
	  largeFrameLayer.style.left = layerleft + 'px';
	  
	  largeFrameImage = document.getElementById('largeFrameImage');
		largeFrameImage.src = frameLargeImages[framecolor].src;
		
		largeFrameShow();
	}
	else
	{
		largeFrameHide();
		setTimeout("largeFrameView('"+framecolor+"');",250);
	}
}

function largeFrameShow()
{
	Effect.Appear('largeFrameLayer',{duration:0.2});
	largeImageOn = true;	
}

function largeFrameHide()
{
	Effect.Fade('largeFrameLayer',{duration:0.2});
	largeImageOn = false;
}

function requestUrl(infourl,returnObj) {
    var httpRequest;

    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
        if (httpRequest.overrideMimeType) {
            httpRequest.overrideMimeType('text/xml');
            // See note below about this line
        }
    } else if (window.ActiveXObject) { // IE
        try {
            httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
        }
    }

    if (!httpRequest) {
        //alert('Giving up :( Cannot create an XMLHTTP instance');
        returnObj.innerHTML = 'Sorry information could not be loaded.';
        return false;
    }
    httpRequest.onreadystatechange = function() { processRequest(httpRequest,infourl,returnObj); };
    
    r = Math.random(20);
    
    if(infourl.indexOf('?') == -1){
    	infourl += '?c='+r;
    } else {
    	infourl += '&c='+r;
    }  	
    	
    httpRequest.open('GET', infourl, true);
    httpRequest.send(null);

}

function processRequest(httpRequest,infourl,returnObj)
{
	if (httpRequest.readyState == 4) {
		if (httpRequest.status == 200) {
			returnObj.innerHTML = '<div style="padding:5px;">'+httpRequest.responseText+'</div>';		  
		} else {
			returnObj.innerHTML = 'Sorry information could not be loaded.';
		}
	}
}

function infoLayerDisplay(infotitle,infourl,wintype)
{
	t2=setTimeout("infoLayerDisplayInit('"+infotitle+"','"+infourl+"','"+wintype+"')", 250);			
}

function infoLayerDisplayInit(infotitle,infourl,wintype)
{
	var infoLayer;
	var infoLayerTable;
	var infoLayerContainer;
	var infoLayerHeadTitle;
	var infoLayerContent;
	var infoLayerHead;	

	clearTimeout(t);
	// window property definitions
	if(wintype == 'package_info') {
		w=400;
		h=450;
	} else if(wintype == 'package_info2') {
		w=664;
		h=680;
	} else if(wintype == 'lenstype_info') {
		w=360;
		h=220;
	} else if(wintype == 'external') {
		w=625;
		h=400;
	} else if(wintype == 'colors') {
		w=625;
		h=450;
	} else if(wintype == 'update_prescription') {
		w=550;
		h=400;		
	} else if(wintype == 'smartfit') {
		w=300;
		h=400;
	} else if(wintype == 'open_frame') {
		w=800;
		h=400;
	} else if(wintype == 'numberinfo') {
		w=560;
		h=560;
	} else if(wintype == 'info') {
		w=560;
		h=500;
	} else if(wintype == 'fsainfo') {
		w=550;
		h=400;
	} else if(wintype == 'prescriptioninfo') {
		w=780;
		h=500;
	} else if(wintype == 'no_ar_coating') {
		w=400;
		h=150;
	} else if(wintype =='error') {
		w=300;
		h=300;
	} else if(wintype =='telecheck') {
		w=780;
		h=580;
	} else {		
		w=400;
		h=500;
	}	
	
	//get object and reset content
	
	//h+=10;
	infoLayer = document.getElementById('infoLayer');
	infoLayer.style.width=w+'px';	
	infoLayer.style.height=h+'px';	
	
	infoLayerTable = document.getElementById('infoLayerTable');
	infoLayerTable.style.width=w+'px';
	infoLayerContainer = document.getElementById('infoLayerContainer');
	infoLayerContainer.style.width=(w-22)+'px';
	//h-=10;
	//infoLayer.style.display='block';	
	Effect.Appear('infoLayer',{duration:0.3});
	
	//infoLayerHead = document.getElementById('infoLayerHead');
	//infoLayerHead.style.width=w+'px';	
	infoLayerHeadTitle = document.getElementById('infoLayerHeadTitle');
	infoLayerHeadTitle.innerHTML = infotitle;	
	
	h-=21; //minus 20px for the header
	//w-=20; //minus 20px for the header
	//set width and height for new info
	infoLayerContent = document.getElementById('infoLayerContent');	
	infoLayerContent.innerHTML = '';
	infoLayerContent.style.width=(w-22)+'px';	
	infoLayerContent.style.height=h+'px';	
	infoLayerContent.style.display='block';	
		
	infoLayerContent.innerHTML = '<img src="/store/images/blueyellow/infoloading.gif" alt="Loading" /> Loading Information<br />Please Wait...';
	
	//move the layer so its in the scroll area
	var iebody=(document.compatMode && document.compatMode != "BackCompat")? document.documentElement : document.body;
	var infoLayerLeft=document.all? iebody.scrollLeft : pageXOffset;
	var infoLayerTop=document.all? iebody.scrollTop : pageYOffset;
		
	infoLayer.style.top=(infoLayerTop + 20)+'px';	
	infoLayer.style.left=(infoLayerLeft + 20)+'px';		
		
	infoLayerOn = true;
	
	//retrieve the help info
	requestUrl(infourl,infoLayerContent);	

}

function infoLayerReset()
{
	if(infoLayerOn)
	{
		t=setTimeout("infoLayerResetHide()", 500);		
	}
	else
	{
		clearTimeout(t2);	
	}
}

function infoLayerResetHide()
{
	if(selectsHidden)
	{
		hideSelects('visible');
	}
	//var infoLayer;
	//infoLayer = document.getElementById('infoLayer');	
	//infoLayer.style.display='none';	
	Effect.Fade('infoLayer',{duration:0.3});
	//infoLayer = false;
}


/**** START ---- STEP2 LENS TYPE AND NOSE POSITION SELECTION ****/

function selectLensType()
{
	var the_form = document.orderform;
	if(the_form.mv_order_lens.length) {
		var selectedLensType = checkedVals(the_form.mv_order_lens);
	} else {
		var selectedLensType = the_form.mv_order_lens.value;	
	}

	if(selectedLensType)
	{	
		calcPrice();
		errClear();
		update_noseposition();
	
		if(!shownosepos)
		{
			hideLensTypeSelection();
		}				
	}
}

function loadPrescription(step)
{
	var url = '/step2_prescription.html?r='+Math.random()*100;
	new Ajax.Request(url, {
	  method: 'get',
	  onSuccess: function(transport)
	  {  
	    if(transport.responseText)
	   	{
	   		Element.update('prescription_data',transport.responseText);
	   		Element.hide('prescription_loading');
	  	} 
	    else
	    {
	    	Element.setStyle('prescription_loading',{border: '1px solid red'});
	    	Element.update('prescription_loading','Failed to load your prescription interface, please call us on 1-800-672-6304');			    	
	    } 
			
	  }	  
	});		
	loadedprescription = true;	
}

function reloadNosePos()
{
	var the_form = document.orderform;
	var selectedNosePos = checkedVals(the_form.mv_order_nosepos);	
	if(selectedNosePos)
	{
		selectNosePosition();
	}
}

function showPrescriptionBox()
{
	if((selectingLensType == false) && (showingPrescription == false))
	{			
		Effect.BlindDown('prescriptionbox',{duration:0.7,queue:'end'});	
		selectingLensType = true;
		showingPrescription = true;
		setTimeout("window.scrollBy(0,90);",1000); //scroll the window down abit so the "step 3" button is visible
		setTimeout("selectingLensType = false;",700);	
		if(!loadedprescription)
		{		
			//wait 1 second for animation to finish then load the prescription
			setTimeout("loadPrescription();",1000);
		}
	}
}

function hideLensTypeSelection()
{
	if(selectingLensType == false)
	{			
		var the_form = document.orderform;
		if(the_form.mv_order_lens.length) {
			var selectedLensType = checkedVals(the_form.mv_order_lens);
		} else {
			var selectedLensType = the_form.mv_order_lens.value;	
		}		
		
		selectingLensType = true;
		document.getElementById('currentLensTypeSelectionLens').innerHTML = document.getElementById('mv_order_lens_'+selectedLensType+'_label').innerHTML;	
		Effect.BlindUp('fullLensTypeSelection',{duration:0.3,queue:'end'});
		Effect.BlindDown('currentLensTypeSelection',{duration:0.3,queue:'end'});
		setTimeout("selectingLensType = false;showPrescriptionBox();",400);
		
		if((!selectingNosePos) && (showingNosePos))
		{
			if((selectedLensType.indexOf('PROG') != -1) || (selectedLensType.indexOf('PROGE') != -1) || (selectedLensType.indexOf('BIF') != -1))
			{
				selectingNosePos = true;
				showingNosePos = false;
				Effect.BlindUp('nosepositionbox',{duration:0.3});
				setTimeout("selectingNosePos = false;",300);
			}
		}
	}
}

var RxCommentsOn = false;

function toggleRxComments()
{
	//when toggling comments RX comments box we need to resize the container box
	//so the step3 button does not get lost behind the textarea
	if(!RxCommentsOn)
	{
		RxCommentsOn = true;
		//using prototype to update styles
		Element.setStyle('manualRx',{display: 'block'});	
		Element.setStyle('prescription_data',{height: '275px'});
		Element.setStyle('proceedButton_step2',{'margin-top': '-80px'});
		window.scrollBy(0,90); //scroll the window down abit so the "step 3" button is still visible
	}
	else
	{
		RxCommentsOn = false;
		//using prototype to update styles
		Element.setStyle('manualRx',{display: 'none'});
		Element.setStyle('prescription_data',{height: '150px'});
		Element.setStyle('proceedButton_step2',{'margin-top': '-30px'});
	}
	return true;
}

function changeTypeSelection()
{
	if(selectingLensType == false)
	{		
		var the_form = document.orderform;
		if(the_form.mv_order_lens.length) {
			var selectedLensType = checkedVals(the_form.mv_order_lens);
		} else {
			var selectedLensType = the_form.mv_order_lens.value;	
		}
				
		Effect.BlindUp('currentLensTypeSelection', {duration:0.3});
		selectingLensType = true;	
		setTimeout("Effect.BlindDown('fullLensTypeSelection',{duration:0.3}); selectingLensType = false;",300);
		if((selectedLensType.indexOf('PROG') != -1) || (selectedLensType.indexOf('PROGE') != -1) || (selectedLensType.indexOf('BIF') != -1))
		{
			selectingNosePos = true;
			showingNosePos = true;
			Effect.BlindDown('nosepositionbox',{duration:0.3,delay:0.6});
			setTimeout("selectingNosePos = false;",900);	
		}
	}
	return false;	
}

function selectNosePosition()
{
	errClear();
	if(!selectingLensType)
	{	
		hideLensTypeSelection();
		if(!showingPrescription)
		{
			Effect.BlindDown('prescriptionbox',{duration:0.7,delay:1.0});	
			selectingPrescription = true;
			showingPrescription = true;
			setTimeout("selectingPrescription = false;",700);
			if(!loadedprescription)
			{		
				//wait 3 second for animation to finish then load the prescription
				setTimeout("loadPrescription();",1000);
			}
		}
	}	
}


/**** END ---- STEP2 LENS TYPE AND NOSE POSITION SELECTION ****/

function changeSelection()
{
	if(selectingLens == false)
	{
		Effect.BlindUp('currentLensSelection', {duration:0.3});
		selectingLens = true;	
		setTimeout("Effect.BlindDown('fullLensSelection',{duration:0.7}); selectingLens = false;",300);
	}
	return false;	
}

function selectLensPackage()
{
	errClear();
	//using new slide effect	
	//toggleSlide('fullLensSelection','hideLensSelection');
	if(selectingLens == false)
	{
		Effect.BlindUp('fullLensSelection',{duration:0.7});	
		selectingLens = true;	
		setTimeout("hideLensSelection(); selectingLens = false;",700);
	}
	return false;
}

function hideLensSelection()
{
	var the_form = document.orderform;	
	var selectedLens = checkedVals(the_form.mv_order_lenstype);	
	document.getElementById('currentLensSelectionLens').innerHTML = document.getElementById('lensLabel_'+selectedLens).innerHTML + ' - ' + document.getElementById('lensLabelLink_'+selectedLens).innerHTML;
	//document.getElementById('currentLensSelection').style.display='block';
	Effect.BlindDown('currentLensSelection', {duration:0.3});
	setTimeout("showCustomizeStep();",300);
	document.getElementById('step3buybutton').style.display='block';
}

function showCustomizeStep()
{
	if(!firstSelected)
	{		
		if(allowed_tints_count > 0)
		{
			if(!showingOptions)
			{
				Effect.BlindDown('lensoptions', {duration:0.7});	
				showingOptions = true;
			}		
			if(!shownCaseSelect)
			{
				shownCaseSelect = true;
				setTimeout("showCaseSummaryStep()",700);
			}
		}
		else
		{		
			if(safety_frame > 0)
			{	
				document.getElementById('lensoptions').style.display = 'none';
				showingOptions = false;
			}
			else if(!showingOptions)
			{				
				Effect.BlindDown('lensoptions', {duration:0.7});
				showingOptions = true;
				if(!shownCaseSelect)
				{
					shownCaseSelect = true;
					setTimeout("showCaseSummaryStep()",700);
				}
			}
			if(!shownCaseSelect)
			{
				shownCaseSelect = true;
				showCaseSummaryStep();
			}
		}
	}
}

function showCaseSummaryStep()
{
	Effect.BlindDown('caseSummaryContainer', {duration:0.7});
}

function selectCase(selectCase)
{
	toggleCaseSelection();	
	setTimeout("selectCaseChange('"+selectCase+"');",400);	
}

function selectCaseChange(selectCase)
{
	var currentCaseName = document.getElementById('currentCaseName');
	var currentCasePrice = document.getElementById('currentCasePrice');
	var selectedCaseName = document.getElementById('caseName_'+selectCase);
	var currentCaseImage = document.getElementById('currentCaseImage');
	currentCaseName.innerHTML = selectedCaseName.innerHTML;
	framePrice = cost['mv_order_case'][selectCase];	
	if(framePrice == 0) {
		if(locale == 'es_ES') {
			framePrice = '!Est� incluido GRATIS';
		} else {
			framePrice = 'Included FREE!';	
		}
	} else {
		framePrice = '$'+framePrice;
		if(locale == 'es_ES') {
			framePrice+= ' adicionales';	
		} else {
			framePrice+= ' additional';		
		}
	}
	currentCasePrice.innerHTML = framePrice;
	currentCaseImage.src = caseImages[selectCase].src;
	
	document.getElementById('mv_order_case').value = selectCase;
	selectedCase = selectCase;
	calcPrice(); 
}

showCaseSelectionState = 0; //define case state as hidden
function toggleCaseSelection()
{
	var caseLayer = document.getElementById('selectCase');
	var aleft=document.all? caseLayer.scrollLeft : caseLayer.pageXOffset
	var atop=document.all? caseLayer.scrollTop : caseLayer.pageYOffset

	caseChoose = document.getElementById('chooseCaseContainer');
	caseLayer = document.getElementById('selectCase');	
	
	var cleft = 0;
	var ctop = 0;

	while (caseChoose.offsetParent){
		cleft += caseChoose.offsetLeft;
		ctop += caseChoose.offsetTop;
		caseChoose = caseChoose.offsetParent;
	}
	
	cleft += caseChoose.offsetLeft;
	ctop += caseChoose.offsetTop;
	
	layertop = ctop - 140;
	layerleft = cleft - 236;
	
	if(layertop < 0)
	{
		layertop = 10;
	}

	caseLayer.style.top = layertop + "px";
	caseLayer.style.left = layerleft + "px";
	
	if(showCaseSelectionState)
	{
		//hide case selection
		Effect.Shrink('selectCase',{direction:'bottom-left',duration:0.5});
		showCaseSelectionState = 0;	
	}
	else
	{
		//show case selection			
		Effect.Grow('selectCase',{direction:'bottom-left',duration:0.5});
		showCaseSelectionState = 1;	
	}	
	//caseLayer.style.display = (showCaseSelectionState?'none':'block');		
	//showCaseSelectionState = (showCaseSelectionState?0:1);
}

showCasePreviewState = 0; //define case state as hidden
function previewCase()
{
	var caseLayer = document.getElementById('casePreview');
	var aleft=document.all? caseLayer.scrollLeft : caseLayer.pageXOffset
	var atop=document.all? caseLayer.scrollTop : caseLayer.pageYOffset

	caseChoose = document.getElementById('chooseCaseContainer');
	caseLayer = document.getElementById('casePreview');
	
	var casePreviewImage = document.getElementById('casePreviewImage');
	casePreviewImage.src = caseLargeImages[selectedCase].src;

	var cleft = 0;
	var ctop = 0;

	while (caseChoose.offsetParent){
		cleft += caseChoose.offsetLeft;
		ctop += caseChoose.offsetTop;
		caseChoose = caseChoose.offsetParent;
	}
	
	cleft += caseChoose.offsetLeft;
	ctop += caseChoose.offsetTop;
	
	layertop = ctop - 220;
	layerleft = cleft - 200;
	
	if(layertop < 0)
	{
		layertop = 10;
		layerleft = cleft - 600;
	}

	caseLayer.style.top = layertop + "px";
	caseLayer.style.left = layerleft + "px";
	
	if(showCasePreviewState)
	{
		//hide case preview
		Effect.Shrink('casePreview',{direction:'bottom-left',duration:0.5});
		showCasePreviewState = 0;	
	}
	else
	{
		//show case preview			
		Effect.Grow('casePreview',{direction:'bottom-left',duration:0.5});
		showCasePreviewState = 1;	
	}	
}






/****** START - CAPTURE MOUSE MOVEMENT AND DRAG ********/

function mouseMove(ev){
	ev = ev || window.event;
	mousePos = mouseCoords(ev);
}

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.documentElement.scrollLeft + document.body.scrollLeft,
		y:ev.clientY + document.documentElement.scrollTop + document.body.scrollTop
	};
}

//<![CDATA[
// Determine browser and version.

function Browser() {

  var ua, s, i;

  this.isIE    = false;
  this.isNS    = false;
  this.version = null;

  ua = navigator.userAgent;

  s = "MSIE";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isIE = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  s = "Netscape6/";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  // Treat any other "Gecko" browser as NS 6.1.

  s = "Gecko";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = 6.1;
    return;
  }
}

var browser = new Browser();

// Global object to hold drag information.

var dragObj = new Object();
dragObj.zIndex = 0;

function dragStart(event, id) {

  var el;
  var x, y;

  // If an element id was given, find it. Otherwise use the element being
  // clicked on.

  if (id)
    dragObj.elNode = document.getElementById(id);
  else {
    if (browser.isIE)
      dragObj.elNode = window.event.srcElement;
    if (browser.isNS)
      dragObj.elNode = event.target;

    // If this is a text node, use its parent element.

    if (dragObj.elNode.nodeType == 3)
      dragObj.elNode = dragObj.elNode.parentNode;
  }

  // Get cursor position with respect to the page.

  if (browser.isIE) {
    x = window.event.clientX + document.documentElement.scrollLeft
      + document.body.scrollLeft;
    y = window.event.clientY + document.documentElement.scrollTop
      + document.body.scrollTop;
  }
  if (browser.isNS) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  // Save starting positions of cursor and element.

  dragObj.cursorStartX = x;
  dragObj.cursorStartY = y;
  dragObj.elStartLeft  = parseInt(dragObj.elNode.style.left, 10);
  dragObj.elStartTop   = parseInt(dragObj.elNode.style.top,  10);

  if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
  if (isNaN(dragObj.elStartTop))  dragObj.elStartTop  = 0;

  // Update element's z-index.

  dragObj.elNode.style.zIndex = ++dragObj.zIndex;

  // Capture mousemove and mouseup events on the page.

  if (browser.isIE) {
    document.attachEvent("onmousemove", dragGo);
    document.attachEvent("onmouseup",   dragStop);
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
  if (browser.isNS) {
    document.addEventListener("mousemove", dragGo,   true);
    document.addEventListener("mouseup",   dragStop, true);
    event.preventDefault();
  }
}

function dragGo(event) {

  var x, y;

  // Get cursor position with respect to the page.

  if (browser.isIE) {
    x = window.event.clientX + document.documentElement.scrollLeft
      + document.body.scrollLeft;
    y = window.event.clientY + document.documentElement.scrollTop
      + document.body.scrollTop;
  }
  if (browser.isNS) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  // Move drag element by the same amount the cursor has moved.

  dragObj.elNode.style.left = (dragObj.elStartLeft + x - dragObj.cursorStartX) + "px";
  dragObj.elNode.style.top  = (dragObj.elStartTop  + y - dragObj.cursorStartY) + "px";

  if (browser.isIE) {
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
  if (browser.isNS)
    event.preventDefault();
}

function dragStop(event) {

  // Stop capturing mousemove and mouseup events.

  if (browser.isIE) {
    document.detachEvent("onmousemove", dragGo);
    document.detachEvent("onmouseup",   dragStop);
  }
  if (browser.isNS) {
    document.removeEventListener("mousemove", dragGo,   true);
    document.removeEventListener("mouseup",   dragStop, true);
  }
}

//]]>

/****** END - CAPTURE MOUSE MOVEMENT AND DRAG ********/



/****** START - STEP2 RX HELP QUESTIONS ********/

var rxHelpQuestion;

function toggleRxQuestion(questionNumber)
{
	//hide any previous element if it is not the same as the current
	if(rxHelpQuestion)
	{
		if(rxHelpQuestion.id != 'rxQuestion'+questionNumber)
		{
			rxHelpQuestion.style.display='none';
		}
	}
	
	//set the object to the newly select question
	rxHelpQuestion = document.getElementById('rxQuestion'+questionNumber);			
	if(rxHelpQuestion.style.display == 'block')
	{
		//hide this element
		rxHelpQuestion.style.display='none';		
	}
	else
	{		
		//show this element
		rxHelpQuestion.style.display='block';	
	}
}

/****** END - STEP2 RX HELP QUESTIONS ********/



/****** START - STEP2 RX HELP MOUSEOVERS ********/

var tipArray = {
	od: {
		title: 'O.D.',
		detail: 'Right eye'
	},
	os: {
		title: 'O.S.',
		detail: 'Left eye'
	},
	sph: {
		title: 'SPH (sphere)',
		detail: 'The "overall" power.  Can be minus-power or plus-power.'
	},
	cyl: {
		title: 'CYL (cylinder)',
		detail: 'The amount of astigmatism.  Can be minus-power or plus-power.<br /><br />If you have no astigmatism, sometimes doctors will write a place-holder here like "SPH" (sphere) or "D.S." (diopters sphere) to signify that there is only a sphere power needed.',
		width:300
	},
	axis: {
		title: 'AXIS',
		detail: 'Usually abbreviated with a letter "X" in front of it, this tells us the direction of the astigmatism in degrees.<br /><br />If you have no astigmatism, there will be nothing written on your prescription.  Leave it blank on our form if this is the case.',
		width:300
	},
	prism: {
		title: 'PRISM',
		detail: 'A component a doctor may add to a prescription for people with double-vision or certain special types of eyestrain.  If you see something here or a number followed by a triangle (?), type it into your order comments.  If you are still having trouble call us and we can take this order over the phone. Putting prism in your eyeglasses is an additional $19.95.',
		width:300
	},
	add: {
		title: 'ADD',
		detail: 'How much power gets added to the distance Rx for your single-vision reading-only eyeglasses OR for the bottom half of your bifocals/progressives.',
		width:300
	},
	dist: {
		title: 'DIST',
		detail: 'The distance portion of your prescription.  This is for distance eyeglasses only (or for people who do not need bifocals or progressives yet).',
		width:300
	},
	near: {
		title: 'NEAR',
		detail: 'The near (reading-only) portion of your prescription.  If it is for bifocals or progressives, sometimes the ADD power is written here.  If you are not sure, type exactly what you see in the order comments box or call us for assistance.',
		width:300
	}	
};

	
var currentObj;
var tipObj;
var tipContent;
var tipLeft;
var tipTop;

document.onmousemove = function(ev)
{
	mouseMove(ev);
	moveRxHelpTip();	
}

//show the rx help tips
function showRxHelpTip(item)
{	
	//set the content
	tipContent = '<b>' + tipArray[item].title + '</b><br />' + tipArray[item].detail;
	Element.update('tipLayer',tipContent);
	
	tipWidth = 'auto';
	if(tipArray[item].width)
	{
		tipWidth = tipArray[item].width + 'px';
	}
	
	tipLeft = mousePos.x + 30;
	tipTop = mousePos.y;	
	Element.setStyle('tipLayer',{display:'block',left:tipLeft + 'px',top:tipTop + 'px',width:tipWidth});
}

function moveRxHelpTip()
{
	if(document.getElementById('tipLayer'))
	{
		if(document.getElementById('tipLayer').style.display == 'block')
		{
			tipLeft = mousePos.x + 30;
			tipTop = mousePos.y;	
			Element.setStyle('tipLayer',{left:tipLeft + 'px',top:tipTop + 'px'});		
		}
	}	
}

//hide the rx help tips
function hideRxHelpTip()
{
	Element.setStyle('tipLayer',{display:'none'});
}


/****** END - STEP2 RX HELP MOUSEOVERS ********/
